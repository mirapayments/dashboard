import { rest } from "msw";
import meResponse from "../fixures/me.json";
import getComplianceResponse from "../fixures/compliance.json";
import paymentlinksResponse from "../fixures/paymentlinks.json";
import paymentlinkResponse from "../fixures/paymentlink.json";
import customersResponse from "../fixures/customers.json";
import customerResponse from "../fixures/customer.json";
import transactionsResponse from "../fixures/transactions.json";
import transactionResponse from "../fixures/transaction.json";
import usersResponse from "../fixures/users.json";
import credentialsResponse from "../fixures/credentials.json";
import balanceResponse from "../fixures/balance.json";

const base =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

const baseUrl =
  process.env.REACT_APP_TEST_WITH_LIVE_SERVER == "yes"
    ? "https://api.mirapayments.com"
    : base;

export const handlers = [
  rest.get(`${baseUrl}/users/me/`, (req, res, ctx) => {
    return res(ctx.json(meResponse));
  }),

  rest.get(`${baseUrl}/accounts/compliance/1234/`, (req, res, ctx) => {
    return res(ctx.json(getComplianceResponse));
  }),

  rest.get(`${baseUrl}/payments/payment-link/1234/`, (req, res, ctx) => {
    return res(ctx.json(paymentlinksResponse));
  }),

  rest.get(
    `${baseUrl}/payments/payment-link/undefined/1234/`,
    (req, res, ctx) => {
      return res(ctx.json(paymentlinkResponse));
    }
  ),

  rest.get(`${baseUrl}/customers/1234/`, (req, res, ctx) => {
    return res(ctx.json(customersResponse));
  }),

  rest.get(`${baseUrl}/customers/customer/undefined/`, (req, res, ctx) => {
    return res(ctx.json(customerResponse));
  }),

  rest.get(`${baseUrl}/transactions/1234/`, (req, res, ctx) => {
    return res(ctx.json(transactionsResponse));
  }),

  rest.get(
    `${baseUrl}/transactions/transaction/undefined/`,
    (req, res, ctx) => {
      return res(ctx.json(transactionResponse));
    }
  ),

  rest.get(`${baseUrl}/users/list/1234/`, (req, res, ctx) => {
    return res(ctx.json(usersResponse));
  }),

  rest.get(`${baseUrl}/accounts/credentials/1234/`, (req, res, ctx) => {
    return res(ctx.json(credentialsResponse));
  }),

  rest.get(`${baseUrl}/accounts/balance/1234/`, (req, res, ctx) => {
    return res(ctx.json(balanceResponse));
  }),
];
