import { createSlice } from "@reduxjs/toolkit"; 

// check if mode is live from local storage
const liveMode = localStorage.getItem("activeMode") === "live";
console.log("MODE",liveMode);

const initialState = {
  username: "",
  email: "",
  liveDashboardMode: liveMode,
  signedInAccountNumber: "",
  isSignedIn: false,
  userEmailVerified: false,
  userPasswordReset: false,
  userAccounts: [],
  token: "",
  role: "",
};

export const signinSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserName: (state, action) => {
      state.username = action.payload;
    },
    setUserToken: (state, action) => {
      state.token = action.payload;
    },
    setSignedInAccountNumber: (state, action) => {
      state.signedInAccountNumber = action.payload;
    },
    setSignedInState: (state, action) => {
      state.isSignedIn = action.payload;
    },
    setUserEmailVerification: (state, action) => {
      state.userEmailVerified = action.payload;
    },
    setUserEmail: (state, action) => {
      state.email = action.payload;
    },
    setUserPasswordReset: (state, action) => {
      state.userPasswordReset = action.payload;
    },
    setUserAccounts: (state, action) => {
      state.userAccounts = action.payload;
    },
    setLiveDashBoardMode: (state, action) => {
      state.liveDashboardMode = action.payload;
    },
    // eslint-disable-next-line no-unused-vars
    logout: (state) => {},
    setUserRole: (state, action) => {
      state.role = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setUserName,
  setUserPassword,
  setSignedInState,
  setUserEmailVerification,
  setUserEmail,
  setUserResetPasswordSent,
  setUserPasswordReset,
  setUserAccounts,
  setSignedInAccountNumber,
  setLiveDashBoardMode,
  setUserToken,
  logout,
  setUserRole,
} = signinSlice.actions;

export default signinSlice.reducer;
