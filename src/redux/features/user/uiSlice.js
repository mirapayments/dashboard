import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  tabPosition: "1",
  roleTabPosition: "1",
  balanceTabPosition:"1",
  customerTabPosition: "1",
  paymentLinkUpdated: false,
  userInvited: false,
  paymentId: "",
  customerAdded: false,
  paymentLinkListLoading: false,
  transactionListLoading: false,
  topUpListLoading: false,
  payOutListLoading: false,
  disputesListLoading: false,
  customerListLoading: false,
  complianceSetupComplete: false,
};

export const uiSlice = createSlice({
  name: "ui",
  initialState,
  reducers: {
    setTabPosition: (state, action) => {
      state.tabPosition = action.payload;
    },
    setPaymentId: (state, action) => {
      state.paymentId = action.payload;
    },
    setRoleTabPosition: (state, action) => {
      state.roleTabPosition = action.payload;
    },
    setBalanceTabPosition: (state, action) => {
      state.balanceTabPosition = action.payload;
    },
    setCustomerTabPosition: (state, action) => {
      state.customerTabPosition = action.payload;
    },
    setPaymentLinkUpdated: (state, action) => {
      state.paymentLinkUpdated = action.payload;
    },
    setCustomerAdded: (state, action) => {
      state.customerAdded = action.payload;
    },
    setUserInvited: (state, action) => {
      state.userInvited = action.payload;
    },
    setComplianceSetupComplete: (state, action) => {
      state.complianceSetupComplete = action.payload;
    },
    setPaymentLinkListLoading: (state, action) => {
      state.paymentLinkListLoading = action.payload;
    },
    setTransactionListLoading: (state, action) => {
      state.transactionListLoading = action.payload;
    },
    setTopUpListLoading: (state, action) => {
      state.topUpListLoading = action.payload;
    },
    setPayoutListLoading: (state, action) => {
      state.payOutListLoading = action.payload;
    },
    setDisputesListLoading: (state, action) => {
      state.payOutListLoading = action.payload;
    },
    setCustomerListLoading: (state, action) => {
      state.customerListLoading = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setPaymentLinkListLoading,
  setTabPosition,
  setPaymentLinkUpdated,
  setRoleTabPosition,
  setCustomerTabPosition,
  setUserInvited,
  setPaymentId,
  setCustomerAdded,
  setTransactionListLoading,
  setTopUpListLoading,
  setPayoutListLoading,
  setDisputesListLoading,
  setCustomerListLoading,
  setBalanceTabPosition,
  setComplianceSetupComplete,
} = uiSlice.actions;

export default uiSlice.reducer;
