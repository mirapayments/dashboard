// Import the necessary the libraries and libraries.
import React from "react";
import { useParams } from "react-router";
import { Col, Container, Row } from "reactstrap";

// Import component.
import CustomerDetails from "./CustomerDetailsComponent";
import TransactionDetail from "./CustomerTransactionComponent";

const CustomerDetail = () => {
  const { id } = useParams();

  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          <h3 className="page-title ml-3">Customer Details</h3>
        </Col>
      </Row>
      <Row>
        <Col sm={6} lg={6} md={12}>
          <CustomerDetails id={id} />
        </Col>

        <Col sm={6} lg={6} md={12}>
          <TransactionDetail />
        </Col>
      </Row>
    </Container>
  );
};

export default CustomerDetail;
