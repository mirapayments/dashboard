// Import the necessary libraries and dependencies.
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./CardContainer.module.css";
import { Col, Card, CardBody } from "reactstrap";
import { getCustomersDetailWithID } from "../../../../../utils/apiHandlers/getCustomersDetailWithID";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";

// CustomerDetails Component.0
const CustomerDetails = ({ id }) => {
  const [customerDetails, setCustomerDetails] = useState({});

  // API Calls
  async function getCustomerDetailsWithIDHandler() {
    try {
      const response = await getCustomersDetailWithID(id).catch((err) =>
        console.log(err)
      );
      setCustomerDetails(response.data.data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getCustomerDetailsWithIDHandler();
  }, []);

  function isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }
  return (
    !isEmpty(customerDetails) && (
      <Col md={12}>
        <Card>
          <CardBody className={styles.card_body}>
            <div className="mb-4">
              <h4>{customerDetails?.full_name}</h4>
            </div>

            <div className={styles.details_wrapper}>
              <div>
                <p>Full Name</p>
                <p>{customerDetails?.full_name}</p>
              </div>
              <div>
                <p>Email</p>
                <p>{customerDetails?.email}</p>
              </div>
              <div>
                <p>Phone</p>
                <p>{customerDetails?.phone}</p>
              </div>
              <div>
                <p>Created On</p>
                <p>{formatDate(customerDetails?.created_at)}</p>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
    )
  );
};

CustomerDetails.propTypes = {
  id: PropTypes.number,
};

// Export the Component.
export default CustomerDetails;
