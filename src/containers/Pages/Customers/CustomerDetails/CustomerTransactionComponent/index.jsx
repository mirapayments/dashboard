// Import the necessary libraries and dependencies.
import React, { useEffect, useState } from "react";
import styles from "./CardContainer.module.css";
import { Card, CardBody, Col } from "reactstrap";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";
import { Table } from "reactstrap";
import { getTransactions } from "../../../../../utils/apiHandlers/getTransactions";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { transactionBadge } from "../../../../../utils/transactionBadge/transactionBadge";
import { Badge } from "reactstrap";
import Skeleton from "react-loading-skeleton";

// Table Header.
const header = [
  { id: 0, title: "Reference" },
  { id: 1, title: "Status" },
  { id: 2, title: "Amount" },
  { id: 3, title: "Date" },
];

// TransactionDetail Component.

const TransactionDetail = () => {
  const [transactionList, setTransactionList] = useState([]);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const history = useHistory();

  // usEffect
  useEffect(() => {
    getTransactionHandler();
  }, [signedInAccountNumber]);

  // API Call.
  async function getTransactionHandler() {
    try {
      const response = await getTransactions(signedInAccountNumber).catch(
        (err) => console.log(err)
      );
      setTransactionList(response.data.data);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Col md={12}>
      <Card>
        <CardBody className={styles.card_body}>
          <h4 className="m1-2 mb-4"> Customer Transactions </h4>
          <Table
            responsive
            className="table--bordered dashboard__audience-table"
          >
            <thead>
              <tr>
                {header.map((item) => (
                  <th className={styles.thead_overide} key={item.id}>
                    <p>{item.title || <Skeleton />} </p>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {transactionList?.map((item) => (
                <>
                  <tr
                    onClick={() => {
                      history.push(`/pages/transaction/${item?.id}`);
                    }}
                    className={styles.trstyles}
                  >
                    <td className={styles.tdstyles}>
                      <p>{item?.short_reference || <Skeleton />}</p>
                    </td>
                    <td className={styles.tdstyles}>
                      <Badge
                        className={styles.badge}
                        color={transactionBadge(item?.status)}
                      >
                        {item?.status || <Skeleton />}
                      </Badge>
                    </td>
                    <td className={styles.tdstyles}>
                      <p>
                        {`${item.amount_currency} ${item.amount}` || (
                          <Skeleton />
                        )}
                      </p>
                    </td>
                    <td className={styles.tdstyles}>
                      <p>{formatDate(item?.created_at) || <Skeleton />}</p>
                    </td>
                  </tr>
                </>
              ))}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </Col>
  );
};

// Export the component
export default TransactionDetail;
