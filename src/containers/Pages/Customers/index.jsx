// Import the necessary libraries and dependencies.
import React from "react";
import { useTranslation } from "react-i18next";
import { Col, Container, Row } from "reactstrap";

// Import Components.
import CreateTableData from "./components/CreateData";
import EditableReactTable from "./components/Table/components/EditableReactTable";

// Customer Component
const Customers = () => {
  const { t } = useTranslation("common");
  const reactTableData = CreateTableData();
  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          <h3 className="page-title">{t("Customers")}</h3>
        </Col>
      </Row>
      <Row>
        <EditableReactTable reactTableData={reactTableData} />
      </Row>
    </Container>
  );
};

// Export the component.
export default Customers;
