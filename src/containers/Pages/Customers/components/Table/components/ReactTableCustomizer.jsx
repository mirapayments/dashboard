/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Row, Collapse, UncontrolledTooltip } from "reactstrap";
import DownIcon from "mdi-react/ChevronDownIcon";
import ReactTableCustomizerToggle from "./ReactTableCustomizerToggle";

const ReactTableCustomizer = ({
  handleClickIsEditable,
  handleClickIsResizable,
  handleClickIsSortable,
  handleClickWithDragAndDrop,
  handleClickWithPagination,
  handleClickWithSearchEngine,
  isEditable,
  isResizable,
  isSortable,
  isDisabledDragAndDrop,
  isDisabledEditable,
  isDisabledResizable,
  withDragAndDrop,
  withPagination,
  withSearchEngine,
  fullCustomizer,
}) => {
  const arrayTableCustomizerSingleToggle = [
    {
      id: 0,
      text: "Show Filter",
      func: handleClickWithSearchEngine,
      isChecked: withSearchEngine,
    },
    {
      id: 1,
      text: "Show Sort",
      func: handleClickIsSortable,
      isChecked: isSortable,
    },
    {
      id: 2,
      text: "Show Pagination",
      func: handleClickWithPagination,
      isChecked: withPagination,
    },
  ];
  const arrayTableCustomizerAddictionToggle = [
    {
      id: 0,
      text: "Drag&Drop mode",
      func: handleClickWithDragAndDrop,
      isChecked: withDragAndDrop,
      isDisabled: isDisabledDragAndDrop,
      tooltip:
        "Drag&Drop mode cannot be performed at the same time with Resizable Mode",
    },
    {
      id: 1,
      text: "Editable mode",
      func: handleClickIsEditable,
      isChecked: isEditable,
      isDisabled: isDisabledEditable,
      tooltip:
        "Editable mode cannot be performed at the same time with Resizable Mode",
    },
    {
      id: 2,
      text: "Resizable mode",
      func: handleClickIsResizable,
      isChecked: isResizable,
      isDisabled: isDisabledResizable,
      tooltip:
        "Resizable mode cannot be performed at the same time with Drag&Drop and Editable Mode",
    },
  ];
  const [collapse, setCollapse] = useState(false);

  const handleOpen = () => {
    setCollapse(!collapse);
  };

  return null;
};

ReactTableCustomizer.propTypes = {
  handleClickIsEditable: PropTypes.func,
  handleClickIsResizable: PropTypes.func,
  handleClickIsSortable: PropTypes.func.isRequired,
  handleClickWithDragAndDrop: PropTypes.func,
  handleClickWithPagination: PropTypes.func.isRequired,
  handleClickWithSearchEngine: PropTypes.func.isRequired,
  isEditable: PropTypes.bool,
  isResizable: PropTypes.bool,
  isSortable: PropTypes.bool.isRequired,
  isDisabledDragAndDrop: PropTypes.bool,
  isDisabledEditable: PropTypes.bool,
  isDisabledResizable: PropTypes.bool,
  withDragAndDrop: PropTypes.bool,
  withPagination: PropTypes.bool.isRequired,
  withSearchEngine: PropTypes.bool.isRequired,
  fullCustomizer: PropTypes.bool,
};

ReactTableCustomizer.defaultProps = {
  handleClickIsEditable: () => {},
  handleClickIsResizable: () => {},
  handleClickWithDragAndDrop: () => {},
  isEditable: false,
  isResizable: false,
  isDisabledDragAndDrop: false,
  isDisabledEditable: false,
  isDisabledResizable: false,
  withDragAndDrop: false,
  fullCustomizer: false,
};

export default ReactTableCustomizer;
