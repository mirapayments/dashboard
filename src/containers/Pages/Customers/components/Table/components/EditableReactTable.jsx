// Import the necessary libraries and dependencies.
import React, { useState, useEffect } from "react";
import { PropTypes } from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { getCustomers } from "../../../../../../utils/apiHandlers/getCustomers";
import { formatDate } from "../../../../../../utils/dateFormater/dateFormater";
import { setCustomerListLoading } from "../../../../../../redux/features/user/uiSlice";

// Import Components.
import ReactTableCustomizer from "./ReactTableCustomizer";
import ReactTableBase from "./ReactTableBase";

// EditableReactTable Components.
const EditableReactTable = ({ reactTableData }) => {
  const [rows, setData] = useState([]);
  const [withPagination, setWithPaginationTable] = useState(true);
  const [isSortable, setIsSortable] = useState(true);
  const [withSearchEngine, setWithSearchEngine] = useState(true);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const { customerAdded } = useSelector((state) => state.ui);
  const dispatch = useDispatch();

  // Get Customers Handler.
  useEffect(() => {
    dispatch(setCustomerListLoading(true));
    getCustomersHandler(signedInAccountNumber);
  }, [signedInAccountNumber, customerAdded]);

  // Fetch the Customers.

  async function getCustomersHandler(account_number) {
    try {
      const response = await getCustomers(account_number).catch((err) => { console.log(err);});

      const data = response.data.data;

      // Format the date.
      const modified = data.map((item) => {
        return {
          ...item,
          created_at: formatDate(item.created_at),
        };
      });

      // Set State.
      setData(modified);
      dispatch(setCustomerListLoading(false));
    } catch (error) {
      console.log(error);
    }
  }

  // Update the table for every new column.
  const updateEditableData = (rowIndex, columnId, value) => {
    setData((old) =>
      old.map((item, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          };
        }
        return item;
      })
    );
  };

  // Make table sortable, with pagination and with search engine.

  const handleClickIsSortable = () => {
    setIsSortable(!isSortable);
  };

  const handleClickWithPagination = () => {
    setWithPaginationTable(!withPagination);
  };

  const handleClickWithSearchEngine = () => {
    setWithSearchEngine(!withSearchEngine);
  };

  // Set the table config object.

  const tableConfig = {
    isEditable: true,
    isSortable: true,
    isResizable: true,
    withPagination: true,
    withSearchEngine: true,
    manualPageSize: [10, 20, 30, 40],
    placeholder: "Search by name, email",
  };

  return (
    <Col md={12} lg={12}>
      <Card>
        <CardBody>
          <div>
            <ReactTableCustomizer
              className="float-left p-0"
              handleClickIsSortable={handleClickIsSortable}
              handleClickWithPagination={handleClickWithPagination}
              handleClickWithSearchEngine={handleClickWithSearchEngine}
              isSortable={isSortable}
              withPagination={withPagination}
              withSearchEngine={withSearchEngine}
            />
            <ReactTableBase
              key={withSearchEngine ? "searchable" : "common"}
              columns={reactTableData.tableHeaderData}
              data={rows}
              updateEditableData={updateEditableData}
              tableConfig={tableConfig}
            />
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

// PropTypes Validation.
EditableReactTable.propTypes = {
  reactTableData: PropTypes.shape({
    tableHeaderData: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        name: PropTypes.string,
      })
    ),
    tableRowsData: PropTypes.arrayOf(PropTypes.shape()),
  }).isRequired,
};

// Export the Component.
export default EditableReactTable;
