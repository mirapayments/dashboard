// Import the necessary libraries and dependencies
import React, { useState } from "react";
import PropTypes from "prop-types";

// Import components
import ReactTableConstructor from "./ReactTableConstructor";
import ReactTableCell from "./ReactTableCell";
import ReactTableCellEditable from "./ReactTableEditableCell";

// ReactTableBase Components
const ReactTableBase = ({ tableConfig, columns, data, updateEditableData }) => {
  // Define the tableConfig props object
  const {
    isEditable,
    isSortable,
    withPagination,
    withSearchEngine,
    manualPageSize,
  } = tableConfig;
  const [filterValue, setFilterValue] = useState(null);

  // Define the tableOptions object.
  const tableOptions = {
    columns,
    data,
    updateEditableData,
    setFilterValue,
    defaultColumn: {},
    isEditable,
    dataLength: data.length,
    autoResetPage: false,
    disableSortBy: !isSortable,
    manualSortBy: !isSortable,
    manualGlobalFilter: !withSearchEngine,
    manualPagination: !withPagination,
    initialState: {
      pageIndex: 0,
      pageSize: manualPageSize ? manualPageSize[0] : 10,
      globalFilter: withSearchEngine && filterValue ? filterValue : undefined,
    },
  };

  let tableOptionalHook = [];
  if (withSearchEngine) {
    tableOptions.defaultColumn = {
      Cell: ReactTableCell,
    };
  }
  if (isEditable) {
    tableOptions.defaultColumn = {
      Cell: ReactTableCellEditable,
    };
  }

  return (
    <ReactTableConstructor
      tableConfig={tableConfig}
      tableOptions={tableOptions}
      tableOptionalHook={tableOptionalHook}
    />
  );
};

// PropsTypes validation
ReactTableBase.propTypes = {
  tableConfig: PropTypes.shape({
    isEditable: PropTypes.bool,
    isSortable: PropTypes.bool,
    withPagination: PropTypes.bool,
    withSearchEngine: PropTypes.bool,
    manualPageSize: PropTypes.arrayOf(PropTypes.number),
  }),
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      name: PropTypes.string,
    })
  ),
  data: PropTypes.arrayOf(PropTypes.shape()),
  updateEditableData: PropTypes.func,
};

ReactTableBase.defaultProps = {
  tableConfig: {
    isEditable: false,
    isSortable: false,
    withPagination: false,
    withSearchEngine: false,
    manualPageSize: [],
  },
  columns: [
    { Header: "#", accessor: "id" },
    { Header: "Header Example Title one", accessor: "first" },
    { Header: "Header Example Title two", accessor: "last" },
  ],
  data: [
    { id: 1, first: "Cell Example Data one", last: "Cell Example Data two" },
    { id: 2, first: "Cell Example Data three", last: "Cell Example Data four" },
  ],

  updateEditableData: () => {},
};

// Export ReactTableBase Components.
export default ReactTableBase;
