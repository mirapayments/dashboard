// Import the necessary libraries and dependencies.
import React from "react";
import { Container, Row } from "reactstrap";

// Import Component.
import EditableReactTable from "../components/EditableReactTable";
import CreateTableData from "../CreateData";

// EditableTable Component.
const EditableTable = () => {
  const reactTableData = CreateTableData();

  return (
    <Container>
      <Row>
        <EditableReactTable reactTableData={reactTableData} />
      </Row>
    </Container>
  );
};

// Export the Components.
export default EditableTable;
