// Import the necessary libraries and dependencies.
import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "reactstrap";
import classNames from "classnames";
import { useSelector } from "react-redux";
import AddCircleOutlineIcon from "mdi-react/AddCircleOutlineIcon";

// Import Css Module.

// Import Components.
import Form from "./Form/Form";

// Modal Components.

const ModalComponent = ({ color, colored, header }) => {
  const [modal, setModal] = useState(false);
  const { className } = useSelector((state) => state.theme);

  const toggle = () => {
    setModal((prevState) => !prevState);
  };

  const modalClass = classNames({
    "modal-dialog--colored": colored,
    "modal-dialog--header": header,
  });

  return (
    <>
      <>
        <button
          color={color}
          onClick={toggle}
          data-testid="create-customer"
          type="submit"
          className="icon btn btn-primary mb-0 button-text"
        >
          <AddCircleOutlineIcon className="mb-1" fontSize="large" /> Add
          Customer{" "}
        </button>
      </>

      <Modal
        isOpen={modal}
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <div className="modal__body">
          <Form setModalOpen={setModal} />
        </div>
      </Modal>
    </>
  );
};

ModalComponent.propTypes = {
  color: PropTypes.string,
  colored: PropTypes.bool,
  header: PropTypes.bool,
};

ModalComponent.defaultProps = {
  title: "",
  message: "",
  colored: false,
  header: false,
};

// Export the Components
export default ModalComponent;
