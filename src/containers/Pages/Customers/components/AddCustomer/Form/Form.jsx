// Import the necessary libraries and dependencies.
import React from "react";
import PropTypes from "prop-types";
import AddCustomerForm from "./components/AddCustomerForm";

// Form Wrapper Component.
const FormWrapper = ({ setModalOpen }) => (
  <div className="account" style={{ minHeight: "0", height: "100%" }}>
    <div className="account__card">
      <div className="account__head">
        <h4 className="account__subhead subhead"> New Customer</h4>
        <p>Fill the fields below to add a new customer</p>
      </div>
      <AddCustomerForm setModal={setModalOpen} />
    </div>
  </div>
);

// Export the FormWrapper Component.

FormWrapper.propTypes = {
  setModalOpen: PropTypes.func
};

export default FormWrapper;
