// Import the necessary libraries and dependencies.
import React from "react";
import { Formik } from "formik";
import PropTypes from "prop-types";
import * as Yup from "yup";
import Swal from "sweetalert2";
import { createNewCustomers } from "../../../../../../../utils/apiHandlers/createCustomers";
import { useSelector, useDispatch } from "react-redux";
import useOnlineStatus from "../../../../../../../utils/hooks/useOnlineStatus";
import AccountOutlineIcon from "mdi-react/AccountOutlineIcon";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import PhoneInput from "react-phone-input-2";
import PhoneIcon from "mdi-react/PhoneIcon";
import Spinner from "react-spinner-material";
import styles from "./AddCustomerForm.module.css";
import { setCustomerAdded } from "../../../../../../../redux/features/user/uiSlice";

// Customer Form components.

const CustomerForm = ({ setModal }) => {
  // const { className } = useSelector((state) => state.theme);
  const online = useOnlineStatus();
  const dispatch = useDispatch();
  const { customerAdded } = useSelector((state) => state.ui);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  async function createCustomers(data, setSubmitting) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    try {
      const res = await createNewCustomers(
        signedInAccountNumber,
        JSON.stringify(data)
      );
      if (res.data.status) {
        Swal.fire({
          title: "",
          text: res.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        dispatch(setCustomerAdded(!customerAdded));
        setSubmitting(false);
        setModal(false);
      }
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <Formik
      initialValues={{ full_name: "", phone: "+234", email: "" }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true);
        createCustomers(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        full_name: Yup.string().required("Please provide your full name"),
        phone: Yup.string()
          .matches({ phoneRegExp }, "Phone number is not valid")
          .required("Input field is required"),
        email: Yup.string()
          .email("Invalid email")
          .required("Email is required"),
      })}
    >
      {({
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => (
        <form
          style={{ height: "10%" }}
          className="form"
          onSubmit={handleSubmit}
        >
          <div className="form__form-group">
            <span className="form__form-group-label">Full Name</span>
            <div
              className={`form__form-group-field ${
                errors.full_name && "form__form-validation"
              }`}
            >
              <div className="form__form-group-icon">
                <AccountOutlineIcon />
              </div>
              <input
                name="full_name"
                component="input"
                type="text"
                placeholder="Full name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.full_name}
              />
            </div>
            <p className="error_validation_text">
              {errors.full_name && touched.full_name && errors.full_name}
            </p>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Email</span>
            <div
              className={`form__form-group-field ${
                errors.email && "form__form-validation"
              }`}
            >
              <div className="form__form-group-icon">
                <AlternateEmailIcon />
              </div>
              <input
                name="email"
                component="input"
                type="email"
                placeholder="Email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
            </div>
            <p className="error_validation_text">
              {errors.email && touched.email && errors.email}
            </p>
          </div>

          <div data-testid="phone-number">
            <span className="form__form-group-label">Phone Number</span>
            <div
              className={`form__form-group-field ${
                errors.phone && "form__form-validation"
              }`}
            >
              <div className="form__form-group-icon">
                <PhoneIcon />
              </div>
              <PhoneInput
                inputProps={{ name: "phone" }}
                onChange={(phone, country, e) => {
                  handleChange(e);
                }}
                placeholder="Enter phone number"
                name="phone_number"
                value={values.phone}
              />
            </div>
            <p className="error_validation_text">
              {errors.phone && touched.phone && errors.phone}
            </p>
          </div>

          <button
            type="submit"
            data-testid="create-customer-btn"
            disabled={isSubmitting}
            className="btn btn-primary account__btn account__btn--small mt-3"
          >
            {isSubmitting ? (
              <div className={styles.button_elem}>
                <Spinner radius={20} color={"#333"} stroke={2} visible={true} />
              </div>
            ) : (
              "Create Customer"
            )}
          </button>
        </form>
      )}
    </Formik>
  );
};

CustomerForm.propTypes = {
  setModal: PropTypes.func,
};

// Export Component.
export default CustomerForm;
