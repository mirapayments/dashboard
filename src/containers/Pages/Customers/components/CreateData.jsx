import { useMemo } from "react";

const CreateTableData = () => {
  const columns = useMemo(
    () => [
      {
        Header: "Full Name",
        accessor: "full_name",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Phone Number",
        accessor: "phone",
      },
      {
        Header: "Created On",
        accessor: "created_at",
      },
    ],
    []
  );

  const reactTableData = { tableHeaderData: columns };
  return reactTableData;
};

export default CreateTableData;
