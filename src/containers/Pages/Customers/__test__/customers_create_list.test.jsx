import "regenerator-runtime/runtime";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import Customers from "../index";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <Customers /> renders", () => {
  const store = mockStore(data);
  it("check if <Customers /> contains create modals, customer list", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Customers />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      const customerSearchInput = screen.getByPlaceholderText(
        "Search by name, email"
      );
      const createCustomer = screen.getByTestId("create-customer");
      const tableRow = screen.getAllByTestId("table-row");
      expect(customerSearchInput).toBeInTheDocument();
      expect(createCustomer).toBeInTheDocument();
      expect(tableRow.length).toBe(1);

      fireEvent.click(screen.getByTestId("create-customer"));
      //   //check if create modal is up
      expect(screen.getByPlaceholderText("Full name")).toBeInTheDocument();
      expect(screen.getByPlaceholderText("Email")).toBeInTheDocument();
      expect(screen.getByTestId("phone-number")).toBeInTheDocument();
      expect(screen.getByTestId("create-customer-btn")).toBeInTheDocument();
    });
  });
});
