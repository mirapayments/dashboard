import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import CustomerDetails from "../CustomerDetails/CustomerDetails";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <CustomerDetails /> renders", () => {
  const store = mockStore(data);
  it("check if <CustomerDetails /> contains data", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <CustomerDetails />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(screen.getAllByText("Adebayo Abdulmalik").length).toBe(2);
      expect(screen.getByText("milikiadbay@gmail.com")).toBeInTheDocument();
      expect(screen.getByText("+234 234 903 956 187")).toBeInTheDocument();
    });
  });
});
