import { Card, CardBody, Col, Container, Row } from "reactstrap";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { removeEmpty } from "../../../utils/removeEmptyObjectKeys/removeEmptyObjectKeys";
import { Formik } from "formik";
import AccountOutlineIcon from "mdi-react/AccountOutlineIcon";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import LinkIcon from "mdi-react/LinkIcon";
import PlanetIcon from "mdi-react/PlanetIcon";
import PhoneIcon from "mdi-react/PhoneClassicIcon";
import styles from "./Compliance.module.css";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "react-spinner-material";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { updateCompliance } from "../../../utils/apiHandlers/updateCompliance";
import { getCompliance } from "../../../utils/apiHandlers/getCompliance";
import { setComplianceSetupComplete } from "../../../redux/features/user/uiSlice";
import Swal from "sweetalert2";

const CompliancePage = () => {
  const { className } = useSelector((state) => state.theme);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const [complianceUpdated, setComplianceUpdated] = useState(true);
  const [complianceDetails, setComplianceDetails] = useState({});
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    getUserComplianceDetailHandler();
  }, [complianceUpdated]);

  // eslint-disable-next-line no-unused-vars

  async function getUserComplianceDetailHandler() {
    try {
      const response = await getCompliance(signedInAccountNumber).catch(
        (err) => {
          console.log(err);
        }
      );
      if (response.data.status) {
        if (response.data.data.setup_complete) {
          dispatch(
            setComplianceSetupComplete(response.data.data.setup_complete)
          );
          return history.push(`/pages/dashboard`);
        }
        setComplianceDetails(response.data.data);
      }
    } catch (error) {
      console.log(error);
    }
  }
  async function updateUserComplianceHandler(data, setSubmitting) {
    try {
      const response = await updateCompliance(
        signedInAccountNumber,
        data
      ).catch((err) => {
        console.log(err);
      });
      if (response.data.status) {
        Swal.fire({
          title: "",
          text: response.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        setSubmitting(false);
        setComplianceUpdated((prev) => !prev);
      }
      setSubmitting(false);
    } catch (error) {
      setSubmitting(false);
    }
  }

  const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/gif",
    "image/png",
    "application/pdf",
  ];
  const FILE_SIZE = 1 * 1024 * 1024;

  //check if there is data and render component only when there is data
  function isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  return (
    !isEmpty(complianceDetails) && (
      <Container className="dashboard">
        <Row>
          <Col md={12}>
            <h3 className="page-title">Compliance</h3>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <Card>
              <CardBody>
                <div className={`${styles.form_container} mt-4`}>
                  <div
                    style={{
                      width: "40rem",
                    }}
                  >
                    <Formik
                      initialValues={{
                        contact_number:
                          complianceDetails?.contact_number !== ""
                            ? complianceDetails?.contact_number
                            : "+234",
                        contact_email: complianceDetails?.contact_email,
                        office_address: complianceDetails?.office_address,
                        owner_name: complianceDetails?.owner_name,
                        nationality: complianceDetails?.nationality,
                        home_address: complianceDetails?.home_address,
                        identity_type:
                          complianceDetails?.identity_type !== ""
                            ? complianceDetails?.identity_type
                            : "Drivers Licence",
                        identity_file: undefined,
                        bank_acct_name: complianceDetails?.bank_acct_name,
                        bank_number: complianceDetails?.bank_number,
                        bank_name: complianceDetails?.bank_name,
                      }}
                      onSubmit={(values, { setSubmitting }) => {
                        setSubmitting(true);
                        const payload = removeEmpty(values);
                        const formData = new FormData();
                        for (let item in payload) {
                          formData.append(item, payload[item]);
                        }
                        console.log(formData.getAll("identity_file"));
                        updateUserComplianceHandler(formData, setSubmitting);
                      }}
                      validationSchema={Yup.object().shape({
                        contact_number: Yup.string()
                          .required("Please provide a phone number.")
                          .notRequired(),
                        contact_email: Yup.string().notRequired().nullable(),
                        office_address: Yup.string().notRequired().nullable(),
                        owner_name: Yup.string()
                          .matches(
                            /^[aA-zZ\s]+$/,
                            "Only alphabets are allowed as account name "
                          )
                          .notRequired()
                          .min(
                            4,
                            "Owner name should be a minimum of 4 characters."
                          ),
                        nationality: Yup.string()
                          .matches(
                            /^[aA-zZ\s]+$/,
                            "Only alphabets are allowed as account name "
                          )
                          .notRequired()
                          .min(
                            4,
                            "Last name should be a minimum of 4 characters."
                          ),
                        home_address: Yup.string().notRequired().nullable(),
                        identity_type: Yup.string().required(
                          "Please select an identity type"
                        ),
                        identity_file: Yup.mixed()
                          .required("A file is required")
                          .test(
                            "fileSize",
                            "File too large",
                            (value) => value && value.size <= FILE_SIZE
                          )
                          .test(
                            "fileFormat",
                            "Unsupported Format",
                            (value) =>
                              value && SUPPORTED_FORMATS.includes(value.type)
                          ),
                        bank_acct_name: Yup.string()
                          .matches(
                            /^[aA-zZ\s]+$/,
                            "Only alphabets are allowed as account name "
                          )
                          .notRequired()
                          .min(
                            4,
                            "Bank account name should be a minimum of 4 characters."
                          ),
                        bank_number: Yup.string()
                          .min(9, "Account number should be 10 characters.")
                          .max(9, "Account number should be 10 characters.")
                          .notRequired(),
                      })}
                    >
                      {({
                        values,
                        touched,
                        errors,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        setFieldValue,
                      }) => (
                        <form
                          className="form"
                          onSubmit={handleSubmit}
                          encType="multipart/form-data"
                        >
                          <div>
                            <h3 className="page-title">Account Information</h3>
                            <span className="form__form-group-label">
                              Contact Number
                            </span>
                            <div
                              className={`${
                                styles.react_tel_input
                              } form__form-group-field ${
                                errors.phone && "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <PhoneIcon />
                              </div>
                              <PhoneInput
                                inputProps={{ name: "phone" }}
                                onChange={(contact_number, country, e) => {
                                  handleChange(e);
                                }}
                                placeholder="Enter contact number"
                                name="contact_number"
                                data-testid="contact_number"
                                value={values.contact_number}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.contact_number &&
                                touched.contact_number &&
                                errors.contact_number}
                            </p>
                          </div>

                          <div className="form__form-group">
                            <span className="form__form-group-label">
                              Contact Email
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.email && "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AlternateEmailIcon />
                              </div>
                              <input
                                name="contact_email"
                                data-testid="contact_email"
                                component="input"
                                type="email"
                                placeholder="Contact Email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.contact_email}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.contact_email &&
                                touched.contact_email &&
                                errors.contact_email}
                            </p>
                          </div>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Office Address
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.office_address &&
                                touched.office_address &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AccountOutlineIcon />
                              </div>
                              <input
                                name="office_address"
                                data-testid="office_address"
                                component="input"
                                type="text"
                                placeholder="Office Address"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.office_address}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.office_address &&
                                touched.office_address &&
                                errors.office_address}
                            </p>
                          </div>

                          <h3 className="page-title">Owner Information</h3>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Owner Name
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.owner_name &&
                                touched.owner_name &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AccountOutlineIcon />
                              </div>
                              <input
                                name="owner_name"
                                data-testid="owner_name"
                                component="input"
                                type="text"
                                placeholder="Owner name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.owner_name}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.owner_name &&
                                touched.owner_name &&
                                errors.owner_name}
                            </p>
                          </div>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Nationality
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.nationality &&
                                touched.nationality &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <PlanetIcon />
                              </div>
                              <input
                                name="nationality"
                                data-testid="nationality"
                                component="input"
                                type="text"
                                placeholder="Nationality"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.nationality}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.nationality &&
                                touched.nationality &&
                                errors.nationality}
                            </p>
                          </div>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Home Address
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.home_address &&
                                touched.home_address &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AccountOutlineIcon />
                              </div>
                              <input
                                name="home_address"
                                data-testid="home_address"
                                component="input"
                                type="text"
                                placeholder="Home Address"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.home_address}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.home_address &&
                                touched.home_address &&
                                errors.home_address}
                            </p>
                          </div>

                          <div className="form__form-group">
                            <span className="form__form-group-label">
                              Identity Type
                            </span>
                            <div
                              className={`${
                                styles.select_form_custom_container
                              } ${
                                errors.identity_type && "form__form-validation"
                              }`}
                            >
                              <div
                                className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                              >
                                <AccountOutlineIcon />
                              </div>
                              <select
                                className={
                                  className == "theme-light"
                                    ? styles.select_form_custom_light
                                    : styles.select_form_custom_dark
                                }
                                name="identity_type"
                                data-testid="identity_type"
                                component="select"
                                type="select"
                                placeholder="Identity Type"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.identity_type}
                              >
                                <option
                                  value={"Drivers Licence"}
                                  label={"Drivers Licence"}
                                />
                                <option
                                  value={"International Passport"}
                                  label={"International Passport"}
                                />
                                <option
                                  value={"National Identity Card"}
                                  label={"National Identity Card"}
                                />
                              </select>
                            </div>
                            <p className="error_validation_text">
                              {errors.identity_type &&
                                touched.identity_type &&
                                errors.identity_type}
                            </p>
                          </div>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Identity File
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.identity_file &&
                                touched.identity_file &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <LinkIcon />
                              </div>
                              <input
                                name="identity_file"
                                data-testid="identity_file"
                                component="input"
                                type="file"
                                placeholder="Identity File"
                                accept="application/pdf,image/*"
                                //onChange={handleChange}
                                onChange={(event) =>
                                  setFieldValue(
                                    "identity_file",
                                    event.currentTarget.files[0]
                                  )
                                }
                                onBlur={handleBlur}
                                //value={values.identity_file}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.identity_file &&
                                touched.identity_file &&
                                errors.identity_file}
                            </p>
                          </div>

                          <h3 className="page-title">Bank Account Details</h3>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Bank Account Name
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.bank_acct_name &&
                                touched.bank_acct_name &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AccountOutlineIcon />
                              </div>
                              <input
                                name="bank_acct_name"
                                data-testid="bank_acct_name"
                                component="input"
                                type="text"
                                placeholder="Bank Account Name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.bank_acct_name}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.bank_acct_name &&
                                touched.bank_acct_name &&
                                errors.bank_acct_name}
                            </p>
                          </div>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Account Number
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.bank_number &&
                                touched.bank_number &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AccountOutlineIcon />
                              </div>
                              <input
                                name="bank_number"
                                data-testid="bank_number"
                                component="input"
                                type="number"
                                min={0}
                                placeholder="Account Number"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.bank_number}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.bank_number &&
                                touched.bank_number &&
                                errors.bank_number}
                            </p>
                          </div>

                          <div className={`form__form-group`}>
                            <span className="form__form-group-label">
                              Bank Name
                            </span>
                            <div
                              className={`form__form-group-field ${
                                errors.bank_name &&
                                touched.bank_name &&
                                "form__form-validation"
                              }`}
                            >
                              <div className="form__form-group-icon">
                                <AccountOutlineIcon />
                              </div>
                              <input
                                name="bank_name"
                                data-testid="bank_name"
                                component="input"
                                type="text"
                                placeholder="Bank Name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.bank_name}
                              />
                            </div>
                            <p className="error_validation_text">
                              {errors.bank_name &&
                                touched.bank_name &&
                                errors.bank_name}
                            </p>
                          </div>

                          <div className={styles.btnContainer}>
                            <button
                              data-testid="save-btn"
                              className={`${styles.saveBtn} btn btn-primary account__btn account__btn--small`}
                            >
                              {isSubmitting ? (
                                <div className={styles.button_elem}>
                                  <Spinner
                                    radius={20}
                                    color={"#333"}
                                    stroke={2}
                                    visible={true}
                                  />
                                </div>
                              ) : (
                                "Save"
                              )}
                            </button>
                          </div>
                        </form>
                      )}
                    </Formik>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  );
};

export default CompliancePage;
