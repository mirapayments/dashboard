import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "reactstrap";
import classNames from "classnames";
import styles from "./Modal.module.css";
import Form from "./Form/Form";
import { useSelector } from "react-redux";

const ModalComponent = ({
  color,
  colored,
  header,
}) => {
  const [modal, setModal] = useState(false);
  const { className } = useSelector((state) => state.theme);

  const toggle = (e) => {
    e.preventDefault();
    setModal((prevState) => !prevState);
  };

  // let Icon = <span className="lnr lnr-pushpin modal__title-icon" />;

  const modalClass = classNames({
    "modal-dialog--colored": colored,
    "modal-dialog--header": header,
  });

  return (
    <div>
      <div
        styles={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <button
          className={`${styles.passwordbtn} btn btn-primary account__btn account__btn--small`}
          color={color}
          onClick={toggle}
        >
          Edit Information
        </button>
      </div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <div className="modal__body">
          <Form setModalOpen={setModal} />
        </div>
      </Modal>
    </div>
  );
};

ModalComponent.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  color: PropTypes.string.isRequired,
  colored: PropTypes.bool,
  header: PropTypes.bool,
  btn: PropTypes.string.isRequired
};

ModalComponent.defaultProps = {
  title: "",
  message: "",
  colored: false,
  header: false,
};

export default ModalComponent;
