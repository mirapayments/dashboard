import React, { useState } from "react";
import PropTypes from "prop-types";
import styles from "./UpdatePasswordForm.module.css";
import { useDispatch } from "react-redux";
import Spinner from "react-spinner-material";
import { setUserPasswordReset } from "../../../../../../redux/features/user/userSlice";
import { changePassword } from "../../../../../../utils/apiHandlers/changePassword";
import EyeIcon from "mdi-react/EyeIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import { Formik } from "formik";
import * as Yup from "yup";
import Swal from "sweetalert2";
import useOnlineStatus from "../../../../../../utils/hooks/useOnlineStatus";

const UpdatePasswordForm = ({ setModal }) => {
  const online = useOnlineStatus();
  const dispatch = useDispatch();
  const [isPasswordShown1, setIsPasswordShown1] = useState(false);
  const [isPasswordShown2, setIsPasswordShown2] = useState(false);
  const [isPasswordShown0, setIsPasswordShown0] = useState(false);

  function showPassword1() {
    setIsPasswordShown1((prev) => !prev);
  }
  function showPassword2() {
    setIsPasswordShown2((prev) => !prev);
  }
  function showPassword0() {
    setIsPasswordShown0((prev) => !prev);
  }

  async function changePasswordHandler(values, setSubmitting) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    try {
      const payload = {
        old_password: values.old_password,
        new_password: values.new_password,
      };

      const response = await changePassword(JSON.stringify(payload)).catch(
        (err) => {
          if (err.status === 400 && err.data.detail) {
            Swal.fire({
              title: "Error",
              text: err.data.detail,
              icon: "error",
              showConfirmButton: false,
              timer: 3000,
            });
            setSubmitting(false);
          }
        }
      );
      if (response.data.status) {
        Swal.fire({
          title: "Password successfully updated",
          text: "Please sign in with your email and password",
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });

        dispatch(setUserPasswordReset(true));
        setSubmitting(false);
        setModal(false);
      }
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <Formik
      initialValues={{
        old_password: "",
        new_password: "",
        confirm_new_password: "",
      }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true);
        changePasswordHandler(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        old_password: Yup.string().required("No password provided."),
        new_password: Yup.string()
          .required("No password provided.")
          .min(8, "Password is too short - should be 8 chars minimum.")
          .matches(/(?=.*[0-9])/, "Password must contain a number.")
          .matches(
            /^.*[!@#$%^&*()_+\-=\\[\]{};':"\\|,.<>\\/?].*$/,
            "Password must contain at least one special character"
          ),
        confirm_new_password: Yup.string().oneOf(
          [Yup.ref("new_password"), null],
          "Passwords must match"
        ),
      })}
    >
      {({ values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit }) =>
        (
          <form className="form" onSubmit={handleSubmit}>
            <div className="form__form-group">
              <span className="form__form-group-label">Enter Old Password</span>
              <div
                className={`form__form-group-field ${
                  errors.old_password && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <KeyVariantIcon />
                </div>
                <input
                  name="old_password"
                  type={isPasswordShown0 ? "text" : "password"}
                  placeholder="Old Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.old_password}
                />
                <button
                  className={`form__form-group-button${
                    isPasswordShown0 ? " active" : ""
                  }`}
                  onClick={() => showPassword0()}
                  type="button"
                >
                  <EyeIcon />
                </button>
              </div>
              <p className="error_validation_text">
                {errors.old_password &&
                  touched.old_password &&
                  errors.old_password}
              </p>
            </div>
            <div className="form__form-group">
              <span className="form__form-group-label">Enter New Password</span>
              <div
                className={`form__form-group-field ${
                  errors.new_password && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <KeyVariantIcon />
                </div>
                <input
                  name="new_password"
                  type={isPasswordShown1 ? "text" : "password"}
                  placeholder="New Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.new_password}
                />
                <button
                  className={`form__form-group-button${
                    isPasswordShown1 ? " active" : ""
                  }`}
                  onClick={() => showPassword1()}
                  type="button"
                >
                  <EyeIcon />
                </button>
              </div>
              <p className="error_validation_text">
                {errors.new_password &&
                  touched.new_password &&
                  errors.new_password}
              </p>
            </div>
            <div className="form__form-group">
              <span className="form__form-group-label">
                Confirm New Password
              </span>
              <div
                className={`form__form-group-field ${
                  errors.confirm_new_password && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <KeyVariantIcon />
                </div>
                <input
                  name="confirm_new_password"
                  type={isPasswordShown2 ? "text" : "password"}
                  placeholder="Confirm New Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.confirm_new_password}
                />
                <button
                  className={`form__form-group-button${
                    isPasswordShown2 ? " active" : ""
                  }`}
                  onClick={() => showPassword2()}
                  type="button"
                >
                  <EyeIcon />
                </button>
              </div>
              <p className="error_validation_text">
                {errors.confirm_new_password &&
                  touched.confirm_new_password &&
                  errors.confirm_new_password}
              </p>
            </div>

            <button
              type="submit"
              disabled={isSubmitting}
              className="btn btn-primary account__btn account__btn--small"
            >
              {isSubmitting ? (
                <div className={styles.button_elem}>
                  <Spinner
                    radius={20}
                    color={"#333"}
                    stroke={2}
                    visible={true}
                  />
                </div>
              ) : (
                "Submit"
              )}
            </button>
          </form>
        )
      }
    </Formik>
  );
};

UpdatePasswordForm.propTypes = {
  setModal: PropTypes.func
};

export default UpdatePasswordForm;
