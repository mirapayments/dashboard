import { render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import CompliancePage from "../index";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <CompliancePage /> renders", () => {
  const store = mockStore(data);
  it("check if <CompliancePage/> contains inputs and save button", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <CompliancePage />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      const contactNumber = screen.getByPlaceholderText("Enter contact number");
      const contactEmail = screen.getByTestId("contact_email");
      const officeAddress = screen.getByTestId("office_address");
      const ownerName = screen.getByTestId("owner_name");
      const nationality = screen.getByTestId("nationality");
      const homeAddress = screen.getByTestId("home_address");
      const identityType = screen.getByTestId("identity_type");
      const identityFile = screen.getByTestId("identity_file");
      const bankAccountName = screen.getByTestId("bank_acct_name");
      const bankNumber = screen.getByTestId("bank_number");
      const bankName = screen.getByTestId("bank_name");
      const saveButton = screen.getByTestId("save-btn");

      expect(contactNumber).toBeInTheDocument();
      expect(contactEmail).toBeInTheDocument();
      expect(officeAddress).toBeInTheDocument();
      expect(ownerName).toBeInTheDocument();
      expect(nationality).toBeInTheDocument();
      expect(homeAddress).toBeInTheDocument();
      expect(identityType).toBeInTheDocument();
      expect(identityFile).toBeInTheDocument();
      expect(bankAccountName).toBeInTheDocument();
      expect(bankNumber).toBeInTheDocument();
      expect(bankName).toBeInTheDocument();
      expect(saveButton).toBeInTheDocument();
    });
    //screen.debug();
  });
});
