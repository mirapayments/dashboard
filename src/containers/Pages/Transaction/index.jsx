import React from "react";
import { Container } from "reactstrap";
import EditableTable from "./EditableTable/index";

const Transaction = () => (
  <Container className="dashboard">
    <EditableTable />
  </Container>
);

export default Transaction;
