import React from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import styles from "./CardContainer.module.css";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";
import { Badge } from "reactstrap";

const ExampleCard = ({ transactionDetails }) => {
  //probably should have used an object for this. Would refactor when I wake up
  function transactionBadge(status) {
    let color = "success";
    switch (status) {
      case "Initiated":
        color = "warning";
        break;
      case "Success":
        color = "success";
        break;
      case "Failed":
        color = "danger";
        break;
      case "Canceled":
        color = "danger";
        break;
      case "Reversed":
        color = "primary";
        break;
      default:
        color = "success";
    }
    return color;
  }

  return (
    transactionDetails && (
      <Col md={12}>
        <Card>
          <CardBody className={styles.card_body}>
            <div className="mb-4">
              <h4>{transactionDetails?.name}</h4>
            </div>
            <div className={styles.details_wrapper}>
              <div>
                <p>Amount</p>
                <p>
                  {transactionDetails?.amount_currency}{" "}
                  {transactionDetails?.amount}
                </p>
              </div>
              <div>
                <p>Status</p>
                <p>
                  <Badge color={transactionBadge(transactionDetails?.status)}>
                    {transactionDetails?.status}
                  </Badge>
                </p>
              </div>
              <div>
                <p>Reference</p>
                <p>{transactionDetails?.reference}</p>
                {console.log(
                  "TRANSACTION DETAILS",
                  transactionDetails.customer
                )}
              </div>
              <div>
                <p>Transaction Fees</p>
                <p>{transactionDetails?.amount}</p>
              </div>
              <div>
                <p>Amount due</p>
                <p>{transactionDetails?.amount_due}</p>
              </div>
              <div>
                <p>Type</p>
                <p>{transactionDetails?.transaction_type}</p>
              </div>
              <div>
                <p>Date</p>
                <p>{formatDate(transactionDetails?.created_at)}</p>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
    )
  );
};

ExampleCard.propTypes = {
  transactionDetails: PropTypes.object
};

export default ExampleCard;
