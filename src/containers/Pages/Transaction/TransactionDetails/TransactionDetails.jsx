import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Col, Container, Row } from "reactstrap";
import TransactionDetails from "./TransactionDetailsComponent/ExampleCard";
import TransactionCustomer from "./TransactionCustomerComponent/ExampleCard";
import { getTransactionDetails } from "../../../../utils/apiHandlers/getTransactionDetails";

const TransactionContainer = () => {
  const [transactionDetails, setTransactionDetails] = useState({});
  const { id } = useParams();

  async function getTransactionDetailsHandler() {
    try {
      const res = await getTransactionDetails(id).catch((err) =>
        console.log(err)
      );

      await setTransactionDetails(res.data.data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getTransactionDetailsHandler();
  }, [id]);

  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          <h3 className="page-title ml-3">Transaction Details</h3>
        </Col>
      </Row>
      <Row>
        <Col sm={6} lg={6} md={12}>
          <TransactionDetails transactionDetails={transactionDetails} />
        </Col>
        <Col sm={6} lg={6} md={12}>
          <TransactionCustomer transactionDetails={transactionDetails} />
        </Col>
      </Row>
    </Container>
  );
};

export default TransactionContainer;
