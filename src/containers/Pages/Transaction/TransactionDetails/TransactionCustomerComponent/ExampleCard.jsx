import React from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import styles from "./CardContainer.module.css";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";

const ExampleCard = ({ transactionDetails }) => {
  const { customer } = transactionDetails;
  return (
    <Col md={12}>
      <Card>
        <CardBody className={styles.card_body}>
          <div className="mb-4">
            <h4 className="mb-4">Customer</h4>
          </div>
          <div className={styles.details_wrapper}>
            <div>
              <p>Full Name</p>
              <p>{customer?.full_name}</p>
            </div>
            <div>
              <p>Email</p>
              <p>{customer?.email}</p>
            </div>
            <div>
              <p>Phone</p>
              <p>{customer?.phone}</p>
            </div>
            <div>
              <p>Date</p>
              <p>{formatDate(customer?.created_at)}</p>
            </div>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

ExampleCard.propTypes = {
  transactionDetails: PropTypes.object
};

export default ExampleCard;
