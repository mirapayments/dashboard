import { useMemo } from "react";

const CreateTableData = () => {
  const columns = useMemo(
    () => [
      {
        Header: "Status",
        accessor: "status",
        disableGlobalFilter: true,
      },
      {
        Header: "Amount",
        accessor: "amount",
        width: 200,
      },

      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "Reference",
        accessor: "reference",
      },
      {
        Header: "Type",
        accessor: "transaction_type",
        disableGlobalFilter: true,
      },
      {
        Header: "Date",
        accessor: "created_at",
        disableGlobalFilter: true,
      },
    ],
    []
  );

  const reactTableData = { tableHeaderData: columns };
  return reactTableData;
};

export default CreateTableData;
