import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect, useSelector } from "react-redux";
import ReactTableDnDBody from "./ReactTableDnDBody";
import NoCustomers from "./undraw_upload_87y9 1.svg";
import styles from "./ReactTableBody.module.css";
import { ThemeProps } from "../../../../../shared/prop-types/ReducerProps";
import { useHistory } from "react-router";
import Skeleton from "react-loading-skeleton";

const ReactTableDefaultBody = ({ page, getTableBodyProps, prepareRow }) => {
  const history = useHistory();
  const { transactionListLoading } = useSelector((state) => state.ui);

  return (
    <>
      <tbody className="table table--bordered" {...getTableBodyProps()}>
        {transactionListLoading ? (
          <tr>
            <td style={{ height: "40vh" }}>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
          </tr>
        ) : page.length > 0 ? (
          page.map((row, index) => {
            prepareRow(row);
            // page[index]?.original.code = `<a style={{color:"blue",width:"100%"}} target="_blank" href={http://www.mirapayments.com/pay/${page[index]?.original.code}}>View</a>`
            return (
              <tr
                key={index}
                className={styles.row_style}
                data-testid="table-row"
                onClick={() =>
                  history.push(`/pages/transaction/${page[index]?.original.id}`)
                }
                {...row.getRowProps()}
              >
                {row.cells.map((cell, index) => (
                  <>
                    <td key={index} {...cell.getCellProps()}>
                      {cell.render("Cell")}
                    </td>
                  </>
                ))}
              </tr>
            );
          })
        ) : (
          <>
            <tr>
              <td
                colSpan={6}
                align="center"
                style={{
                  height: "40vh",
                  verticalAlign: "middle",
                  left: "20px",
                }}
              >
                <img
                  src={NoCustomers}
                  width="20%"
                  height="150px"
                  alt="No customer"
                />
                <p>You have no transactions yet.</p>
                <p>Go ahead and create your payment links and start selling.</p>
              </td>
            </tr>
          </>
        )}
      </tbody>
    </>
  );
};

ReactTableDefaultBody.propTypes = {
  page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getTableBodyProps: PropTypes.func.isRequired,
  prepareRow: PropTypes.func.isRequired,
};

const ReactTableBody = ({
  page,
  getTableBodyProps,
  prepareRow,
  withDragAndDrop,
  updateDraggableData,
  theme,
}) => (
  <Fragment>
    {withDragAndDrop ? (
      <ReactTableDnDBody
        page={page}
        getTableBodyProps={getTableBodyProps}
        prepareRow={prepareRow}
        updateDraggableData={updateDraggableData}
        theme={theme}
      />
    ) : (
      <ReactTableDefaultBody
        page={page}
        getTableBodyProps={getTableBodyProps}
        prepareRow={prepareRow}
      />
    )}
  </Fragment>
);

ReactTableBody.propTypes = {
  page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getTableBodyProps: PropTypes.func.isRequired,
  prepareRow: PropTypes.func.isRequired,
  updateDraggableData: PropTypes.func.isRequired,
  withDragAndDrop: PropTypes.bool.isRequired,
  theme: ThemeProps.isRequired,
};

export default connect((state) => ({
  theme: state.theme,
}))(ReactTableBody);
