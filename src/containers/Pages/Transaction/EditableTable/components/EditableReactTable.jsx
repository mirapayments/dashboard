import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import { getTransactions } from "../../../../../utils/apiHandlers/getTransactions";
import { exportTransactions } from "../../../../../utils/apiHandlers/exportTransactions";
import ReactTableBase from "./ReactTableBase";
import ReactTableCustomizer from "./ReactTableCustomizer";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";
import { useSelector } from "react-redux";
import FileDownloadOutlineIcon from "mdi-react/CloudDownloadIcon";
import { setTransactionListLoading } from "../../../../../redux/features/user/uiSlice";
import { useDispatch } from "react-redux";

import { CSVLink } from "react-csv";

const EditableReactTable = ({ reactTableData }) => {
  const [rows, setData] = useState([]);
  const [withPagination, setWithPaginationTable] = useState(false);
  const [isSortable, setIsSortable] = useState(true);
  const [withSearchEngine, setWithSearchEngine] = useState(false);
  const [csv, setCsv] = useState(null);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const csvLinkRef = useRef();

  useEffect(() => {
    dispatch(setTransactionListLoading(true));
    getTransactionsHandler(signedInAccountNumber);
  }, [signedInAccountNumber]);

  const handleExportTransactions = async () => {
    try {
      const response = await exportTransactions(signedInAccountNumber).catch((error) => {
        console.log(error);
      });
      let result = await response;
      setCsv(result.data);
      setTimeout(()=> setCsv(null), 1000);
      csvLinkRef.current.link.click();
    } catch (error) {
      console.log(error);
    }
  };

  async function getTransactionsHandler(account_number) {
    try {
      const response = await getTransactions(account_number).catch((err) => {
        console.log(err);
      });
      //format date before setting state
      const data = response.data.data;
      const modified = data.map((item) => {
        return {
          ...item,
          created_at: formatDate(item.created_at),
          amount: item.amount_currency + " " + item.amount,
          reference: item.reference.slice(0, 10) + "...",
        };
      });
      setData(modified);
      dispatch(setTransactionListLoading(false));
    } catch (error) {
      console.log(error);
    }
  }

  const updateEditableData = (rowIndex, columnId, value) => {
    setData((old) =>
      old.map((item, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          };
        }
        return item;
      })
    );
  };

  const handleClickIsSortable = () => {
    setIsSortable(!isSortable);
  };

  const handleClickWithPagination = () => {
    setWithPaginationTable(!withPagination);
  };

  const handleClickWithSearchEngine = () => {
    setWithSearchEngine(!withSearchEngine);
  };

  const tableConfig = {
    isEditable: true,
    isSortable: true,
    isResizable: false,
    withPagination: true,
    withSearchEngine: true,
    manualPageSize: [10, 20, 30, 40],
    placeholder: "Search by Description, Reference or Amount",
  };

  return (
    <Col md={12} lg={12}>
      {csv? <CSVLink  ref={csvLinkRef} data={csv} target="_blank" filename={"transactions.csv"} /> : null }
      <Card>
        <CardBody>
          <div className="react-table__wrapper">
            <div className="card__title">
              <h5 className="subhead">
                <span className="red-text"></span>
              </h5>
            </div>

            <ReactTableCustomizer
              handleClickIsSortable={handleClickIsSortable}
              handleClickWithPagination={handleClickWithPagination}
              handleClickWithSearchEngine={handleClickWithSearchEngine}
              isSortable={isSortable}
              withPagination={withPagination}
              withSearchEngine={withSearchEngine}
            />
            <>
              <button
                type="submit"
                style={{ top: "57px" }}
                className="icon btn btn-primary mt-4"
                onClick={handleExportTransactions}
                disabled={rows.length > 0? false : true}
              >
                <FileDownloadOutlineIcon className="mb-1" fontSize="large" />{" "}
                Export{" "}
              </button>
            </>
          </div>
          <ReactTableBase
            key={withSearchEngine ? "searchable" : "common"}
            columns={reactTableData.tableHeaderData}
            data={rows}
            updateEditableData={updateEditableData}
            tableConfig={tableConfig}
          />
        </CardBody>
      </Card>
    </Col>
  );
};

EditableReactTable.propTypes = {
  reactTableData: PropTypes.shape({
    tableHeaderData: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        name: PropTypes.string,
      })
    ),
    tableRowsData: PropTypes.arrayOf(PropTypes.shape()),
  }).isRequired,
};
export default EditableReactTable;
