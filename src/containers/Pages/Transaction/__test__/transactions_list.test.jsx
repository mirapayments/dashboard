import "regenerator-runtime/runtime";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import Transaction from "../index";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <Transactions /> renders", () => {
  const store = mockStore(data);
  it("check if contains transactions list", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Transaction />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      const tableRow = screen.getAllByTestId("table-row");
      expect(tableRow.length).toBe(7);
    });
  });
});
