import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import TransactionContainer from "../TransactionDetails/TransactionDetails";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <TransactionDetails /> renders", () => {
  const store = mockStore(data);
  it("check if <TransactionDetails /> contains data", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <TransactionContainer />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(screen.getByText("5000.00")).toBeInTheDocument();
      expect(
        screen.getByText("92572350618431526308444512129844")
      ).toBeInTheDocument();
      expect(screen.getByText("Success")).toBeInTheDocument();

      expect(screen.getByText("yekinnijibola@gmail.com")).toBeInTheDocument();
      //   expect(screen.getByText("+234 234 903 956 187")).toBeInTheDocument();
    });
  });
});
