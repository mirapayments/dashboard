import "regenerator-runtime/runtime";
import {
  fireEvent,
  getAllByText,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import BalancePage from "../index";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <BalancePage /> renders", () => {
  const store = mockStore(data);
  it("check modals and forms", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <BalancePage />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      const topupLink = screen.getByTestId("topup-link");
      const payoutsLink = screen.getByTestId("payouts-link");

      expect(
        screen.getByText("This is your holding balance")
      ).toBeInTheDocument();
      expect(
        screen.getByText("Money here is available for transfers and spend.")
      ).toBeInTheDocument();

      fireEvent.click(screen.getByTestId("topup-btn"));

      expect(screen.getAllByText("Kindly fund your wallet").length).toBe(2);

      fireEvent.click(screen.getByTestId("modal"));
      fireEvent.click(topupLink);

      const topupRow = screen.getAllByTestId("topup-row");
      expect(topupRow.length).toBe(7);

      fireEvent.click(payoutsLink);

      expect(
        screen.getByText("You do not have any transactions yet.")
      ).toBeInTheDocument();

      fireEvent.click(screen.getByText("Pay Out"));

      expect(screen.getByLabelText("Account Name")).toBeInTheDocument();
      expect(screen.getByLabelText("Account Number")).toBeInTheDocument();
      expect(screen.getByLabelText("Bank")).toBeInTheDocument();
      expect(screen.getByLabelText("Amount")).toBeInTheDocument();
    });
  });
});
