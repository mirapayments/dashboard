import React from 'react';
import { useState, useEffect } from 'react';
import { ModalBody } from 'reactstrap';
import PropTypes from 'prop-types';
// import Swal from 'sweetalert2';
import { getAccountBalance } from "../../../../../../utils/apiHandlers/getAccountBalance";
import { useSelector } from 'react-redux';


import CurrencyInput from 'react-currency-input-field';

// Modal Content Component.
const ModalContent = ({ setModal }) => {
  const [accountDetails, setAccountDetails] = useState({});
  const { signedInAccountNumber } = useSelector((state) => state.user);

  async function getAccountBalanceHandler() {
    try {
      const resp = await getAccountBalance(signedInAccountNumber).catch((error) => console.log(error));
      setAccountDetails(resp.data.data);
      console.log(resp);
      console.log(accountDetails);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getAccountBalanceHandler();
  }, [signedInAccountNumber]);

  setTimeout(() => {
    setModal(false);
  }, 10000000);
  return (
    <ModalBody>
      <form className="form d-block">
        <div className="form__form-group">
          <label htmlFor="account_name" className="form__form-group-label">
            Account Name
          </label>
          <input type="text" name="account_name" id="account_name" placeholder="Account Name" />
        </div>
        <div className="form__form-group">
          <label htmlFor="account_number" className="form__form-group-label">
            Account Number
          </label>
          <input
            type="text"
            name="account_number"
            id="account_number"
            placeholder="Account Number"
          />
        </div>
        <div className="form__form-group-select">
          <label htmlFor="bank">Bank</label>
          <select name="bank" className="Select-input Select-control" id="bank">
            <option value="">Select a bank</option>
            <option value="NGN">Kuda bank</option>
            <option value="USD">First bank</option>
            <option value="GBP">Guaranty Trust Bank</option>
            <option value="GBP">Access Bank</option>
            <option value="GBP">Wema Bank</option>
            <option value="GBP">Sterling Bank</option>
            <option value="GBP">UBA</option>
            <option value="GBP">Zenith Bank</option>
          </select>
        </div>
        <div className="form__form-group">
          <label htmlFor="amount" className="form__form-group-label">
            Amount
          </label>
          <CurrencyInput
            id="amount"
            name="amount"
            placeholder="₦ 50,000"
            prefix="&#8358;"
            decimalsLimit={2}
          />
        </div>

        <div>
          <button type='button' className='btn btn-primary w-100 mt-3'>Pay</button>
        </div>
      </form>
    </ModalBody>
  );
};

// Props validation
ModalContent.propTypes = {
  setModal: PropTypes.func,
};

// Export component
export default ModalContent;
