import React from "react";
import { Col, Container } from "reactstrap";
// import ExampleCard from "./Payout/PayoutCard";
import TabContainer from "../Balance/Tab/Tabs";

const BalancePage = () => (
  <Container className="dashboard">
    <Col md={12}>
      <h3 className="page-title">Balance</h3>
    </Col>
    <TabContainer />
  </Container>
);

export default BalancePage;
