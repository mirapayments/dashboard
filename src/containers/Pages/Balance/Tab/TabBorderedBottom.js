import React from "react";
import { useTranslation } from "react-i18next";
import { Card, CardBody, Col } from "reactstrap";
import Tabs from "./Tabs";

const TabsBorderedBottom = () => {
  const { t } = useTranslation("common");

  return (
    <Col>
      <Card>
        <CardBody>
          <div className="tabs tabs--justify tabs--bordered-bottom">
            <Tabs />
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

export default TabsBorderedBottom;
