import React from "react";
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from "reactstrap";
import classnames from "classnames";
// import ListAccounts from "../components/ListAccount";
// import styles from "../components/ListAccount.module.css";
// import ListUserTiedToAccount from "../Users/Users";
import { useDispatch, useSelector } from "react-redux";
// import { useLocation } from "react-router";
// import PermissionPage from "../components/RolesPermission";
import PayoutCard from "../Payout/PayoutCard";
import TopupCard from "../Topup/TopupCard";
import BalanceCard from "../Balance/BalanceCard";
import styles from "./tab.module.css";
import { setBalanceTabPosition } from "../../../../redux/features/user/uiSlice";

const Tabs = () => {
  const dispatch = useDispatch();
  const { balanceTabPosition } = useSelector((state) => state.ui);
  const toggle = (tab) => {
    if (balanceTabPosition !== tab) dispatch(setBalanceTabPosition(tab));
  };

  return (
    <div>
      <Nav className={styles.tab} tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: balanceTabPosition === "1" })}
            data-testid="balance-link"
            onClick={() => toggle("1")}
          >
            Balance
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: balanceTabPosition === "2" })}
            data-testid="topup-link"
            onClick={() => toggle("2")}
          >
            Top Up
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: balanceTabPosition === "3" })}
            data-testid="payouts-link"
            onClick={() => toggle("3")}
          >
            Payouts
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={balanceTabPosition}>
        <TabPane tabId="1">
          <BalanceCard />
        </TabPane>
        <TabPane tabId="2">
          <TopupCard />
        </TabPane>
        <TabPane tabId="3">
          <PayoutCard />
        </TabPane>
      </TabContent>
    </div>
  );
};

export default Tabs;
