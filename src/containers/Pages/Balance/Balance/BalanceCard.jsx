import React, { useState, useEffect} from 'react';
import { Card, CardBody, CardHeader, Col, Row, Container } from 'reactstrap';
import ModalComponent from './ModalComponent';
import { getAccountBalance } from '../../../../utils/apiHandlers/getAccountBalance';
import { useSelector } from 'react-redux';

const BalanceCard = () => {

  const [accountDetails, setAccountDetails] = useState({});
  const { signedInAccountNumber } = useSelector((state) => state.user);

  async function getAccountBalanceHandler() {
    try {
      const resp = await getAccountBalance(signedInAccountNumber).catch((error) =>
        console.log(error)
      );
      setAccountDetails(resp.data.data);
      console.log(resp);
      console.log(accountDetails);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getAccountBalanceHandler();
  }, [signedInAccountNumber]);

  return (
    <>
      <Col md={12}>
        <Card>
          <CardHeader className="white-background">
            <div className="d-flex justify-content-between align-items-center">
              <div>
                <h4>NGN Wallet</h4>
              </div>
              <div className="d-flex align-items-center">
                <p className="mr-2">Low limit: Not Available</p>
                <button type="button" className="btn btn-light py-2 border-info mb-0">
                  Edit Limit
                </button>
                <ModalComponent />
              </div>
            </div>
          </CardHeader>
          <CardBody>
            <Container>
              <Row>
                <Col md={6} className="border-right">
                  <h4>Total Balance</h4>
                  <p className="bold-text">{accountDetails.balance_currency} {accountDetails.balance}</p>
                  <span>This is your holding balance</span>
                </Col>
                <Col md={6}>
                  <h4>Available Balance</h4>
                  <p className="bold-text">{accountDetails.balance_currency} {accountDetails.balance}</p>
                  <span>Money here is available for transfers and spend.</span>
                </Col>
                {/* <Col md={4}>
                  <h4>Bank Account</h4>
                  <p className="bold-text">NGN 0.00</p>
                  <span>This is your holding balance</span>
                </Col> */}
              </Row>
            </Container>
          </CardBody>
        </Card>
      </Col>
    </>
  );
};


export default BalanceCard;
