import React from 'react';
import { useState, useEffect } from 'react';
import { ModalBody } from 'reactstrap';
import PropTypes from 'prop-types';
// import Swal from 'sweetalert2';
import { getAccountBalance } from '../../../../utils/apiHandlers/getAccountBalance';
import { useSelector } from 'react-redux';

// Modal Content Component.

const ModalContent = ({ setModal, modalClass, color }) => {
  const [accountDetails, setAccountDetails] = useState({});
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const { className } = useSelector((state) => state.theme);

  async function getAccountBalanceHandler(account_number) {
    try {
      const resp = await getAccountBalance(account_number).catch((error) =>
        console.log(error)
      );
      setAccountDetails(resp.data.data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getAccountBalanceHandler(signedInAccountNumber);
  }, [signedInAccountNumber]);

  setTimeout(() => {
    setModal(false);
  }, 10000000);
  return (
    <ModalBody className={`modal-dialog--${color} ${modalClass}`}>
      <form action="" className={`form d-block ${className !== 'theme-light' }`}>
        <div>
          <h4>Kindly fund your wallet</h4>
          <p>Which balance do you want to fund?</p>
          <div className="form__form-group-select">
            <label htmlFor="currency">Currency</label>
            <select name="currency" className="Select-input Select-control" id="currency">
              <option value="">Select a currency</option>
              <option value={accountDetails.balance_currency}>{accountDetails.balance_currency}</option>
              
            </select>
          </div>
        </div>

        <div>
          <h4>Kindly fund your wallet</h4>
          <p>
            Transfer the amount you want to fund to the account details below and your balance will
            be funded
          </p>
        </div>
        <div className="bg-light my-3 py-3 rounded">
          <div className="d-flex justify-content-between align-items-center px-3">
            <div>
              <span className="d-block">Wema Bank PLC</span>
              <span className="d-block">5055839221</span>
              <span className="d-block">Miracle</span>
            </div>
            <button className="btn btn-sm btn-primary">Copy</button>
          </div>
        </div>
      </form>
    </ModalBody>
  );
};

// Props validation
ModalContent.propTypes = {
  setModal: PropTypes.func,
  modalClass: PropTypes.object,
  color: PropTypes.bool,
};

// Export component
export default ModalContent;
