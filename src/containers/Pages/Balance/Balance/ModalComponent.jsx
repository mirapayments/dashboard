import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ModalHeader, Modal } from 'reactstrap';
import ModalContent from './ModalContent';
import { useSelector } from 'react-redux';

const ModalComponent = ({ color, colored, header }) => {
  const [modal, setModal] = useState(false);
  const { className } = useSelector((state) => state.theme);

  const toggle = () => {
    setModal(!modal);
  };

  const modalClass = classNames({
    'modal-dialog--colored': colored,
    'modal-dialog--header': header,
  });

  return (
    <>
      <button data-testid="topup-btn" type="button" color={color} onClick={toggle} className="btn btn-primary px-5 mb-0">
        Top Up
      </button>

      <Modal
        isOpen={modal}
        data-testid="modal"
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <ModalHeader toggle={toggle} className={`modal-dialog--${color} ${modalClass}`}>
          <h3>TopUp Balance</h3>
        </ModalHeader>
        <div className="modal__body">
          <ModalContent setModal={setModal} modalClass={modalClass} color={color} />
        </div>
      </Modal>
    </>
  );
};

ModalComponent.propTypes = {
  color: PropTypes.string,
  colored: PropTypes.bool,
  header: PropTypes.bool,
};

ModalComponent.defaultProps = {
  title: '',
  message: '',
  colored: false,
  header: false,
};

export default ModalComponent;
