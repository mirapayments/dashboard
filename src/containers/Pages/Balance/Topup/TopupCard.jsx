import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import CreateTableData from './table/CreateData';
import EditableReactTable from './table/EditableReactTable';

const TopUpCard = () => {

  const reactTableData = CreateTableData();

  return (
    <Col md={12}>
      <Card>
        <CardBody className='p-0'>
          <EditableReactTable reactTableData={reactTableData} />
        </CardBody>
      </Card>
    </Col>
  );
};

export default TopUpCard;
