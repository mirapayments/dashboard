import { useMemo } from "react";

const CreateTableData = () => {
  const columns = useMemo(
    () => [
      {
        Header: "Status",
        accessor: "status",
        disableGlobalFilter: true,
      },
      {
        Header: "Reference",
        accessor: "reference",
      },
      {
        Header: "Amount",
        accessor: "amount",
      },
      {
        Header: "Type",
        accessor: "transaction_type",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "Date",
        accessor: "created_at",
      },
    ],
    []
  );

  const reactTableData = { tableHeaderData: columns };
  return reactTableData;
};

export default CreateTableData;
