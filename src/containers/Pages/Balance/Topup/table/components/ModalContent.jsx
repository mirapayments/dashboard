import React from 'react';
import { ModalBody } from 'reactstrap';
import PropTypes from 'prop-types';


// Modal Content Component.

const ModalContent = ({ setModal }) => {
  setTimeout(() => {
    setModal(false);
  }, 10000000);
  return (
    <ModalBody>
      <form action="" className='form d-block'>
        <div>
          <h4>Kindly fund your wallet</h4>
          <p>Which balance do you want to fund?</p>
          <div className="form__form-group-select">
            <label htmlFor="currency">
              Currency
            </label>
            <select name="currency" className="Select-input Select-control" id='currency'>
              <option value="">Select a currency</option>
              <option value="NGN">NGN</option>
              <option value="USD">USD</option>
              <option value="GBP">GBP</option>
            </select>
          </div>
        </div>

        <div>
          <h4>Kindly fund your wallet</h4>
          <p>
            Transfer the amount you want to fund to the account details below and your balance will be
            funded
          </p>
        </div>
        <div className="bg-light my-3 py-3 rounded">
          <div className="d-flex justify-content-between align-items-center px-3">
              <div>
                  <span className="d-block">Wema Bank PLC</span>
                  <span className="d-block">5055839221</span>
                  <span className="d-block">Miracle</span>
              </div>
            <button className="btn btn-sm btn-primary">Copy</button>
          </div>
        </div>
      </form>
    </ModalBody>
  );
};

// Props validation
ModalContent.propTypes = {
  setModal: PropTypes.func,
};

// Export component
export default ModalContent;
