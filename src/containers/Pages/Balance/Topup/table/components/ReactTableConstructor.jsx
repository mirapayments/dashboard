import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from "classnames";
import ModalContent from './ModalContent';
import { useSelector } from 'react-redux';
import {
  useTable,
  useGlobalFilter,
  usePagination,
  useSortBy,
  useResizeColumns,
  useRowSelect,
} from 'react-table';
import ReactTableHeader from './ReactTableHeader';
import BodyReactTable from './ReactTableBody';
import ReactTableFooter from './ReactTableFooter';
import ReactTableFilter from './ReactTableFilter';
import ReactTablePagination from './ReactTablePagination';
import { CardHeader, InputGroup, InputGroupText, Modal, ModalHeader } from 'reactstrap';
import Select from 'react-select';

const ReactTableConstructor = ({ tableConfig, tableOptions, tableOptionalHook, color, colored, header }) => {

  const [modal, setModal] = useState(false);
  const { className } = useSelector((state) => state.theme);

  const toggle = () => {
    setModal(!modal);
  };

  const modalClass = classNames({
    'modal-dialog--colored': colored,
    'modal-dialog--header': header,
  });

  const {
    isEditable,
    isResizable,
    isSortable,
    withPagination,
    withSearchEngine,
    manualPageSize,
    placeholder,
  } = tableConfig;
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    state,
    rows,
    prepareRow,
    page,
    pageCount,
    pageOptions,
    gotoPage,
    previousPage,
    canPreviousPage,
    nextPage,
    canNextPage,
    setPageSize,
    setGlobalFilter,
    withDragAndDrop,
    updateDraggableData,
    updateEditableData,
    dataLength,
    state: { pageIndex, pageSize },
  } = useTable(
    tableOptions,
    useGlobalFilter,
    useSortBy,
    usePagination,
    useResizeColumns,
    useRowSelect,
    ...tableOptionalHook
  );

  const options = [
    { value: 'Today', label: 'Today' },
    { value: 'This week', label: 'This week' },
    { value: 'This month', label: 'This month' },
    { value: 'Last 30 days', label: 'Last 30 days' },
    { value: 'Last 90 days', label: 'Last 90 days' },
    { value: 'All time', label: 'All time' },
    { value: 'Custom period', label: 'Custom period' },
  ];

  return (
    <div>
      <CardHeader className="white-background d-flex align-items-center justify-content-end">
        <div className="d-flex align-items-center justify-content-between">
          <div className="mr-2">
            <InputGroup>
              <InputGroupText>Filter:</InputGroupText>
              <Select options={options} defaultValue={options[0]} />
            </InputGroup>
          </div>
          {withSearchEngine && (
            <ReactTableFilter
              rows={rows}
              setGlobalFilter={setGlobalFilter}
              setFilterValue={tableOptions.setFilterValue}
              globalFilter={state.globalFilter}
              placeholder={placeholder}
              dataLength={dataLength}
            />
          )}
          <button type='button' onClick={toggle} color={color} className="btn btn-primary py-2 mb-0 text-nowrap text-center">Top Up</button>
        </div>
      </CardHeader>

      <Modal
        isOpen={modal}
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
        fullscreen="md"
      >
        <ModalHeader toggle={toggle}>
          <h3>TopUp Balance</h3>
        </ModalHeader>
        <div className="modal__body">
          <ModalContent setModal={setModal} />
        </div>
      </Modal>


      <div
        className={withPagination ? 'table react-table' : 'table react-table table--not-pagination'}
      >
        <table
          {...getTableProps()}
          className={isEditable ? 'react-table editable-table' : 'react-table resizable-table'}
        >
          <ReactTableHeader
            headerGroups={headerGroups}
            isSortable={isSortable}
            isResizable={isResizable}
          />
          <BodyReactTable
            page={page}
            getTableBodyProps={getTableBodyProps}
            prepareRow={prepareRow}
            updateDraggableData={updateDraggableData}
            updateEditableData={updateEditableData}
            isEditable={isEditable}
            withDragAndDrop={withDragAndDrop}
          />
          {(pageCount === pageIndex + 1 || (!withPagination && rows.length !== 0)) && (
            <ReactTableFooter footerGroups={footerGroups} />
          )}
        </table>
      </div>
      {withPagination && rows.length > 0 && (
        <ReactTablePagination
          page={page}
          gotoPage={gotoPage}
          previousPage={previousPage}
          nextPage={nextPage}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          pageOptions={pageOptions}
          pageSize={pageSize}
          pageIndex={pageIndex}
          pageCount={pageCount}
          setPageSize={setPageSize}
          manualPageSize={manualPageSize}
          dataLength={dataLength}
        />
      )}
    </div>
  );
};


ReactTableConstructor.propTypes = {
  color: PropTypes.string,
  colored: PropTypes.bool,
  header: PropTypes.bool,
  tableConfig: PropTypes.shape({
    isEditable: PropTypes.bool,
    isResizable: PropTypes.bool,
    isSortable: PropTypes.bool,
    withDragAndDrop: PropTypes.bool,
    withPagination: PropTypes.bool,
    withSearchEngine: PropTypes.bool,
    manualPageSize: PropTypes.arrayOf(PropTypes.number),
    placeholder: PropTypes.string,
  }),
  tableOptions: PropTypes.shape({
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        name: PropTypes.string,
      })
    ),
    data: PropTypes.arrayOf(PropTypes.shape()),
    setFilterValue: PropTypes.func,
    updateDraggableData: PropTypes.func,
    updateEditableData: PropTypes.func,
    defaultColumn: PropTypes.oneOfType([
      PropTypes.any,
      PropTypes.shape({
        Cell: PropTypes.func,
      }).isRequired,
    ]),
    isEditable: PropTypes.bool,
    withDragAndDrop: PropTypes.bool,
    dataLength: PropTypes.number,
  }),
  tableOptionalHook: PropTypes.arrayOf(PropTypes.func).isRequired,
};

ReactTableConstructor.defaultProps = {
  title: '',
  message: '',
  colored: false,
  header: false,
  tableConfig: {
    isEditable: false,
    isResizable: false,
    isSortable: false,
    withDragAndDrop: false,
    withPagination: false,
    withSearchEngine: false,
    manualPageSize: [10, 20, 30, 40],
    placeholder: 'Search...',
  },
  tableOptions: [
    {
      columns: [],
      data: [],
      setFilterValue: () => {},
      updateDraggableData: () => {},
      updateEditableData: () => {},
      defaultColumn: [],
      withDragAndDrop: false,
      dataLength: null,
      disableSortBy: false,
      manualSortBy: false,
      manualGlobalFilter: false,
      manualPagination: false,
    },
  ],
};

export default ReactTableConstructor;
