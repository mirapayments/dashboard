// Import the necessary libraries and dependencies.
import React, { useState } from "react";
import { PropTypes } from "prop-types";
import { Card, CardBody, Col } from "reactstrap";

// Import Components.
import ReactTableCustomizer from "./components/ReactTableCustomizer";
import ReactTableBase from "./components/ReactTableBase";

// EditableReactTable Components.
const EditableReactTable = ({ reactTableData }) => {
  const [rows, setData] = useState([]);
  const [withPagination, setWithPaginationTable] = useState(true);
  const [isSortable, setIsSortable] = useState(true);
  const [withSearchEngine, setWithSearchEngine] = useState(true);

  // Update the table for every new column.
  const updateEditableData = (rowIndex, columnId, value) => {
    setData((old) =>
      old.map((item, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          };
        }
        return item;
      })
    );
  };

  // Make table sortable, with pagination and with search engine.

  const handleClickIsSortable = () => {
    setIsSortable(!isSortable);
  };

  const handleClickWithPagination = () => {
    setWithPaginationTable(!withPagination);
  };

  const handleClickWithSearchEngine = () => {
    setWithSearchEngine(!withSearchEngine);
  };

  // Set the table config object.

  const tableConfig = {
    isEditable: true,
    isSortable: true,
    isResizable: true,
    withPagination: true,
    withSearchEngine: true,
    manualPageSize: [10, 20, 30, 40],
    placeholder: "Search by name",
  };

  return (
    <Col md={12} lg={12}>
      <Card className="p-0">
        <CardBody className="p-0">
          <div>
            <ReactTableCustomizer
              className="float-left p-0"
              handleClickIsSortable={handleClickIsSortable}
              handleClickWithPagination={handleClickWithPagination}
              handleClickWithSearchEngine={handleClickWithSearchEngine}
              isSortable={isSortable}
              withPagination={withPagination}
              withSearchEngine={withSearchEngine}
            />
            <ReactTableBase
              key={withSearchEngine ? "searchable" : "common"}
              columns={reactTableData.tableHeaderData}
              data={rows}
              updateEditableData={updateEditableData}
              tableConfig={tableConfig}
            />
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

// PropTypes Validation.
EditableReactTable.propTypes = {
  reactTableData: PropTypes.shape({
    tableHeaderData: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        name: PropTypes.string,
      })
    ),
    tableRowsData: PropTypes.arrayOf(PropTypes.shape()),
  }).isRequired,
};

// Export the Component.
export default EditableReactTable;
