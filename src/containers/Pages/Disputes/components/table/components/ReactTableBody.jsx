import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect, useSelector } from 'react-redux';
import { ThemeProps } from '../../../../../../shared/prop-types/ReducerProps';
import Image from '../../../../../../shared/img/emptyTableImage.svg';
import ReactTableDnDBody from './ReactTableDnDBody';
import Skeleton from "react-loading-skeleton";  

const ReactTableDefaultBody = ({ page, getTableBodyProps, prepareRow }) => {
  const { disputesListLoading } = useSelector((state) => state.ui);
  return (
    <>
      <tbody className="table table--bordered" {...getTableBodyProps()}>
        {disputesListLoading ? (
          <tr>
            <td>
              <Skeleton count={9} width="8%" style={{ marginTop: '20px' }} />
            </td>
            <td>
              <Skeleton count={9} width="50%" style={{ marginTop: '20px' }} />
            </td>
            <td>
              <Skeleton count={9} width="50%" style={{ marginTop: '20px' }} />
            </td>
            <td>
              <Skeleton count={9} width="50%" style={{ marginTop: '20px' }} />
            </td>
          </tr>
        ) : page.length > 0 ? (
          page.map((row, index) => {
            prepareRow(row);
            return (
              <tr key={index} {...row.getRowProps()}>
                {row.cells.map((cell, index) => (
                  <td {...cell.getCellProps()} key={index}>
                    {cell.render('Cell')}
                  </td>
                ))}
              </tr>
            );
          })
        ) : (
          <>
            <tr>
              <td
                colSpan={5}
                align="center"
                style={{
                  height: '40vh',
                  verticalAlign: 'middle',
                  left: '20px',
                }}
              >
                <img src={Image} width="20%" height="150px" alt="No disputes" />
                <span className="d-block">You do not have any disputes yet.</span>
                <span className="d-block">
                  There are presently no disputes yet, all of your customers are satisfied. Keep it Up.
                </span>
              </td>
            </tr>
          </>
        )}
      </tbody>
    </>
  );
};

ReactTableDefaultBody.propTypes = {
  page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getTableBodyProps: PropTypes.func.isRequired,
  prepareRow: PropTypes.func.isRequired,
};

const ReactTableBody = ({
  page,
  getTableBodyProps,
  prepareRow,
  withDragAndDrop,
  updateDraggableData,
  theme,
}) => (
  <Fragment>
    {withDragAndDrop ? (
      <ReactTableDnDBody
        page={page}
        getTableBodyProps={getTableBodyProps}
        prepareRow={prepareRow}
        updateDraggableData={updateDraggableData}
        theme={theme}
      />
    ) : (
      <ReactTableDefaultBody
        page={page}
        getTableBodyProps={getTableBodyProps}
        prepareRow={prepareRow}
      />
    )}
  </Fragment>
);

ReactTableBody.propTypes = {
  page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getTableBodyProps: PropTypes.func.isRequired,
  prepareRow: PropTypes.func.isRequired,
  updateDraggableData: PropTypes.func.isRequired,
  withDragAndDrop: PropTypes.bool.isRequired,
  theme: ThemeProps.isRequired,
};

export default connect((state) => ({
  theme: state.theme,
}))(ReactTableBody);
