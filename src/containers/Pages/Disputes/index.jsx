import React from "react";
import { Col, Container, Row } from "reactstrap";
import DisputesCard from "./components/DisputesCard";

const DisputesPage = () => (
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">Disputes</h3>
      </Col>
    </Row>
    <Row>
      <DisputesCard />
    </Row>
  </Container>
);

export default DisputesPage;
