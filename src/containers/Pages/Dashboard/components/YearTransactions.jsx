import React from 'react';
import PropTypes from 'prop-types';
// import { useTranslation } from 'react-i18next';
import Select from 'react-select';
import { Card, CardBody, Col } from 'reactstrap';
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, CartesianAxis,
} from 'recharts';

const data = [
    {
      "name": "Jan",
      "pv": 24000
    },
    {
      "name": "Feb",
      "pv": 13980
    },
    {
      "name": "Mar",
      "pv": 98000
    },
    {
      "name": "Apr",
      "pv": 39080
    },
    {
      "name": "May",
      "pv": 48000
    },
    {
      "name": "June",
      "pv": 38000
    },
    {
      "name": "July",
      "pv": 43000
    },
    {
      "name": "Aug",
      "pv": 78000
    },
    {
      "name": "Sept",
      "pv": 67000
    },
    {
      "name": "Oct",
      "pv": 83000
    },
    {
      "name": "Nov",
      "pv": 73000
    },
    {
      "name": "Dec",
      "pv": 53000
    }
  ];

const SimpleBarChart = ({ dir }) => {
  // const { t } = useTranslation('common');

  const options = [
    { value: '2022', label: '2022' },
    { value: '2021', label: '2021' },
    { value: '2020', label: '2020' }
  ];

  return (
    <Col xs={12} md={9} lg={9} xl={9}>
      <Card>
        <CardBody>
          <div className="card__title__dashboard">
            <h5 className="bold-text amount">Transactions Stats</h5>
            <Select options={options} defaultValue={options[0]} />
          </div>
          <div dir="ltr"> 
            <ResponsiveContainer height={250}>
              <BarChart
                data={data}
                margin={{
                  top: 0, right: 0, left: 0, bottom: 0,
                }}
              >
                <XAxis dataKey="name"  reversed={dir === 'rtl'} />
                <YAxis axisLine={false} tickLine={false} domain={[0, 100000]} tickCount={7} />
                <CartesianAxis  />
                <CartesianGrid vertical={false}  strokeDasharray="5 5" />
                <Tooltip />
                <Legend />
                <Bar dataKey="pv" fill="#F59C65" barSize={10} radius={[10, 10, 10, 10]}/>
              </BarChart>
            </ResponsiveContainer>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

SimpleBarChart.propTypes = {
  dir: PropTypes.string.isRequired,
};

export default SimpleBarChart;
