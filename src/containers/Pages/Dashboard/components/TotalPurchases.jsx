// Import the necessary modules and library.
import React from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import styles from "../Dashboard.module.css";
import { numberFormat } from "../../../../utils/formatCurrency/formatCurrency";

// TotalPurchases Component.

const TotalPurchases = ({ dashboardStats, accountCurrency, themeMode }) => {
  return (
    <>
      <Col xs={12} md={12} lg={3} xl={3}>
        <Card>
          <CardBody>
            <div className="card__title__dashboard">
              <h5 className="bold-text amount">Total Transactions</h5>
            </div>

            <div className="d-flex justify-content-center align-items-center mt-5">
              <div
                className={`${styles.totalPurchases} ${
                  themeMode === "theme-dark" ? styles.dark : ""
                }`}
              >
                <div className={styles.rectangle}></div>
                <div className="d-flex justify-content-center align-items-center mt-3">
                  <div className={styles.totalPurchasesDot}></div>
                  <div className="text-center ">Total Transactions</div>
                </div>
                <h3 className="mt-1 text-center">
                  {numberFormat(dashboardStats?.total_trans, accountCurrency)}
                </h3>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
      <Col xs={12} md={12} lg={3} xl={3}>
        <Card>
          <CardBody>
            <div className="card__title__dashboard">
              <h5 className="bold-text amount">Total Inflow</h5>
            </div>

            <div className="d-flex justify-content-center align-items-center mt-5">
              <div
                className={`${styles.totalInflow} ${
                  themeMode === "theme-dark" ? styles.dark : ""
                }`}
              >
                <div className={styles.rectangle}></div>
                <div className="d-flex justify-content-center align-items-center mt-3">
                  <div className={styles.totalInflowDot}></div>
                  <div className="text-center ">Total Inflow</div>
                </div>
                <h3 className="mt-1 text-center">
                  {numberFormat(dashboardStats?.total_inflow, accountCurrency)}
                </h3>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
      <Col xs={12} md={12} lg={3} xl={3}>
        <Card>
          <CardBody>
            <div className="card__title__dashboard">
              <h5 className="bold-text amount">Total Outflow</h5>
            </div>

            <div className="d-flex justify-content-center align-items-center mt-5">
              <div
                className={`${styles.totalOutflow} ${
                  themeMode === "theme-dark" ? styles.dark : ""
                }`}
              >
                <div className={styles.rectangle}></div>
                <div className="d-flex justify-content-center align-items-center mt-3">
                  <div className={styles.totalOutflowDot}></div>
                  <div className="text-center ">Total Outflow</div>
                </div>
                <h3 className="mt-1 text-center">
                  {" "}
                  {numberFormat(dashboardStats?.total_outflow, accountCurrency)}
                </h3>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
      <Col xs={12} md={12} lg={3} xl={3}>
        <Card>
          <CardBody>
            <div className="card__title__dashboard">
              <h5 className="bold-text amount">Available Balance</h5>
            </div>

            <div className="d-flex justify-content-center align-items-center mt-5">
              <div
                className={`${styles.availableBalance} ${
                  themeMode === "theme-dark" ? styles.dark : ""
                }`}
              >
                <div className={styles.rectangle}></div>
                <div className="d-flex justify-content-center align-items-center mt-3">
                  <div className={styles.dot}></div>
                  <div className="text-center ">Available Balance</div>
                </div>
                <h3 className="mt-1 text-center">
                  {numberFormat(
                    dashboardStats?.balance_details?.balance,
                    accountCurrency
                  )}
                </h3>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
    </>
  );
};

TotalPurchases.propTypes = {
  dashboardStats: PropTypes.object,
  accountCurrency: PropTypes.string,
  themeMode: PropTypes.string,
};

// Export Components
export default TotalPurchases;
