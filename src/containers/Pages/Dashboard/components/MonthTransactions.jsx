import React from 'react';
import PropTypes from 'prop-types';
// import { useTranslation } from 'react-i18next';
import { Card, CardBody, Col } from 'reactstrap';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, CartesianAxis,
} from 'recharts';
import { numberFormat } from '../../../../utils/formatCurrency/formatCurrency';

const SimpleLineChart = ({ dir, dashboardStats, accountCurrency  }) => {
  const {amounts, timestamps} = dashboardStats?.outflow_stats;
  let data = [];
  for (let i = 0; i < amounts.length; i++) data.push({
    timestamp: timestamps[i],
    amounts: amounts[i]
  });

  return (
    <Col xs={12} md={12} lg={6} xl= {6}>
      <Card>
        <CardBody>
          <div className="card__title__dashboard">
            <h5 className="bold-text amount">{numberFormat(dashboardStats?.total_outflow, accountCurrency)}</h5>
            <p className='subhead'>Outgoing Transactions</p>
          </div>
          <div dir="ltr">
            <ResponsiveContainer height={250}>
              <LineChart
                data={data}
                margin={{ 
                  top: 0, right: 15, left: 0, bottom: 0,
                }}
              >
                <XAxis dataKey="timestamp"  domain={['auto', 'auto']} width={10} reversed={dir === 'rtl'} />
                <YAxis axisLine={false} type="number" tickLine={false} domain={[0, 100000]} tickCount={7} />
                <CartesianAxis  />
                <CartesianGrid vertical={false}  strokeDasharray="5 5" />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="amounts" stroke="#000080" activeDot={{ r: 8 }} />
              </LineChart>
            </ResponsiveContainer>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

SimpleLineChart.propTypes = {
  dir: PropTypes.string.isRequired,
  dashboardStats: PropTypes.object,
  accountCurrency: PropTypes.string.isRequired,
};

export default SimpleLineChart;
