import React, { useEffect, useState } from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { getUserDashboardStats } from "../../../utils/apiHandlers/getUserDashboardStats";
import { source } from "../../../utils/apiHandlers/getUserDashboardStats";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Col, Container, Row } from "reactstrap";
import { ThemeProps, RTLProps } from "../../../shared/prop-types/ReducerProps";
import styles from "./Dashboard.module.css";
import WeekTransactions from "./components/WeekTransactions";
import MonthTransactions from "./components/MonthTransactions";
// import YearTransactions from './components/YearTransactions';
import TotalPurchases from "./components/TotalPurchases";

const Dashboard = ({ rtl }) => {
  const { t } = useTranslation("common");

  const [selectedFilterValue, setSelectedFilterValue] = useState("all");
  const [dashboardStats, setDashboardStats] = useState(null);
  const { className } = useSelector((state) => state.theme);
  const { signedInAccountNumber, userAccounts } = useSelector(
    (state) => state.user
  );
  let currency = "";

  userAccounts.forEach((item) => {
    if (item?.account_number === signedInAccountNumber) {
      currency = item?.balance_currency;
      return;
    }
  });

  const handleChange = (e) => {
    setSelectedFilterValue(e.value);
  };

  const getDashboardStatsHandler = async () => {
    try {
      const response = await getUserDashboardStats(
        signedInAccountNumber,
        selectedFilterValue
      ).catch((error) => {
        if (axios.isCancel(error)) {
          console.log("Successfully aborted");
        }
      });
      setDashboardStats(response.data.data);
      console.log(dashboardStats);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(selectedFilterValue);

  useEffect(() => {
    let isMounted = true;
    let abortController = new AbortController();
    if (isMounted) {
      getDashboardStatsHandler();
    }
    return () => {
      // cancel the request before component unmounts
      isMounted = false;
      abortController.abort();
      source.cancel();
    };
  }, [signedInAccountNumber, selectedFilterValue]);

  const options = [
    { value: "all", label: "All time" },
    { value: "today", label: "Today" },
    { value: "yesterday", label: "Yesterday" },
    { value: "3days", label: "3 days" },
    { value: "week", label: "Week" },
    { value: "month", label: "Month" },
    { value: "3months", label: "3 Months" },
    { value: "6months", label: "6 Months" },
    { value: "year", label: "Year" },
    { value: "2years", label: "2 Years" },
    { value: "3years", label: "3 Years" },
  ];

  return (
    <>
      <Container className="dashboard">
        <Row>
          <Col md={12}>
            <h3 className="page-title">{t("Dashboard")}</h3>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <div className={styles.top_board}>
              <div
                className={`${styles.successful} ${
                  className === "theme-dark" ? styles.dark : ""
                }`}
              >
                <p>Successful Transaction</p>
                <div className="d-flex align-items-center ml-3 mt-3">
                  <h3 className="">
                    {dashboardStats?.sucessful_transactions?.value}
                  </h3>
                  <span className="mx-2">
                    {dashboardStats?.sucessful_transactions?.arrow == "up" ? (
                      <svg
                        width="8"
                        height="7"
                        viewBox="0 0 8 7"
                        fill="none"
                        className="mr-1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4.754 0.41277C4.79267 0.450438 4.958 0.592669 5.094 0.725158C5.94933 1.50191 7.34933 3.52821 7.77667 4.58877C7.84533 4.74984 7.99067 5.15705 8 5.37461C8 5.58309 7.952 5.78182 7.85467 5.97146C7.71867 6.20787 7.50467 6.39751 7.252 6.50142C7.07667 6.56831 6.552 6.67223 6.54267 6.67223C5.96867 6.77614 5.036 6.83329 4.00533 6.83329C3.02333 6.83329 2.12867 6.77614 1.546 6.69106C1.53667 6.68132 0.884667 6.57741 0.661333 6.46375C0.253333 6.25528 0 5.84807 0 5.41228V5.37461C0.01 5.0908 0.263333 4.49395 0.272667 4.49395C0.700667 3.49054 2.032 1.511 2.91667 0.715416C2.91667 0.715416 3.144 0.491354 3.286 0.393936C3.49 0.241963 3.74267 0.166626 3.99533 0.166626C4.27733 0.166626 4.54 0.251705 4.754 0.41277"
                          fill="#11B8A4"
                        />
                      </svg>
                    ) : (
                      <svg
                        width="8"
                        height="7"
                        viewBox="0 0 8 7"
                        fill="none"
                        className="mr-1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M3.246 6.58715C3.20733 6.54948 3.042 6.40725 2.906 6.27476C2.05067 5.49801 0.650667 3.47171 0.223333 2.41115C0.154667 2.25008 0.00933333 1.84287 0 1.6253C0 1.41683 0.048 1.2181 0.145333 1.02845C0.281333 0.792052 0.495333 0.602411 0.748 0.498498C0.923333 0.431604 1.448 0.327691 1.45733 0.327691C2.03133 0.223778 2.964 0.166626 3.99467 0.166626C4.97667 0.166626 5.87133 0.223778 6.454 0.308857C6.46333 0.318599 7.11533 0.422512 7.33867 0.536166C7.74667 0.744642 8 1.15185 8 1.58764V1.6253C7.99 1.90912 7.73667 2.50597 7.72733 2.50597C7.29933 3.50938 5.968 5.48892 5.08333 6.2845C5.08333 6.2845 4.856 6.50856 4.714 6.60598C4.51 6.75796 4.25733 6.83329 4.00467 6.83329C3.72267 6.83329 3.46 6.74821 3.246 6.58715"
                          fill="#D00026"
                        />
                      </svg>
                    )}
                    {dashboardStats?.sucessful_transactions?.percentage}
                  </span>
                </div>
              </div>
              <div
                className={`${styles.failed} ${
                  className === "theme-dark" ? styles.dark : ""
                }`}
              >
                <p>Failed Transaction</p>
                <div className="d-flex align-items-center ml-3 mt-3">
                  <h3 className="">
                    {dashboardStats?.failed_transactions?.value}
                  </h3>
                  <span className="ml-2">
                    {dashboardStats?.failed_transactions?.arrow == "up" ? (
                      <svg
                        width="8"
                        height="7"
                        viewBox="0 0 8 7"
                        fill="none"
                        className="mr-1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4.754 0.41277C4.79267 0.450438 4.958 0.592669 5.094 0.725158C5.94933 1.50191 7.34933 3.52821 7.77667 4.58877C7.84533 4.74984 7.99067 5.15705 8 5.37461C8 5.58309 7.952 5.78182 7.85467 5.97146C7.71867 6.20787 7.50467 6.39751 7.252 6.50142C7.07667 6.56831 6.552 6.67223 6.54267 6.67223C5.96867 6.77614 5.036 6.83329 4.00533 6.83329C3.02333 6.83329 2.12867 6.77614 1.546 6.69106C1.53667 6.68132 0.884667 6.57741 0.661333 6.46375C0.253333 6.25528 0 5.84807 0 5.41228V5.37461C0.01 5.0908 0.263333 4.49395 0.272667 4.49395C0.700667 3.49054 2.032 1.511 2.91667 0.715416C2.91667 0.715416 3.144 0.491354 3.286 0.393936C3.49 0.241963 3.74267 0.166626 3.99533 0.166626C4.27733 0.166626 4.54 0.251705 4.754 0.41277"
                          fill="#11B8A4"
                        />
                      </svg>
                    ) : (
                      <svg
                        width="8"
                        height="7"
                        viewBox="0 0 8 7"
                        fill="none"
                        className="mr-1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M3.246 6.58715C3.20733 6.54948 3.042 6.40725 2.906 6.27476C2.05067 5.49801 0.650667 3.47171 0.223333 2.41115C0.154667 2.25008 0.00933333 1.84287 0 1.6253C0 1.41683 0.048 1.2181 0.145333 1.02845C0.281333 0.792052 0.495333 0.602411 0.748 0.498498C0.923333 0.431604 1.448 0.327691 1.45733 0.327691C2.03133 0.223778 2.964 0.166626 3.99467 0.166626C4.97667 0.166626 5.87133 0.223778 6.454 0.308857C6.46333 0.318599 7.11533 0.422512 7.33867 0.536166C7.74667 0.744642 8 1.15185 8 1.58764V1.6253C7.99 1.90912 7.73667 2.50597 7.72733 2.50597C7.29933 3.50938 5.968 5.48892 5.08333 6.2845C5.08333 6.2845 4.856 6.50856 4.714 6.60598C4.51 6.75796 4.25733 6.83329 4.00467 6.83329C3.72267 6.83329 3.46 6.74821 3.246 6.58715"
                          fill="#D00026"
                        />
                      </svg>
                    )}
                    {dashboardStats?.failed_transactions?.percentage}
                  </span>
                </div>
              </div>
              <div
                className={`${styles.customers} ${
                  className === "theme-dark" ? styles.dark : ""
                }`}
              >
                <p>Total Customers</p>
                <div className="d-flex align-items-center ml-3 mt-3">
                  <h3 className="">{dashboardStats?.customers?.value}</h3>
                  <span className="ml-2">
                    {dashboardStats?.customers?.arrow == "up" ? (
                      <svg
                        width="8"
                        height="7"
                        viewBox="0 0 8 7"
                        fill="none"
                        className="mr-1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4.754 0.41277C4.79267 0.450438 4.958 0.592669 5.094 0.725158C5.94933 1.50191 7.34933 3.52821 7.77667 4.58877C7.84533 4.74984 7.99067 5.15705 8 5.37461C8 5.58309 7.952 5.78182 7.85467 5.97146C7.71867 6.20787 7.50467 6.39751 7.252 6.50142C7.07667 6.56831 6.552 6.67223 6.54267 6.67223C5.96867 6.77614 5.036 6.83329 4.00533 6.83329C3.02333 6.83329 2.12867 6.77614 1.546 6.69106C1.53667 6.68132 0.884667 6.57741 0.661333 6.46375C0.253333 6.25528 0 5.84807 0 5.41228V5.37461C0.01 5.0908 0.263333 4.49395 0.272667 4.49395C0.700667 3.49054 2.032 1.511 2.91667 0.715416C2.91667 0.715416 3.144 0.491354 3.286 0.393936C3.49 0.241963 3.74267 0.166626 3.99533 0.166626C4.27733 0.166626 4.54 0.251705 4.754 0.41277"
                          fill="#11B8A4"
                        />
                      </svg>
                    ) : (
                      <svg
                        width="8"
                        height="7"
                        viewBox="0 0 8 7"
                        fill="none"
                        className="mr-1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M3.246 6.58715C3.20733 6.54948 3.042 6.40725 2.906 6.27476C2.05067 5.49801 0.650667 3.47171 0.223333 2.41115C0.154667 2.25008 0.00933333 1.84287 0 1.6253C0 1.41683 0.048 1.2181 0.145333 1.02845C0.281333 0.792052 0.495333 0.602411 0.748 0.498498C0.923333 0.431604 1.448 0.327691 1.45733 0.327691C2.03133 0.223778 2.964 0.166626 3.99467 0.166626C4.97667 0.166626 5.87133 0.223778 6.454 0.308857C6.46333 0.318599 7.11533 0.422512 7.33867 0.536166C7.74667 0.744642 8 1.15185 8 1.58764V1.6253C7.99 1.90912 7.73667 2.50597 7.72733 2.50597C7.29933 3.50938 5.968 5.48892 5.08333 6.2845C5.08333 6.2845 4.856 6.50856 4.714 6.60598C4.51 6.75796 4.25733 6.83329 4.00467 6.83329C3.72267 6.83329 3.46 6.74821 3.246 6.58715"
                          fill="#D00026"
                        />
                      </svg>
                    )}
                    {dashboardStats?.customers?.percentage}
                  </span>
                </div>
              </div>
              <div className={styles.filter}>
                <div className="input-group">
                  <span className="input-group-text">Filter By:</span>
                  <Select
                    options={options}
                    // defaultValue={options[0]}
                    value={options.find(
                      (option) => option.value === selectedFilterValue
                    )}
                    onChange={handleChange}
                  />
                </div>
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          {dashboardStats !== null ? (
            <WeekTransactions
              dir={rtl.direction}
              dashboardStats={dashboardStats}
              accountCurrency={currency}
            />
          ) : null}
          {dashboardStats !== null ? (
            <MonthTransactions
              dir={rtl.direction}
              dashboardStats={dashboardStats}
              accountCurrency={currency}
            />
          ) : null}
        </Row>
        <Row>
          {/* <YearTransactions dir={rtl.direction} /> */}
          <TotalPurchases
            dashboardStats={dashboardStats}
            accountCurrency={currency}
            themeMode={className}
          />
        </Row>
      </Container>
    </>
  );
};

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
  rtl: RTLProps.isRequired,
  theme: ThemeProps.isRequired,
};

export default connect((state) => ({
  cryptoTable: state?.cryptoTable?.items,
  rtl: state.rtl,
  theme: state.theme,
}))(Dashboard);
