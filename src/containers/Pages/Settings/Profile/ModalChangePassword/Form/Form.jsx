import React from "react";
import PropTypes from "prop-types";
import UpdatePasswordForm from "./components/UpdatePassword";

const FormWrapper = ({ setModalOpen }) => (
  <div
    className="account"
    style={{
      minHeight: "0",
      height: "100%",
    }}
  >
    <div className="account__card">
      <div className="account__head">
        <h4 className="account__subhead subhead">Update Password</h4>
        <p>Please input your preferred password</p>
      </div>
      <UpdatePasswordForm setModal={setModalOpen} />
    </div>
  </div>
);

FormWrapper.propTypes = {
  setModalOpen: PropTypes.func
};

export default FormWrapper;

// if you want to add select, date-picker and time-picker in your app you need to uncomment the first
// four lines in /scss/components/form.scss to add styles
