import React, { useEffect, useState } from "react";
import { removeEmpty } from "../../../../utils/removeEmptyObjectKeys/removeEmptyObjectKeys";
import { Formik } from "formik";
import AccountOutlineIcon from "mdi-react/AccountOutlineIcon";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import PlanetIcon from "mdi-react/PlanetIcon";
import PhoneIcon from "mdi-react/PhoneClassicIcon";
import styles from "./General.module.css";
import * as Yup from "yup";
import { useSelector } from "react-redux";
import Spinner from "react-spinner-material";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import PassWordModal from "./ModalChangePassword/Modal";
import { updateUserProfile } from "../../../../utils/apiHandlers/updateUserProfile";
import { getUserProfileDetail } from "../../../../utils/apiHandlers/getUserProfileDetails";
import Swal from "sweetalert2";

const GeneralForm = () => {
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const { className } = useSelector((state) => state.theme);
  const [profileUpdated, setProfileUpdated] = useState(true);
  const [profileDetails, setProfileDetails] = useState({});

  useEffect(() => {
    getUserProfileDetailHandler();
  }, [profileUpdated]);

  // eslint-disable-next-line no-unused-vars
  const showPassword = () => {
    setIsPasswordShown(!isPasswordShown);
  };

  async function getUserProfileDetailHandler() {
    try {
      const response = await getUserProfileDetail().catch((err) => {
        console.log(err);
      });
      if (response.data.status) {
        setProfileDetails(response.data.data);
      }
    } catch (error) {
      console.log(error);
    }
  }
  async function updateUserProfileHandler(data, setSubmitting) {
    try {
      const response = await updateUserProfile(JSON.stringify(data)).catch(
        (err) => {
          console.log(err);
        }
      );
      if (response.data.status) {
        Swal.fire({
          title: "",
          text: response.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        setSubmitting(false);
        setProfileUpdated((prev) => !prev);
      }
      setSubmitting(false);
    } catch (error) {
      setSubmitting(false);
    }
  }

  //check if there is data and render component only when there is data
  function isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  return (
    !isEmpty(profileDetails) && (
      <div className={`${styles.form_container} mt-4`}>
        <div
          style={{
            width: "40rem",
          }}
        >
          <Formik
            initialValues={{
              first_name: "",
              last_name: "",
              phone: "+234",
              country: "",
            }}
            onSubmit={(values, { setSubmitting }) => {
              setSubmitting(true);
              const payload = removeEmpty(values);
              updateUserProfileHandler(payload, setSubmitting);
            }}
            validationSchema={Yup.object().shape({
              first_name: Yup.string()
                .matches(
                  /^[aA-zZ\s]+$/,
                  "Only alphabets are allowed as account name "
                )
                .notRequired()
                .min(4, "First name should be a minimum of 3 characters."),
              last_name: Yup.string()
                .matches(
                  /^[aA-zZ\s]+$/,
                  "Only alphabets are allowed as account name "
                )
                .notRequired()
                .min(4, "Last name should be a minimum of 3 characters."),
              phone: Yup.string()
                .required("Please provide a phone number.")
                .notRequired(),
              country: Yup.string()
                .required("Please select a country")
                .notRequired(),
            })}
          >
            {({
              values,
              touched,
              errors,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
            }) => (
              <form className="form" onSubmit={handleSubmit}>
                <div className={styles.name_container}>
                  <div className={`form__form-group`}>
                    <span className="form__form-group-label">First Name</span>
                    <div
                      className={`form__form-group-field ${
                        errors.first_name &&
                        touched.first_name &&
                        "form__form-validation"
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <AccountOutlineIcon />
                      </div>
                      <input
                        name="first_name"
                        data-testid="first_name"
                        component="input"
                        type="text"
                        placeholder="First Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.first_name || profileDetails.first_name}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.first_name &&
                        touched.first_name &&
                        errors.first_name}
                    </p>
                  </div>
                  <div className="form__form-group">
                    <span className="form__form-group-label">Last Name</span>
                    <div
                      className={`form__form-group-field ${
                        errors.last_name &&
                        touched.last_name &&
                        "form__form-validation"
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <AccountOutlineIcon />
                      </div>
                      <input
                        name="last_name"
                        data-testid="last_name"
                        component="input"
                        type="text"
                        placeholder="Last Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.last_name || profileDetails.last_name}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.last_name &&
                        touched.last_name &&
                        errors.last_name}
                    </p>
                  </div>
                </div>
                <div className="form__form-group">
                  <span className="form__form-group-label">Email</span>
                  <div
                    className={`form__form-group-field ${
                      errors.email && "form__form-validation"
                    }`}
                  >
                    <div className="form__form-group-icon">
                      <AlternateEmailIcon />
                    </div>
                    <input
                      disabled
                      name="email"
                      data-testid="email"
                      component="input"
                      type="email"
                      placeholder="Email"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={profileDetails.email}
                    />
                  </div>
                  <p className="error_validation_text">
                    {errors.email && touched.email && errors.email}
                  </p>
                </div>
                <div>
                  <span className="form__form-group-label">Phone Number</span>
                  <div
                    className={`form__form-group-field ${
                      errors.phone && "form__form-validation"
                    }`}
                  >
                    <div
                      data-testid="phone_number"
                      className="form__form-group-icon"
                    >
                      <PhoneIcon />
                    </div>
                    <PhoneInput
                      inputProps={{ name: "phone" }}
                      onChange={(phone, country, e) => {
                        handleChange(e);
                      }}
                      placeholder="Enter phone number"
                      name="phone_number"
                      value={
                        profileDetails.phone
                          ? profileDetails.phone
                          : values.phone
                      }
                    />
                  </div>
                  <p className="error_validation_text">
                    {errors.phone && touched.phone && errors.phone}
                  </p>
                </div>

                <div className="form__form-group">
                  <span className="form__form-group-label">Country</span>
                  <div
                    className={`${styles.select_form_custom_container} ${
                      errors.country && "form__form-validation"
                    }`}
                  >
                    <div
                      className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                    >
                      <PlanetIcon />
                    </div>
                    <select
                      className={
                        className == "theme-light"
                          ? styles.select_form_custom_light
                          : styles.select_form_custom_dark
                      }
                      name="country"
                      data-testid="country"
                      component="select"
                      type="select"
                      placeholder="Country"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.country || profileDetails.country}
                    >
                      <option label={"Country"} />
                      <option value={"Nigeria"} label={"Nigeria"} />
                      <option value={"South Africa"} label={"South Africa"} />
                      <option value={"Kenya"} label={"Kenya"} />
                      <option value={"Ghana"} />
                    </select>
                  </div>
                  <p className="error_validation_text">
                    {errors.country && touched.country && errors.country}
                  </p>
                </div>
                <div className={`form__form-group`}>
                  <span className="form__form-group-label">Password</span>
                  <div
                    className={`form__form-group-field ${
                      errors.password && "form__form-validation"
                    }`}
                  >
                    <div className="form__form-group-icon">
                      <KeyVariantIcon />
                    </div>
                    <input
                      disabled
                      name="password"
                      data-testid="password"
                      component="input"
                      type={isPasswordShown ? "text" : "password"}
                      placeholder="*********"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.password}
                    />
                  </div>
                  <p className="error_validation_text">
                    {errors.password && touched.password && errors.password}
                  </p>
                </div>
                <div className={styles.btnContainer}>
                  <PassWordModal />
                  <button
                    placeholder="save"
                    className={`${styles.saveBtn} btn btn-primary account__btn account__btn--small`}
                  >
                    {isSubmitting ? (
                      <div className={styles.button_elem}>
                        <Spinner
                          radius={20}
                          color={"#333"}
                          stroke={2}
                          visible={true}
                        />
                      </div>
                    ) : (
                      "Save"
                    )}
                  </button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    )
  );
};

export default GeneralForm;
