import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import GeneralForm from "../General";
import { data } from "../../../../../fixures/store";
import server from "../../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <GeneralForm/> renders", () => {
  const store = mockStore(data);
  it("check if <GeneralForm /> contains, inputs, buttons and modal", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <GeneralForm />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      const firstName = screen.getByTestId("first_name");
      const lastName = screen.getByTestId("last_name");
      const email = screen.getByTestId("email");
      const phoneNumber = screen.getByTestId("phone_number");
      const country = screen.getByTestId("country");
      const password = screen.getByTestId("password");
      const saveButton = screen.getByPlaceholderText("save");
      const changePasswordButton = screen.getByTestId("change-password-btn");

      expect(firstName).toBeInTheDocument();
      expect(lastName).toBeInTheDocument();
      expect(email).toBeInTheDocument();
      expect(phoneNumber).toBeInTheDocument();
      expect(country).toBeInTheDocument();
      expect(password).toBeInTheDocument();
      expect(saveButton).toBeInTheDocument();
      expect(screen.queryByTestId("new_password")).toBe(null);

      //opens change password modal
      fireEvent.click(changePasswordButton);

      const newPassword = screen.getByTestId("new_password");
      const confirmNewPassword = screen.getByTestId("confirm_new_password");
      const submitPasswordButton = screen.getByTestId("submit_password");
      //assert modal is showing
      expect(newPassword).toBeVisible();
      expect(confirmNewPassword).toBeVisible();
      expect(submitPasswordButton).toBeVisible();

      //password modal inputs and buttons
    });
  });
});
