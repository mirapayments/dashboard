import React from "react";
import PropTypes from 'prop-types';
import { Col, Container, Row } from "reactstrap";
import TabContainer from "./components/Container";

const SettingsPage = ({ rolepermissions }) => {
  console.log(rolepermissions);
  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          {/* <h3 className="page-title">Settings Page</h3> */}
        </Col>
      </Row>
      <Row>
        <TabContainer />
      </Row>
    </Container>
  );
};

SettingsPage.propTypes = {
  rolepermissions: PropTypes.isRequired
};

export default SettingsPage;
