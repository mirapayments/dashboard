import ModalComponent from "../Modal";

import { fireEvent, render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import { data } from "../../../../../../fixures/store";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe("check if <SignIn/> renders", () => {
  const store = mockStore(data);
  it("check if <ModalComponent/> contains button and inputs", () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <ModalComponent color="red" colored={false} header={true} btn="red" />
        </BrowserRouter>
      </Provider>
    );

    const openModalButton = screen.getByTestId("open-modal-btn");
    fireEvent.click(openModalButton);

    //get inputs
    const emailInput = screen.getByTestId("email");
    const roleInput = screen.getByTestId("role");

    const inviteButton = screen.getByTestId("invite-btn");
    expect(emailInput).toBeInTheDocument();
    expect(roleInput).toBeInTheDocument();
    expect(inviteButton).toBeInTheDocument();
  });
});
