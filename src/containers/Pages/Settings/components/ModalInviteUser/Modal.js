import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "reactstrap";
import classNames from "classnames";
import styles from "../ListAccount.module.css";
import ButtonIcon from "../../../../../shared/img/Vector.svg";
import Form from "../ModalInviteUser/Form/Form";
import { useSelector } from "react-redux";

const ModalComponent = ({ color, colored, header }) => {
  const [modal, setModal] = useState(false);
  const { className } = useSelector((state) => state.theme);

  const toggle = () => {
    setModal((prevState) => !prevState);
  };

  // let Icon = <span className="lnr lnr-pushpin modal__title-icon" />;

  const modalClass = classNames({
    "modal-dialog--colored": colored,
    "modal-dialog--header": header,
  });

  return (
    <div>
      <div
        styles={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <button
          data-testid="open-modal-btn"
          color={color}
          onClick={toggle}
          type="submit"
          className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
        >
          <img src={ButtonIcon} />
          Invite user
        </button>
      </div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        // modalClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <div className="modal__body">
          <Form setModalOpen={setModal} />
        </div>
      </Modal>
    </div>
  );
};

ModalComponent.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  color: PropTypes.string.isRequired,
  colored: PropTypes.bool,
  header: PropTypes.bool,
  btn: PropTypes.string.isRequired,
};

ModalComponent.defaultProps = {
  title: "",
  message: "",
  colored: false,
  header: false,
};

export default ModalComponent;
