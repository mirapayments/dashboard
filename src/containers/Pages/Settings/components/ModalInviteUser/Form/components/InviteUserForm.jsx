import React from "react";
import { Formik } from "formik";
import PropTypes from "prop-types";
import * as Yup from "yup";
import styles from "./InviteUserForm.module.css";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "react-spinner-material";
import Swal from "sweetalert2";
import useOnlineStatus from "../../../../../../../utils/hooks/useOnlineStatus";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import AccountCogIcon from "mdi-react/AccountCogIcon";
import { setUserInvited } from "../../../../../../../redux/features/user/uiSlice";
import { inviteUser } from "../../../../../../../utils/apiHandlers/inviteUser";
import { Link } from "react-router-dom";

const InviteUserForm = ({ setModal }) => {
  const { className } = useSelector((state) => state.theme);
  const online = useOnlineStatus();
  const { signedInAccountNumber, role } = useSelector((state) => state.user);
  const { userInvited } = useSelector((state) => state.ui);
  const dispatch = useDispatch();

  // pass role permissions page visibility as a prop
  const location = {
    pathname: "/pages/settings/user/rolepermissions",
    state: { rolepermissions: true },
  };

  async function inviteUserHandler(data, setSubmitting) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      setSubmitting(false);
      return;
    }
    try {
      const res = await inviteUser(
        signedInAccountNumber,
        JSON.stringify(data)
      ).catch((err) => {
        if (err.status === 403) {
          Swal.fire({
            title: "",
            text: err.data.detail,
            icon: "error",
            showConfirmButton: false,
            timer: 3000,
          });
          setSubmitting(false);
          return;
        }
        if (err.status === 400 && err.data.detail) {
          Swal.fire({
            title: "",
            text: err.data.detail,
            icon: "error",
            showConfirmButton: false,
            timer: 3000,
          });
          setSubmitting(false);
          return;
        }
      });
      if (res.data.status) {
        Swal.fire({
          title: "",
          text: res.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        //refresh user List
        dispatch(setUserInvited(!userInvited));
        setSubmitting(false);
        setModal(false);
      }
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <Formik
      initialValues={{ email: "", role: "" }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true);
        inviteUserHandler(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .required("Please provide the user's email.")
          .email(),
        role: Yup.string().required(
          "Please select a role for the invited user."
        ),
      })}
    >
      {({
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => (
        <form
          style={{
            height: "10%",
          }}
          className="form"
          onSubmit={handleSubmit}
        >
          <div className={`form__form-group`}>
            <span className="form__form-group-label">Email</span>
            <div
              className={`form__form-group-field ${
                errors.email && "form__form-validation"
              }`}
            >
              <div className="form__form-group-icon">
                <AlternateEmailIcon />
              </div>
              <input
                data-testid="email"
                name="email"
                component="input"
                type="email"
                placeholder="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
            </div>
            <p className="error_validation_text">
              {errors.email && touched.email && errors.email}
            </p>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Role</span>
            <div
              className={`${styles.select_form_custom_container} ${
                errors.balance_currency && "form__form-validation"
              }`}
            >
              <div
                className={`form__form-group-icon ${styles.select_form_custom_icon}`}
              >
                <AccountCogIcon />
              </div>
              <select
                className={
                  className == "theme-light"
                    ? styles.select_form_custom_light
                    : styles.select_form_custom_dark
                }
                name="role"
                data-testid="role"
                component="select"
                type="select"
                placeholder="role"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.role}
              >
                <option label={"Please select a role"} />
                {/* Only owner can invite another owner */}
                {role === "Owner" && <option value={"Owner"} label={"Owner"} />}
                {/* Only Owner or Admin can invite an Admin */}
                {(role === "Owner" || role === "Admin") && (
                  <option value={"Admin"} label={"Admin"} />
                )}

                <option value={"Operations"} label={"Operations"} />
                <option value={"Support"} label={"Support"} />
                <option value={"Developer"} label={"Developer"} />
              </select>
            </div>
            <p className="error_validation_text">
              {errors.role && touched.role && errors.role}
            </p>
          </div>

          <div className="mb-4">
            <p>
              To learn more about roles and permissions click{" "}
              <Link to={location} style={{ color: "blue" }}>
                here.
              </Link>
            </p>
          </div>

          <button
            data-testid="invite-btn"
            type="submit"
            disabled={isSubmitting}
            className="btn btn-primary account__btn account__btn--small"
          >
            {isSubmitting ? (
              <div className={styles.button_elem}>
                <Spinner radius={20} color={"#333"} stroke={2} visible={true} />
              </div>
            ) : (
              "Invite"
            )}
          </button>
        </form>
      )}
    </Formik>
  );
};

InviteUserForm.propTypes = {
  setModal: PropTypes.func,
};

export default InviteUserForm;
