import React from "react";
import PropTypes from "prop-types";
import InviteUserForm from "./components/InviteUserForm";

const FormWrapper = ({ setModalOpen }) => (
  <div
    className="account"
    style={{
      minHeight: "0",
      height: "100%",
    }}
  >
    <div className="account__card">
      <div className="account__head">
        <h4 className="account__subhead subhead">Invite new user</h4>
        <p>Fill in the user&apos s email below to invite the user</p>
      </div>
      <InviteUserForm setModal={setModalOpen} />
    </div>
  </div>
);
FormWrapper.propTypes = {
  setModalOpen: PropTypes.func
};


export default FormWrapper;
