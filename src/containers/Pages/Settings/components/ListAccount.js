import React, { useEffect, useState } from "react";
import { Table } from "reactstrap";
import Spinner from "react-spinner-material";
import styles from "./ListAccount.module.css";
import { setSignedInAccountNumber, setLiveDashBoardMode } from "../../../../redux/features/user/userSlice";
import { setUserAccounts } from "../../../../redux/features/user/userSlice";
import { useDispatch, useSelector } from "react-redux";
import { listUserAccounts } from "../../../../utils/apiHandlers/listUserAccounts";
import { getUserAccountDetails } from "../../../../utils/apiHandlers/getUserAccountDetails";
import { setUserRole } from "../../../../redux/features/user/userSlice";
const header = [
  { id: 0, title: "Name" },
  { id: 1, title: "Account ID" },
  { id: 2, title: "Account Type" },
  { id: 3, title: "Currency" },
];

const ListAccounts = () => {
  const dispatch = useDispatch();
  const { userAccounts, signedInAccountNumber } = useSelector(
    (state) => state.user
  );
  const [isSelectedAccount, setIsSelectedAccount] = useState(false);

  useEffect(() => {
    async function listUserAccountHandler() {
      try {
        const response = await listUserAccounts().catch((err) => err);
        dispatch(setUserAccounts(response.data.data));
      } catch (error) {
        console.log(error);      
      }
    }
    listUserAccountHandler();
  }, []);

  async function switchAccountsHandler(account_number) {
    setIsSelectedAccount(true);
    dispatch(setSignedInAccountNumber(account_number));
    getUserRole(account_number);
    //dispatch action to update
    let accountMode = await localStorage.getItem("mode");
    let mode = JSON.parse(accountMode)[account_number];
    await localStorage.setItem("activeMode", mode);
    dispatch(setLiveDashBoardMode(mode === "test"? false : true ));

    setTimeout(() => setIsSelectedAccount(false), 500);
  }

  async function getUserRole(account_number) {
    try {
      const response = await getUserAccountDetails(account_number).catch(
        (err) => {
          console.log(err);
        }
      );
      const role = response?.data?.data?.signed_in_user_role;
      await dispatch(setUserRole(role));
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Table responsive className="table--bordered dashboard__audience-table" id="assertion-table">
      <thead>
        <tr>
          {header.map((item) => (
            <th className={styles.thead_overide} key={item.id}>
              <p>{item.title}</p>
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {userAccounts?.map((item) => (
          <tr key={item.account_number}>
            <td className={styles.tdstyles}>
              <p>{item.name}</p>
            </td>
            <td className={styles.tdstyles}>
              <p>{item.account_number}</p>
            </td>
            <td className={styles.tdstyles}>
              <p>{item.account_type}</p>
            </td>
            <td className={styles.tdstyles}>
              <p>{item.balance_currency}</p>
            </td>
            <td>
              <button
                type="submit"
                disabled={item.account_number === signedInAccountNumber}
                onClick={() => switchAccountsHandler(item.account_number)}
                className={`${styles.button} btn btn-primary account__btn account__btn--small`}
              >
                {item.account_number === signedInAccountNumber ? (
                  isSelectedAccount ? (
                    <div
                      style={{
                        display: "grid",
                        placeItems: "center",
                      }}
                    >
                      <Spinner
                        radius={20}
                        color={"#333"}
                        stroke={2}
                        visible={true}
                      />
                    </div>
                  ) : (
                    "Switch to"
                  )
                ) : (
                  "Switch to"
                )}
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default ListAccounts;
