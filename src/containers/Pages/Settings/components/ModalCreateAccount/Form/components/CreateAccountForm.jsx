import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import * as Yup from "yup";
import styles from "./CreateAccountForm.module.css";
import AccountBalanceWallet from "mdi-react/AccountBalanceWalletIcon";
import Public from "mdi-react/PublicIcon";
import AttachMoney from "mdi-react/AttachMoneyIcon";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "react-spinner-material";
import Swal from "sweetalert2";
import { createNewBankAccount } from "../../../../../../../utils/apiHandlers/createNewBankAccount";
import { listUserAccounts } from "../../../../../../../utils/apiHandlers/listUserAccounts";
import { setUserAccounts } from "../../../../../../../redux/features/user/userSlice";
import useOnlineStatus from "../../../../../../../utils/hooks/useOnlineStatus";

const CreateAccountForm = ({ setModal }) => {
  //theme hack for custom select styles
  const { className } = useSelector((state) => state.theme);
  const online = useOnlineStatus();
  const dispatch = useDispatch();
  async function createUserBankAccount(data, setSubmitting) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    try {
      const res = await createNewBankAccount(JSON.stringify(data));
      if (res.data.status) {
        Swal.fire({
          title: "",
          text: res.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        const accountList = await listUserAccounts();
        const newUserAccounts = accountList.data.data;

        dispatch(setUserAccounts(newUserAccounts));
        setSubmitting(false);
        setModal(false);
      }
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <Formik
      initialValues={{ name: "", balance_currency: "", account_type: "" }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true);
        createUserBankAccount(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        balance_currency: Yup.string().required("Please select a currency"),
        name: Yup.string()
          .required("Please provide an account name.")
          .min(4, "Account name is too short."),
        account_type: Yup.string().required("Please select an account type"),
      })}
    >
      {({
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => (
        <form
          style={{
            height: "10%",
          }}
          className="form"
          onSubmit={handleSubmit}
        >
          <div className={`form__form-group`}>
            <span className="form__form-group-label">Name</span>
            <div
              className={`form__form-group-field ${
                errors.name && "form__form-validation"
              }`}
            >
              <div className="form__form-group-icon">
                <AccountBalanceWallet />
              </div>
              <input
                name="name"
                component="input"
                placeholder="Trading name or organisation name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
              />
            </div>
            <p className="error_validation_text">
              {errors.name && touched.name && errors.name}
            </p>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Currency</span>
            <div
              className={`${styles.select_form_custom_container} ${
                errors.balance_currency && "form__form-validation"
              }`}
            >
              <div
                className={`form__form-group-icon ${styles.select_form_custom_icon}`}
              >
                <AttachMoney />
              </div>
              <select
                className={
                  className == "theme-light"
                    ? styles.select_form_custom_light
                    : styles.select_form_custom_dark
                }
                name="balance_currency"
                component="select"
                type="select"
                placeholder="Currency"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.balance_currency}
              >
                <option label={"Select a currency"} />
                <option value={"NGN"} label={"NGN"} />
                <option value={"USD"} label={"USD"} />
                <option value={"EUR"} label={"EUR"} />
              </select>
            </div>
            <p className="error_validation_text">
              {errors.balance_currency &&
                touched.balance_currency &&
                errors.balance_currency}
            </p>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Account Type</span>
            <div
              className={`${styles.select_form_custom_container} ${
                errors.account_type && "form__form-validation"
              }`}
            >
              <div
                className={`form__form-group-icon ${styles.select_form_custom_icon}`}
              >
                <Public />
              </div>
              <select
                className={
                  className == "theme-light"
                    ? styles.select_form_custom_light
                    : styles.select_form_custom_dark
                }
                name="account_type"
                component="select"
                type="select"
                placeholder="Account Type"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.account_type}
              >
                <option label={"Select an account type"} />
                <option value={"Individual"} label={"Individual"} />
                <option value={"Company"} label={"Company"} />
                <option value={"Religious"} label={"Religious"} />
                <option value={"Government"} label={"Government"} />
                <option value={"NGO"} label={"NGO"} />
              </select>
            </div>
            <p className="error_validation_text">
              {errors.account_type &&
                touched.account_type &&
                errors.account_type}
            </p>
          </div>
          <button
            type="submit"
            disabled={isSubmitting}
            className="btn btn-primary account__btn account__btn--small"
          >
            {isSubmitting ? (
              <div className={styles.button_elem}>
                <Spinner radius={20} color={"#333"} stroke={2} visible={true} />
              </div>
            ) : (
              "Create Account"
            )}
          </button>
        </form>
      )}
    </Formik>
  );
};

CreateAccountForm.propTypes = {
  setModal: PropTypes.func,
};

export default CreateAccountForm;
