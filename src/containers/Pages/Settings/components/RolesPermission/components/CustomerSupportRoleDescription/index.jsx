// Import the necessary libraries
import React from "react";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import List from "reactstrap/es/List";

// Import the css module file.
import styles from "./CustomerSupportDescription.module.css";

// CustomerSupportRoleDescription Components.

const CustomerSupportRoleDescription = () => {
  return (
    <Col>
      <h3>Customer Support Role</h3>
      <p>
        This role has limited access to the permissions to manage everything on
        the dashboard
      </p>
      <hr />

      <Row className={"g-5"}>
        <Col md={6}>
          <Card className={"border"}>
            <CardHeader className={styles.card_header}>
              What this role can access
            </CardHeader>

            <CardBody>
              <List type="unstyled">
                <p className={styles.list}>
                  Can View Business Performance Metrics
                </p>
                <hr />
                <p className={styles.list}>Can View Customer Insights</p>
                <hr />
                <p className={styles.list}>
                  Can View Business Performance Metrics
                </p>
                <hr />
                <p className={styles.list}>Can View Products</p>
                <hr />
                <p className={styles.list}>Can View Subaccounts & Splits</p>
                <hr />
                <p className={styles.list}>
                  Can View Business Settings & Preference
                </p>
              </List>
            </CardBody>
          </Card>
        </Col>

        <Col md={6}>
          {/* eslint-disable-next-line react/jsx-no-duplicate-props */}
          <Card className={"border"} style={{ maxHeight: "auto" }}>
            <CardHeader style={{ backgroundColor: " #FFEBD6" }}>
              What this role can`&apos;`t access
            </CardHeader>

            <CardBody>
              <List type="unstyled">
                <li className={styles.list}>Can`&apos;`t Manage Refunds & Disputes</li>
                <hr />
                <li className={styles.list}>Can`&apos;`t Export Balance History</li>
                <hr />
                <li className={styles.list}>
                  Can`&apos;`t Create and Manage Payment Pages
                </li>
              </List>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Col>
  );
};

// Export CustomerSupportRoleDescription

export default CustomerSupportRoleDescription;
