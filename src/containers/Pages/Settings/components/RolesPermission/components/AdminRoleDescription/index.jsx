// Import the necessary libraries
import React from "react";

// Import the css module file.
import styles from "./AdminRoleDescription.module.css";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import List from "reactstrap/es/List";

// AdminRoleDescription Components.

const AdminRoleDescription = () => {
  return (
    <Col>
      <h3>Admin Role</h3>
      <p>
        This role grants users the permissions to manage everything on the
        dashboard
      </p>
      <hr />

      <Row className={"g-5"}>
        <Col md={7}>
          <Card className={"border"}>
            <CardHeader className={styles.card_header}>
              What this role can access
            </CardHeader>

            <CardBody>
              <List type="unstyled">
                <p className={styles.list}>
                  Can View Business Performance Metrics
                </p>
                <hr />
                <p className={styles.list}>Can View Customer Insights</p>
                <hr />
                <p className={styles.list}>Can Manage Refunds & Disputes</p>
                <hr />
                <p className={styles.list}>Can Export Balance History</p>
                <hr />
                <p className={styles.list}>
                  Can View Business Performance Metrics
                </p>
                <hr />
                <p className={styles.list}>
                  Can Create and Manage Payment Pages
                </p>
                <hr />
                <p className={styles.list}>Can View Products</p>
                <hr />
                <p className={styles.list}>Can View Subaccounts & Splits</p>
                <hr />
                <p className={styles.list}>
                  Can View Business Settings & Preference
                </p>
              </List>
            </CardBody>
          </Card>
        </Col>

        <Col md={5}>
          {/* eslint-disable-next-line react/jsx-no-duplicate-props */}
          <Card className={"border"} style={{ maxHeight: "286px" }}>
            <CardHeader style={{ backgroundColor: " #FFEBD6" }}>
              What this role can`&apos;`t access
            </CardHeader>

            <CardBody className={"text-center d-flex align-items-center"}>
              <div>
                <h6> This role has full access!</h6>
                <p>
                  Any team member with this role can access all the sections of
                  the dashboard.
                </p>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Col>
  );
};

// Export AdminRoleDescription

export default AdminRoleDescription;
