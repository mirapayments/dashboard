// Import the necessary libraries and methods
import React from "react";
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Col,
  Container,
  Row,
} from "reactstrap";
import classnames from "classnames";
import { setRoleTabPosition } from "../../../../../redux/features/user/uiSlice";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import ArrowBackIcon from "mdi-react/ArrowBackIcon";

// Import Css Module.
import styles from "./RolesPermission.module.css";

//Import Components
import OwnerRoleDescription from "./components/OwnerRoleDescription";
import AdminRoleDescription from "./components/AdminRoleDescription";
import DeveloperRoleDescription from "./components/DeveloperRoleDescription";
import OperationRoleDescription from "./components/OperationRoleDescription";
import CustomerSupportRoleDescription from "./components/CustomerSupportRoleDescription";

// PermissionPage Component
const PermissionPage = () => {
  const dispatch = useDispatch();
  const { roleTabPosition } = useSelector((state) => state.ui);
  const toggle = (tab) => {
    if (roleTabPosition !== tab) dispatch(setRoleTabPosition(tab));
  };

  return (
    <Container>
      <div className={styles.navigate}>
        <Link to={"/pages/settings"}>
          <ArrowBackIcon fontSize="small" className={styles.backicon} />
          Back
        </Link>
      </div>
      <hr />
      <Row>
        <Col md={3}>
          <h4 className={"text-center"}>Default Roles</h4>
          <br />
          <div className="tabs__wrap">
            <Nav vertical>
              <NavItem className={styles.tab}>
                <NavLink
                  className={classnames({ active: roleTabPosition === "1" })}
                  onClick={() => toggle("1")}
                  style={{ cursor: "pointer" }}
                >
                  Owner
                </NavLink>
                <hr />
              </NavItem>

              <NavItem className={styles.tab}>
                <NavLink
                  className={classnames({ active: roleTabPosition === "1" })}
                  onClick={() => toggle("2")}
                  style={{ cursor: "pointer" }}
                >
                  Admin
                </NavLink>
                <hr />
              </NavItem>
              <NavItem className={styles.tab}>
                <NavLink
                  className={classnames({ active: roleTabPosition === "2" })}
                  onClick={() => toggle("3")}
                  style={{ cursor: "pointer" }}
                >
                  Operations
                </NavLink>
                <hr />
              </NavItem>
              <NavItem className={styles.tab}>
                <NavLink
                  className={classnames({ active: roleTabPosition === "3" })}
                  onClick={() => toggle("4")}
                  style={{ cursor: "pointer" }}
                >
                  Customer Support
                </NavLink>
                <hr />
              </NavItem>
              <NavItem className={styles.tab}>
                <NavLink
                  className={classnames({ active: roleTabPosition === "4" })}
                  onClick={() => toggle("5")}
                  style={{ cursor: "pointer" }}
                >
                  Developer
                </NavLink>
                <hr />
              </NavItem>
            </Nav>
          </div>
        </Col>

        <Col md={9}>
          <TabContent activeTab={roleTabPosition}>
            <TabPane tabId="1">
              <OwnerRoleDescription />
            </TabPane>
            <TabPane tabId="2">
              <AdminRoleDescription />
            </TabPane>
            <TabPane tabId="3">
              <OperationRoleDescription />
            </TabPane>
            <TabPane tabId="4">
              <CustomerSupportRoleDescription />
            </TabPane>
            <TabPane tabId="5">
              <DeveloperRoleDescription />
            </TabPane>
          </TabContent>
        </Col>
      </Row>
    </Container>
  );
};

// Export PermissionPage Component.
export default PermissionPage;
