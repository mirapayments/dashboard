import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import ListUser from "../Users";
import { data } from "../../../../../fixures/store";
import server from "../../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <ListUSer/> renders", () => {
  const store = mockStore(data);
  it("check if <ListUser /> lists user", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <ListUser />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      expect(screen.getByText("CI/CDtest@gmail.com")).toBeInTheDocument();
      expect(screen.getAllByTestId("user-table-row").length).toBe(1);
    });
  });
});
