import React, { useEffect, useState } from "react";
import { Table } from "reactstrap";
import styles from "./Users.module.css";
import { listUsersTiedToAccount } from "../../../../utils/apiHandlers/ListUsersTiedToAccount";
import { useSelector } from "react-redux";
import { removeUserFromAccount } from "../../../../utils/apiHandlers/removeUserFromAccount";
import Spinner from "react-spinner-material";
import Swal from "sweetalert2";
import { formatDate } from "../../../../utils/dateFormater/dateFormater";

const header = [
  { id: 0, title: "Active" },
  { id: 1, title: "Full Name" },
  { id: 2, title: "Email" },
  { id: 3, title: "Role" },
  { id: 4, title: "Last Login" },
];

const ListUsers = () => {
  //   const dispatch = useDispatch()
  const { signedInAccountNumber, role, email } = useSelector(
    (state) => state.user
  );
  const [attachedUsers, setAttachedUsers] = useState([]);
  const [clickedButtonId, setClickedButtonId] = useState("");
  const { userInvited } = useSelector((state) => state.ui);
  const [userId, setUserId] = useState("");
  const [removingUser, setRemovingUser] = useState(false);
  useEffect(() => {
    async function listUsersTiedToAccountHandler() {
      const userList = await listUsersTiedToAccount(signedInAccountNumber);
      setAttachedUsers(userList.data.data);
    }

    listUsersTiedToAccountHandler();
  }, [signedInAccountNumber, userId, userInvited]);

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  async function removeUserFromAccountHandler(id) {
    setClickedButtonId(id);
    setRemovingUser(true);
    try {
      const response = await removeUserFromAccount(signedInAccountNumber, id);
      setUserId(id);
      if (response.data.status) {
        swalWithBootstrapButtons.fire("Deleted!", "User deleted", "success");
      } else {
        swalWithBootstrapButtons.fire({
          title: "Error!",
          text: response.data.detail,
          icon: "error",
          showConfirmButton: false,
          timer: 3000,
        });
      }
    } catch (error) {
      swalWithBootstrapButtons.fire({
        title: "Error!",
        text: "An error occured",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }

  function confirmDelete(event) {
    const id = event.target.id;

    swalWithBootstrapButtons
      .fire({
        title: "Are you sure you want to delete user?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          removeUserFromAccountHandler(id);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "User deletion canceled",
            "error"
          );
        }
      });
  }
  return (
    <Table responsive className="table--bordered dashboard__audience-table">
      <thead>
        <tr>
          {header.map((item) => (
            <th className={styles.thead_overide} key={item.id}>
              <p>{item.title}</p>
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {attachedUsers?.map((item) => (
          <tr key={item.id} data-testid="user-table-row">
            <td className={styles.tdstyles}>
              <div>
                {item.is_active ? (
                  <div
                    style={{
                      width: "10px",
                      height: "10px",
                      borderRadius: "100%",
                      backgroundColor: "green",
                    }}
                  ></div>
                ) : (
                  <div
                    style={{
                      width: "10px",
                      height: "10px",
                      borderRadius: "100%",
                      backgroundColor: "red",
                    }}
                  ></div>
                )}
              </div>
            </td>
            <td className={styles.tdstyles}>
              <p>
                {item.first_name} {item.last_name}
              </p>
            </td>
            <td className={styles.tdstyles}>
              <p>{item.email}</p>
            </td>
            <td className={styles.tdstyles}>
              <p>{item.role}</p>
            </td>
            <td className={styles.tdstyles}>
              <p>{formatDate(item.last_login)}</p>
            </td>

            <td className={styles.tdstyles}>
              {
                <button
                  disabled={
                    email === item.email ||
                    (item.role == "Owner" && role == "Admin")
                  }
                  type="submit"
                  id={item.id}
                  onClick={(event) => confirmDelete(event)}
                  className={`${styles.button} btn btn-primary account__btn account__btn--small`}
                >
                  {item.id === clickedButtonId ? (
                    !removingUser ? (
                      <div
                        style={{
                          display: "grid",
                          placeItems: "center",
                        }}
                      >
                        <Spinner
                          radius={20}
                          color={"#333"}
                          stroke={2}
                          visible={true}
                        />
                      </div>
                    ) : (
                      "Removing"
                    )
                  ) : (
                    "Remove"
                  )}
                </button>
              }
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default ListUsers;
