import React from "react";
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from "reactstrap";
import classnames from "classnames";
import ListAccounts from "../components/ListAccount";
import styles from "../components/ListAccount.module.css";
// import ButtonIcon from "../../../../shared/img/Vector.svg";
import Modal from "../components/ModalCreateAccount/Modal";
import ModalInviteUser from "../components/ModalInviteUser/Modal";
import GeneralForm from "../Profile/General";
import ListUserTiedToAccount from "../Users/Users";
import Api from "../Api/Api";
import { setTabPosition } from "../../../../redux/features/user/uiSlice";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router";
import PermissionPage from "../components/RolesPermission";
import AccountSetting from "../AccountSettings/AccountSetting";

const Tabs = () => {
  const dispatch = useDispatch();
  const { tabPosition } = useSelector((state) => state.ui);
  const { role } = useSelector((state) => state.user);
  // const [activeTab, setActiveTab] = useState("1");
  const location = useLocation();
  console.log("CURRENT ROUTE", location.pathname);
  const rolePermissionRoute = "/pages/settings/user/rolepermissions";
  const permissionPage = location?.state?.rolepermissions;
  const permissionPath = location.pathname === rolePermissionRoute;
  const toggle = (tab) => {
    if (tabPosition !== tab) dispatch(setTabPosition(tab));
  };

  return (
    <div className="tabs__wrap">
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: tabPosition === "1" })}
            onClick={() => toggle("1")}
          >
            Profile
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: tabPosition === "2" })}
            onClick={() => toggle("2")}
          >
            Payout Account
          </NavLink>
        </NavItem>
        {/* Only show tab when user is admin or Owner */}
        {(role == "Owner" || role == "Admin") && (
          <NavItem>
            <NavLink
              className={classnames({ active: tabPosition === "3" })}
              onClick={() => toggle("3")}
            >
              Users
            </NavLink>
          </NavItem>
        )}
        <NavItem>
          <NavLink
            className={classnames({ active: tabPosition === "4" })}
            onClick={() => toggle("4")}
          >
            Accounts
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: tabPosition === "5" })}
            onClick={() => toggle("5")}
          >
            Account Settings
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: tabPosition === "6" })}
            onClick={() => toggle("6")} data-testid={"api"} 
          >
            API
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={tabPosition}>
        <TabPane tabId="1">
          <GeneralForm />
        </TabPane>
        <TabPane tabId="2">
          <p>
            Direction has strangers now believing. Respect enjoyed gay far
            exposed parlors towards. Enjoyment use tolerably dependeo peculiar
            in handsome together unlocked do by.
          </p>
        </TabPane>
        <TabPane tabId="3">
          {permissionPage || permissionPath ? (
            <PermissionPage />
          ) : (
            <>
              <div className={styles.Account_buttons}>
                <div className={styles.button_newaccount}>
                  <ModalInviteUser />
                </div>
              </div>
              <ListUserTiedToAccount />
            </>
          )}
        </TabPane>
        <TabPane tabId="4">
          {/* Modal create account button */}
          <div>
            <div className={styles.Account_buttons}>
              <div className={styles.button_newaccount}>
                <Modal />
              </div>
            </div>
            {/* List user accounts component */}

            <ListAccounts />
          </div>
        </TabPane>
        <TabPane tabId="5">
          <AccountSetting/>
        </TabPane>
        <TabPane tabId="6">
          <Api />
        </TabPane>
      </TabContent>
    </div>
  );
};

export default Tabs;
