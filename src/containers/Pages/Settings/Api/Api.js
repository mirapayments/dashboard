import React, { useState, useEffect } from "react";
import { Container } from "reactstrap";
import Spinner from "react-spinner-material";
import styles from "./Api.module.css";
import EyeIcon from "mdi-react/EyeIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import LinkVariantIcon from "mdi-react/LinkVariantIcon";
import Swal from "sweetalert2";
import { refreshApiKeys } from "../../../../utils/apiHandlers/refreshApiKeys";
import { Alert } from "reactstrap";
import { getAccountCredentials } from "../../../../utils/apiHandlers/getAccountCredentials";
import { changeAccountCredentials } from "../../../../utils/apiHandlers/changeAccountCredentials";
import { validateUrl } from "../../../../utils/validateUrl/validateUrl";
import { useSelector } from "react-redux";

const Api = () => {
  const [isTestKeyShown, setIsTestKeyShown] = useState(false);
  const [liveCallBackUrl, setLiveCallBackUrl] = useState("");
  const [testCallBackUrl, setTestCallBackUrl] = useState("");
  const [isLiveKeyShown, setIsLiveKeyShown] = useState(false);
  const [liveErrorValidation, setLiveErrorValidation] = useState(false);
  const [apiInfo, setApiInfo] = useState({});
  const [isChanging, setIsChanging] = useState(false);
  const [updatingLiveCallback, setUpdatingLiveCallback] = useState(false);
  const [updatingTestCallback, setUpdatingTestCallback] = useState(false);
  const [webHookUpdated, setWebHookUpdated] = useState(false);

  const [errorValidation, setErrorValidation] = useState(false);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  function showPassword() {
    setIsTestKeyShown((prev) => !prev);
  }
  function showLiveKey() {
    setIsLiveKeyShown((prev) => !prev);
  }
  useEffect(() => {
    getAccountCredentialsHandler();
  }, [signedInAccountNumber, webHookUpdated]);

  async function getAccountCredentialsHandler() {
    try {
      const response = await getAccountCredentials(signedInAccountNumber);
      setApiInfo(response?.data?.data);
    } catch (error) {
      console.log(error);
    }
  }

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: true,
  });

  async function changeAccountCredentialsHandler() {
    setIsChanging(true);
    try {
      const response = await refreshApiKeys(signedInAccountNumber);
      if (response.data.status) {
        Swal.fire({
          title: "Success",
          text: "Api keys have been updated, kindly update in your code where necessary",
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        setApiInfo(response?.data?.data);
        setTimeout(() => setIsChanging(false), 500);
      } else {
        Swal.fire({
          icon: "error",
          showConfirmButton: false,
          timer: 3000,
        });
        setTimeout(() => setIsChanging(false), 500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      setTimeout(() => setIsChanging(false), 500);
    }
  }

  function confirmKeyChange(e) {
    e.preventDefault();
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure you want to change Api Keys?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ok",
        cancelButtonText: "Cancel",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          changeAccountCredentialsHandler();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Action canceled",
            "error"
          );
        }
      });
  }

  async function updateCallBackUrl(e, payload) {
    e.preventDefault();
    try {
      const response = await changeAccountCredentials(
        signedInAccountNumber,
        JSON.stringify(payload)
      );
      if (response.data.status) {
        Swal.fire({
          title: "Success",
          text: "Webhook Url has been updated successfully",
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        setWebHookUpdated((prev) => !prev);
        setUpdatingLiveCallback(false);
        setUpdatingTestCallback(false);
      } else {
        Swal.fire({
          title: "Error",
          text: "An error occured",
          icon: "error",
          showConfirmButton: false,
          timer: 3000,
        });
        setUpdatingLiveCallback(false);
        setUpdatingTestCallback(false);
      }
    } catch (error) {
      Swal.fire({
        title: "Error",
        text: "An error occured",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      setUpdatingTestCallback(false);
      setUpdatingLiveCallback(false);
    }
  }

  return (
    <Container>
      <div className={styles.container}>
        <h3 className="mb-4">API Configuration - Test Mode</h3>
        <p>These keys are for testing only, DO NOT use them in production</p>
        <form className={`form ${styles.form} mt-4`}>
          <div className="form__form-group">
            <span className="form__form-group-label">Test Secret Key</span>
            <div className={`form__form-group-field`}>
              <div className="form__form-group-icon">
                <KeyVariantIcon />
              </div>
              <input
                name="password"
                type={isTestKeyShown ? "text" : "password"}
                placeholder="Test Secret Key"
                value={apiInfo?.test_token}
              />
              <button
                className={`form__form-group-button${
                  isTestKeyShown ? " active" : ""
                }`}
                onClick={() => showPassword()}
                type="button"
              >
                <EyeIcon />
              </button>
            </div>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Test Public Key</span>
            <div className={`form__form-group-field`}>
              <div className="form__form-group-icon">
                <KeyVariantIcon />
              </div>
              <input
                name="test_pub_key"
                type="text"
                placeholder="Test Public Key"
                value={apiInfo?.test_pub_key}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Test Web Hook Url</span>
            <div className={`form__form-group-field `}>
              <div className="form__form-group-icon">
                <LinkVariantIcon />
              </div>
              <input
                type="url"
                placeholder="Test Webhook Url"
                value={testCallBackUrl || apiInfo?.test_webhook_url}
                onChange={(e) =>{
                  setTestCallBackUrl(e.target.value);
                  setApiInfo({
                    ...apiInfo,
                    test_webhook_url: e.target.value
                  });

                } }
              />
            </div>
          </div>
          {
            <p
              style={{ visibility: "hidden" }}
              className={`${
                errorValidation && styles.validation_text
              } error_validation_text`}
            >
              Please provide a valid url
            </p>
          }
          <div className={styles.button_containers}>
            <button
              onClick={(e) => {
                e.preventDefault();
                setUpdatingTestCallback(true);
                if (validateUrl(testCallBackUrl)) {
                  const payload = {
                    test_webhook_url: testCallBackUrl,
                    live_webhook_url: apiInfo.live_webhook_url,
                  };
                  updateCallBackUrl(e, payload);
                } else {
                  setErrorValidation(true);
                  setTimeout(() => setUpdatingTestCallback(false), 800);
                  setTimeout(() => setErrorValidation(false), 800);
                }
              }}
              className={`btn btn-primary account__btn account__btn--small ${styles.apiform_buttons}`}
              data-testid={"save"}
            >
              {updatingTestCallback ? (
                <div className={styles.button_elem}>
                  <Spinner
                    radius={20}
                    color={"#333"}
                    stroke={2}
                    visible={true}
                  />
                </div>
              ) : (
                "Save"
              )}
            </button>
          </div>
        </form>
        <h3 className="mt-4">API Configuration - Live Mode</h3>
        <form className={`form ${styles.form} mt-4`}>
          <div style={{ width: "100%" }}>
            <Alert variant="success">
              Please be informed that these are your live keys. DO NOT expose to
              the public.
            </Alert>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Live Secret Key</span>
            <div className={`form__form-group-field`}>
              <div className="form__form-group-icon">
                <KeyVariantIcon />
              </div>
              <input
                name="password"
                type={isLiveKeyShown ? "text" : "password"}
                placeholder="Live Secret Key"
                value={apiInfo?.live_token}
              />
              <button
                className={`form__form-group-button${
                  isLiveKeyShown ? " active" : ""
                }`}
                onClick={() => showLiveKey()}
                type="button"
              >
                <EyeIcon />
              </button>
            </div>
          </div>
          <div className="form__form-group">
            <span className="form__form-group-label">Live Public Key</span>
            <div className={`form__form-group-field`}>
              <div className="form__form-group-icon">
                <KeyVariantIcon />
              </div>
              <input
                name="test_pub_key"
                type="text"
                placeholder="Live Public Key"
                value={apiInfo?.live_pub_key}
              />
            </div>
          </div>

          <div className="form__form-group">
            <span className="form__form-group-label">Live Web Hook Url</span>
            <div className={`form__form-group-field `}>
              <div className="form__form-group-icon">
                <LinkVariantIcon />
              </div>
              <input
                type="url"
                placeholder="Webhook Url"
                value={liveCallBackUrl || apiInfo?.live_webhook_url}
                onChange={(e) =>{
                  setLiveCallBackUrl(e.target.value);
                  setApiInfo({
                    ...apiInfo,
                    live_webhook_url: e.target.value
                  });
                } }
              />
            </div>
          </div>
          {
            <p
              style={{ visibility: "hidden" }}
              className={`${
                liveErrorValidation && styles.validation_text
              } error_validation_text`}
            >
              Please provide a valid url
            </p>
          }
          <div className={styles.button_containers}>
            <button
              onClick={(e) => {
                e.preventDefault();
                setUpdatingLiveCallback(true);
                if (validateUrl(liveCallBackUrl)) {
                  const payload = {
                    live_webhook_url: liveCallBackUrl,
                    test_webhook_url: apiInfo.test_webhook_url,
                  };
                  updateCallBackUrl(e, payload);
                } else {
                  setLiveErrorValidation(true);
                  setTimeout(() => setUpdatingLiveCallback(false), 800);
                  setTimeout(() => setLiveErrorValidation(false), 800);
                }
              }}
              className={`btn btn-primary account__btn account__btn--small ${styles.apiform_buttons}`}
            >
              {updatingLiveCallback ? (
                <div className={styles.button_elem}>
                  <Spinner
                    radius={20}
                    color={"#333"}
                    stroke={2}
                    visible={true}
                  />
                </div>
              ) : (
                "Save"
              )}
            </button>
            <button
              onClick={confirmKeyChange}
              style={{ width: "137px", backgroundColor: "#E91E63" }}
              className={`btn btn-primary account__btn account__btn--small ${styles.apiform_buttons}`}
            >
              {isChanging ? (
                <div className={styles.button_elem}>
                  <Spinner
                    radius={20}
                    color={"#333"}
                    stroke={2}
                    visible={true}
                  />
                </div>
              ) : (
                "Refresh Keys"
              )}
            </button>
          </div>
        </form>
      </div>
    </Container>
  );
};

export default Api;
