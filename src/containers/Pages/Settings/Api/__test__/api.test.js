import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import Api from "../Api";
import { data } from "../../../../../fixures/store";
import server from "../../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <Api /> renders", () => {
  const store = mockStore(data);
  it("check if <Api /> contains key, buttons", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Api />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      expect(screen.getByPlaceholderText("Live Public Key").value).toBe(
        "live_pk_1sitob9z20zkd9tnbtoi54pfxqbpqfjrd9ele9g6"
      );
      expect(screen.getAllByText("Save").length).toBe(2);
    });
  });
});
