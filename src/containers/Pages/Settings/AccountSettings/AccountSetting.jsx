import React from 'react';
import { Col, Row, Container, Input, Label } from 'reactstrap';
import styles from './AccountSetting.module.css';

const AccountSetting = () => {
  return (
    <>
      <header className="d-flex justify-content-between align-items-center">
        <h5 className="bold-text">Account Settings</h5>
        <button className="btn btn-primary px-5 mb-0">Save</button>
      </header>
      <hr />
      <Container>
        <Row className='pb-2'>
          <Col md={4}>
            <p>Who should pay the transaction fees?</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Make customers pay the transaction fees</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Charge me for the transaction fees</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Transaction Emails</p>
          </Col>
          <Col md={8}>
            <div>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Email me for every transaction</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Transaction Emails(Customers)</p>
          </Col>
          <Col md={8}>
            <div>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Email customers for every transaction</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>What methods of payment do you want?</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Dashboard Payment Options</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Card Payments</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable PayPal</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Bank Payments</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Barter Payments</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Mobile Money</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Visa QR</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable PayAltitude</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Bank Transfer</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable USSD</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Send notification emails to other users</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Send to the business email address only</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Send to all dashboard users</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Send to specific users only</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>How do you want to get your earnings?</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Settle to bank account</p></Label>
            </div>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Settle to wallet(Available Balance)</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Two Factor Authentication for Transfers</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Two Factor Authentication for Transfers</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Transfer Receipts</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Send email receipts for successful transfers</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Two Factor Authentication for Login</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable Two Factor Authentication for Login</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Subscription Emails</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Allow customers cancel subscriptions</p></Label>
            </div>
          </Col>
        </Row>
        <Row className='pb-3'>
          <Col md={4}>
            <p>Balance History</p>
          </Col>
          <Col md={8}>
            <div className='pb-2'>
              <Input className={styles.checkboxes} type="checkbox" />
              <Label check className='ml-2 align-text-top'><p>Enable balance history for your ledger balance</p></Label>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AccountSetting;
