import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import AccountSetting from "../AccountSetting";
import { data } from "../../../../../fixures/store";
import server from "../../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <AccountSetting/> renders", () => {
  const store = mockStore(data);
  it("check if contains btn and texts", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <AccountSetting />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      expect(screen.getByText("Save")).toBeInTheDocument();
      expect(screen.getByText("Make customers pay the transaction fees")).toBeInTheDocument();
    });
  });
});
