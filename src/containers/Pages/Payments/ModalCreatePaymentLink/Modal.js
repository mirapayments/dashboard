import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "reactstrap";
import classNames from "classnames";
import Form from "../ModalCreatePaymentLink/Form/Form";
import { useSelector } from "react-redux";
import AddCircleOutlineIcon from "mdi-react/AddCircleOutlineIcon";

const ModalComponent = ({ color, colored, header }) => {
  const [modal, setModal] = useState(false);
  const { className } = useSelector((state) => state.theme);

  const toggle = () => {
    setModal((prevState) => !prevState);
  };

  // let Icon = <span className="lnr lnr-pushpin modal__title-icon" />;

  const modalClass = classNames({
    "modal-dialog--colored": colored,
    "modal-dialog--header": header,
  });

  return (
    <div>
      <div
        styles={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        {/*  Button triggers the modal*/}

        <>
          <button
            color={color}
            onClick={toggle}
            type="submit"
            data-testid="create-payment-link"
            className="icon btn btn-primary mb-0 button-text"
          >
            <AddCircleOutlineIcon className="mb-1" fontSize="large" /> New
            Payment Link{" "}
          </button>
        </>
      </div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <div className="modal__body">
          <Form setModalOpen={setModal} />
        </div>
      </Modal>
    </div>
  );
};

ModalComponent.propTypes = {
  header: PropTypes.bool,
  colored: PropTypes.bool,
  color: PropTypes.string,
};

ModalComponent.defaultProps = {
  title: "",
  message: "",
  colored: false,
  header: false,
};

export default ModalComponent;
