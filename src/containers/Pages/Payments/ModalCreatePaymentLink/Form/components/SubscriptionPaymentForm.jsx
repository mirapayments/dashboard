import React, { useState } from "react";
import { Formik, FieldArray, Field } from "formik";
import * as Yup from "yup";
import PropTypes from "prop-types";
import styles from "./PaymentLinkForm.module.css";
import ArrowDropDownIcon from "mdi-react/ArrowDropDownIcon";
import ArrowDropUpIcon from "mdi-react/ArrowDropUpIcon";
import AccountCircleIcon from "mdi-react/AccountCircleIcon";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import CalenderClockIcon from "mdi-react/CalendarClockIcon";
import InfoIcon from "mdi-react/InformationIcon";
import MoneyIcon from "mdi-react/MoneyIcon";
import PushPinIcon from "mdi-react/PinIcon";
import LinkIcon from "mdi-react/LinkIcon";
import AttachMoney from "mdi-react/AttachMoneyIcon";
import DescriptionIcon from "mdi-react/ImageDescriptionIcon";
import Numeric0CircleIcon from "mdi-react/Numeric0CircleIcon";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "react-spinner-material";
import Swal from "sweetalert2";
import { createPaymentLink } from "../../../../../../utils/apiHandlers/createPaymentLink";
import useOnlineStatus from "../../../../../../utils/hooks/useOnlineStatus";
import { setPaymentLinkUpdated } from "../../../../../../redux/features/user/uiSlice";
import { Container } from "reactstrap";
import { setLimitBasedOnCurrency } from "../../../../../../utils/setLimitBasedOnCurrency/setLimitBasedOnCurrency";

const SubscriptionPaymentForm = ({ setModal }) => {
  //theme hack for custom select styles
  const { className } = useSelector((state) => state.theme);
  const { paymentLinkUpdated } = useSelector((state) => state.ui);
  const online = useOnlineStatus();
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const [advancedForms, setAdvancedForms] = useState(false);
  const [currency, setCurrency] = useState(0);
  const dispatch = useDispatch();

  //parse string true or false to boolean
  function parseBool(b) {
    return !/^(false|0)$/i.test(b) && !!b;
  }

  async function createPaymentLinkHandler(data, setSubmitting) {
    //check if user is offline or online
    if (!online) {
      setSubmitting(false);
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });

      return;
    }
    
    //check if optional form fields are cleared out
    if(data.number_of_charges === "") data.number_of_charges = null;

    try {
      const res = await createPaymentLink(
        signedInAccountNumber,
        JSON.stringify(data)
      );
      if (res.data.status) {
        Swal.fire({
          title: "",
          text: res.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        setSubmitting(false);
        dispatch(setPaymentLinkUpdated(!paymentLinkUpdated));
        setModal(false);
      }
      if (!res.data.status) {
        Swal.fire({
          title: "",
          text: res.data.detail,
          icon: "error",
          showConfirmButton: false,
          timer: 3000,
        });
        setSubmitting(false);
      }
    } catch (error) {
      Swal.fire({
        title: "",
        text: error,
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      setSubmitting(false);
    }
  }

  // const fixedPaymentRequired = {
  //   is: false,
  //   then: Yup.string().required(),
  // };

  return (
    <Formik
      initialValues={{
        amount: "0.00",
        amount_currency: "",
        interval:"Hourly",
        name: "",
        fixed_amount: false,
        redirect_url: "",
        description: "",
        notify: "",
        payment_type: "Subscription",
        extra_info: [],
        number_of_charges: null
      }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true);
        //helper function to remove empty keys in object
        function removeEmpty(obj) {
          return Object.fromEntries(
            Object.entries(obj).filter(([v]) => v.length != 0)
          );
        }
        const filteredObj = removeEmpty(values);

        createPaymentLinkHandler(filteredObj, setSubmitting);
      }}
      validationSchema={Yup.object().shape(
        {
          name: Yup.string().required("Please input your name"),
          amount_currency: Yup.string().required(
            "Please select a currency type"
          ),
          interval:Yup.string().required(
            "Please select an interval"
          ),
          description: Yup.string().required(
            "Please input your description for this payment."
          ),
          fixed_amount: Yup.boolean().notRequired(),

          amount: Yup.number().when("fixed_amount", {
            is: true,
            then: Yup.number()
              .min(setLimitBasedOnCurrency(currency), `Amount should be greater than or equal to ${setLimitBasedOnCurrency(currency)}`)
              .required("Please enter the required amount"),
            otherwise: Yup.number().notRequired(),
          }),
          redirect_url: Yup.string().nullable().notRequired(),
          extra_info: Yup.array().notRequired(),
          notify: Yup.string().nullable().notRequired(),
          payment_type: Yup.string().nullable().notRequired(),
          number_of_charges: Yup.number()
            .nullable()
            .min(0, "should be greater than or equal to 0")
            .notRequired(),
        },
        [["fixed_amount"]]
      )}
    >
      {({ values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit }) =>
        (
          <form
            style={{
              height: "10%",
            }}
            className={`form ${
              className !== "theme-light" && styles.dark_background
            }`}
            onSubmit={handleSubmit}
          >
            {console.log(values.fixed_amount)}
            <Container>
              <div className={`form__form-group`}>
                <div className="account__head mt-4">
                  <h4 className="account__subhead subhead">Subscription</h4>
                  <p>Please fill in your payment link details</p>
                </div>
                <span className="form__form-group-label">Name</span>
                <div
                  className={`form__form-group-field ${
                    errors.name && "form__form-validation"
                  }`}
                >
                  <div className="form__form-group-icon">
                    <AccountCircleIcon />
                  </div>
                  <input
                    name="name"
                    component="input"
                    placeholder="Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                  />
                </div>
                <p className="error_validation_text">
                  {errors.name && touched.name && errors.name}
                </p>
              </div>

              <div className="form__form-group">
                <span className="form__form-group-label">Currency</span>
                <div
                  className={`${styles.select_form_custom_container} ${
                    errors.amount_currency && "form__form-validation"
                  }`}
                >
                  <div
                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                  >
                    <AttachMoney />
                  </div>
                  <select
                    className={
                      className == "theme-light"
                        ? styles.select_form_custom_light
                        : styles.select_form_custom_dark
                    }
                    name="amount_currency"
                    component="select"
                    type="select"
                    placeholder="Currency"
                    onChange={(e) => {
                      setCurrency(e.currentTarget.value);
                      handleChange(e);
                    }}
                    onBlur={handleBlur}
                    value={values.amount_currency}
                  >
                    <option label={"Select a currency"} />
                    <option value={"NGN"} label={"NGN"} />
                    <option value={"USD"} label={"USD"} />
                    <option value={"EUR"} label={"EUR"} />
                    <option value={"GBP"} label={"GBP"} />
                  </select>
                </div>
                <p className="error_validation_text">
                  {errors.amount_currency &&
                    touched.amount_currency &&
                    errors.amount_currency}
                </p>
              </div>

              <div className="form__form-group">
                <span className="form__form-group-label">Interval</span>
                <div
                  className={`${styles.select_form_custom_container} ${
                    errors.amount_currency && "form__form-validation"
                  }`}
                >
                  <div
                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                  >
                    <CalenderClockIcon />
                  </div>
                  <select
                    className={
                      className == "theme-light"
                        ? styles.select_form_custom_light
                        : styles.select_form_custom_dark
                    }
                    name="interval"
                    component="select"
                    type="select"
                    placeholder="Interval"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.interval}
                  >
                    <option label={"Select an Interval"} />
                    <option value={"Hourly"} label={"Hourly"} />
                    <option value={"Daily"} label={"Daily"} />
                    <option value={"Weekly"} label={"Weekly"} />
                    <option value={"Monthly"} label={"Monthly"} />
                    <option value={"Quarterly"} label={"Quarterly"} />
                    <option value={"Half-Yearly"} label={"Half-Yearly"} />
                    <option value={"Yearly"} label={"Yearly"} />
                    
                  </select>
                </div>
                <p className="error_validation_text">
                  {errors.interval &&
                    touched.interval &&
                    errors.interval}
                </p>
              </div>

              <div className={`form__form-group`}>
                <span className="form__form-group-label">Number of times to charge a subscriber
                (Leave empty to charge subscriber indefinitely)</span>
                  <div
                    className={`form__form-group-field ${
                    errors.number_of_charges && "form__form-validation"
                    }`}
                  >
                    <div className="form__form-group-icon">
                        <Numeric0CircleIcon />
                      </div>
                      <input
                        name="number_of_charges"
                        type="number"
                        placeholder=""
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.number_of_charges}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.number_of_charges && touched.number_of_charges && errors.number_of_charges}
                    </p>
              </div>

              <div className="form__form-group">
                <span className="form__form-group-label">
                  Is this a fixed Amount?
                </span>
                <div className={`${styles.select_form_custom_container}`}>
                  <div
                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                  >
                    <PushPinIcon />
                  </div>
                  <select
                    className={
                      className == "theme-light"
                        ? styles.select_form_custom_light
                        : styles.select_form_custom_dark
                    }
                    name="fixed_amount"
                    component="select"
                    type="select"
                    placeholder="Fixed Amount?"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.fixed_amount}
                  >
                    {/* <option  label={"Select an option"}/> */}
                    <option value={true} label={"Yes"}>Yes</option>
                    <option value={false} label={"No"}>No</option>
                  </select>
                </div>
                <p className="error_validation_text">
                  {errors.fixed_amount &&
                    touched.fixed_amount &&
                    errors.fixed_amount}
                </p>
              </div>
              {parseBool(values.fixed_amount) && (
                <>
                  <div className={`form__form-group`}>
                    <span className="form__form-group-label">Amount</span>
                    <div
                      className={`form__form-group-field ${
                        errors.amount && "form__form-validation"
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <MoneyIcon />
                      </div>
                      <input
                        name="amount"
                        type="number"
                        placeholder="Amount"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.amount}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.amount && touched.amount && errors.amount}
                    </p>
                  </div>
                </>
              )}

              <div className={`form__form-group`}>
                <span className="form__form-group-label">Description</span>
                <div
                  className={`form__form-group-field ${
                    errors.description && "form__form-validation"
                  }`}
                >
                  <div className="form__form-group-icon">
                    <DescriptionIcon />
                  </div>
                  <input
                    name="description"
                    component="input"
                    type="textarea"
                    placeholder="Please enter a description for this payment link"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                  />
                </div>
                <p className="error_validation_text">
                  {errors.description &&
                    touched.description &&
                    errors.description}
                </p>
              </div>
              <div className={styles.advance_button_container}>
                <div
                  onClick={(e) => {
                    e.preventDefault();
                    setAdvancedForms((prev) => !prev);
                  }}
                  disabled={isSubmitting}
                  className={`${styles.button_advanced_toggle} btn--small`}
                >
                  {advancedForms ? <ArrowDropDownIcon /> : <ArrowDropUpIcon />}
                  Advanced Options
                </div>
              </div>
              {advancedForms && (
                <>
                  <div className={`form__form-group`}>
                    <span className="form__form-group-label">
                      Notification Email
                    </span>
                    <div
                      className={`form__form-group-field ${
                        errors.notify && "form__form-validation"
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <AlternateEmailIcon />
                      </div>
                      <input
                        name="notify"
                        type="email"
                        component="input"
                        placeholder="Notification Email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.notify}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.notify && touched.notify && errors.notify}
                    </p>
                  </div>
                  <div className={`form__form-group`}>
                    <span className="form__form-group-label">Redirect Url</span>
                    <div
                      className={`form__form-group-field ${
                        errors.redirect_url && "form__form-validation"
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <LinkIcon />
                      </div>
                      <input
                        name="redirect_url"
                        type="url"
                        component="input"
                        placeholder="Redirect Url"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.redirect_url}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.redirect_url &&
                        touched.redirect_url &&
                        errors.redirect_url}
                    </p>
                  </div>
                  <div className={`form__form-group`}>
                    <span className="form__form-group-label">
                      Extra Info (press the - or + keys to add or remove field)
                    </span>
                    <div className={`form__form-group-field `}>
                      <FieldArray
                        className={styles.extra_info}
                        name="extra_info"
                        render={(arrayHelpers) => (
                          <div>
                            {values.extra_info &&
                            values.extra_info.length > 0 ? (
                              values.extra_info.map((extra_info, index) => (
                                <div
                                  style={{
                                    display: "flex",
                                    marginBottom: "20px",
                                  }}
                                  key={index}
                                >
                                  <div
                                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                                  >
                                    <InfoIcon />
                                  </div>
                                  <Field
                                    placeholder="Field Name"
                                    name={`extra_info.${index}`}
                                  />
                                  <button
                                    className={`${styles.extra_info_buttons} ${styles.button_advanced_toggle} `}
                                    type="button"
                                    onClick={() => arrayHelpers.remove(index)}
                                  >
                                    -
                                  </button>
                                  <button
                                    className={`${styles.extra_info_buttons} ${styles.button_advanced_toggle} `}
                                    type="button"
                                    onClick={() =>
                                      arrayHelpers.insert(index, "")
                                    }
                                  >
                                    +
                                  </button>
                                </div>
                              ))
                            ) : (
                              <button
                                type="button"
                                className={`${styles.button_advanced_toggle} btn--small`}
                                onClick={() => arrayHelpers.push("")}
                              >
                                Add Extra Info
                              </button>
                            )}
                          </div>
                        )}
                      />
                    </div>
                  </div>
                </>
              )}

              <button
                type="submit"
                disabled={isSubmitting}
                className="btn btn-primary account__btn account__btn--small"
              >
                {isSubmitting ? (
                  <div className={styles.button_elem}>
                    <Spinner
                      radius={20}
                      color={"#333"}
                      stroke={2}
                      visible={true}
                    />
                  </div>
                ) : (
                  "Create Payment Link"
                )}
              </button>
            </Container>
          </form>
        )
      }
    </Formik>
  );
};

SubscriptionPaymentForm.propTypes = {
  setModal: PropTypes.func
};

export default SubscriptionPaymentForm;
