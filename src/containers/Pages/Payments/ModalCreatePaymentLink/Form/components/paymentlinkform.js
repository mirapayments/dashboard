// /*

// return <Formik
// initialValues={{
// amount:"",
// amount_currency:"",
// name:"" ,
// code:"",
// fixed_amount: false,
// redirect_url:"",
// description:"",
// extra_info:"",
// notify:"",
// interval:"",
// donation_website:"",
// donation_phone:"",
// is_active:false,
// payment_type:"",
// }}
// enableReinitialize={true}

// onSubmit={(values, { setSubmitting }) => {
// //  ("value in form", values)
// alert("form submits!")
//   setSubmitting(true);
//   createPaymentLinkHandler(values, setSubmitting)
// }}

// validationSchema={Yup.object().shape({
//   amount_currency: Yup.string().notRequired()
//   .when('fixed_amount', {
//   is: (val) => val == false || 'false',
//   then: Yup.string().notRequired(),
//   otherwise: Yup.string().required('Please select a currency'),
//   }),
//   name: Yup.string()
//     .required("Please provide a name.")
//     .min(4, "Name is too short.")
//     ,
// description:  Yup.string().required('Please input your description for this transaction'),
// payment_type: Yup.string().required('Please select a payment type'),
// amount:  Yup.string().notRequired()
// .when('fixed_amount', {
// is: (val) => val == false || 'false',
// then: Yup.string().notRequired(),
// otherwise:   Yup.string().required('Please enter the required amount'),
// }),
// code: Yup.string().nullable().notRequired(),
// fixed_amount: Yup.string().nullable().notRequired(),
// redirect_url: Yup.string().nullable().notRequired(),
// extra_info:Yup.string().nullable().notRequired(),
// notify:Yup.string().nullable().notRequired(),
// interval:Yup.string().nullable().notRequired(),
// donation_website:Yup.string().nullable().notRequired(),
// donation_phone:Yup.string().nullable().notRequired(),
// is_active:Yup.string().nullable().notRequired(),
// })}
// >
// {props => {
//   const {
//     values,
//     touched,
//     errors,
//     isSubmitting,
//     handleChange,
//     handleBlur,
//     handleSubmit
//   } = props;
//   return ( <form style={{
//       height: "10%"
//     }}className="form" onSubmit={handleSubmit}>
//        <div className={`form__form-group`}>
//       <span className="form__form-group-label">Name</span>
//       <div className={`form__form-group-field ${errors.name &&  "form__form-validation"}`}>
//         <div className="form__form-group-icon">
//           <AccountBalanceWallet/>
//         </div>
//         <input
//           name="name"
//           component="input"
//           placeholder="Name"
//           onChange={handleChange}
//           onBlur={handleBlur}
//           value={values.name}
//         />
//       </div>
//       <p>
//         { (JSON.stringify(values))}
//       {errors.name && touched.name && errors.name}
//       </p>
//     </div>

//           <div className={`form__form-group`}>
//       <span className="form__form-group-label">Code</span>
//       <div className={`form__form-group-field ${errors.code &&  "form__form-validation"}`}>
//         <div className="form__form-group-icon">
//           <AccountBalanceWallet/>
//         </div>
//         <input
//           name="code"
//           type="text"
//           placeholder="Code"
//           onChange={handleChange}
//           onBlur={handleBlur}
//           value={values.code}
//         />
//       </div>
//       <p>
//       {errors.code && touched.code && errors.code}
//       </p>
//     </div>
//     <div className="form__form-group">
//             <span className="form__form-group-label">Is this a fixed Amount?</span>
//             <div className={`${styles.select_form_custom_container} ${errors.amount_currency &&  "form__form-validation"}`}>
//             <div className={`form__form-group-icon ${styles.select_form_custom_icon}`}>

//                 <AttachMoney />
//              </div>
//                 <select
//                 className={className == "theme-light" ? styles.select_form_custom_light : styles.select_form_custom_dark}
//                  name="fixed_amount"
//                  component="select"
//                  type="select"
//                   placeholder="Currency"
//                  onChange={handleChange}
//                  onBlur={handleBlur}
//                  value={values.fixed_amount}
//                 >
//                   {/* <option  label={"Select an option"}/> */}
//                   <option value={true}  label={"Yes"}/>
//                   <option value={false}  label={"No"}/>
//                 </select>
//             </div>
//                 <p>
//                   { (!!props.values.fixed_amount)}
//                    {errors.fixed_amount && touched.fixed_amount && errors.fixed_amount}
//                </p>
//           </div>
//           {parseBool(values.fixed_amount) && (<>
//             <div className={`form__form-group`}>
//             <span className="form__form-group-label">Amount</span>
//             <div className={`form__form-group-field ${errors.amount &&  "form__form-validation"}`}>
//               <div className="form__form-group-icon">
//                 <AccountBalanceWallet/>
//               </div>
//               <input
//                 name="amount"
//                 type="number"
//                 placeholder="Amount"
//                 onChange={handleChange}
//                 onBlur={handleBlur}
//                 value={values.amount}
//               />
//             </div>
//             <p>
//               { (values)}
//             {errors.amount && touched.amount && errors.amount}
//             </p>
//           </div>
//           <div className="form__form-group">
//                   <span className="form__form-group-label">Currency</span>
//                   <div className={`${styles.select_form_custom_container} ${errors.amount_currency &&  "form__form-validation"}`}>
//                   <div className={`form__form-group-icon ${styles.select_form_custom_icon}`}>

//                       <AttachMoney />
//                    </div>
//                       <select
//                       className={className == "theme-light" ? styles.select_form_custom_light : styles.select_form_custom_dark}
//                        name="amount_currency"
//                        component="select"
//                        type="select"
//                         placeholder="Currency"
//                        onChange={handleChange}
//                        onBlur={handleBlur}
//                        value={values.amount_currency}
//                       >
//                         <option  label={"Select a currency"}/>
//                         <option value={"NGN"}  label={"NGN"}/>
//                         <option value={"USD"}  label={"USD"}/>
//                         <option value={"EUR"}  label={"EUR"}/>
//                         {/* To fix */}
//                         {/* {
//                         currencyTypes.map(options=>{
//                           return <option value={options} key={options} label={options}/>
//                         })
//                       } */}
//                       </select>
//                   </div>
//                       <p>
//                          {errors.amount_currency && touched.amount_currency && errors.amount_currency}
//                      </p>
//                 </div>
//                 </>

//           )

//           }

//           <div className="form__form-group">
//             <span className="form__form-group-label">Payment Type</span>
//             <div className={`${styles.select_form_custom_container} ${errors.account_type &&  "form__form-validation"}`}>
//             <div className={`form__form-group-icon ${styles.select_form_custom_icon}`}>
//             <Public />
//              </div>
//                 <select
//                 className={className == "theme-light" ? styles.select_form_custom_light : styles.select_form_custom_dark}
//                  name="payment_type"
//                  component="select"
//                  type="select"
//                  placeholder="Payment Type"
//                  onChange={handleChange}
//                  onBlur={handleBlur}
//                  value={values.payment_type}
//                 >
//                   <option  label={"Select an account type"}/>
//                   <option value={"single"}  label={"single"}/>
//                   <option value={"subscription"}  label={"subscription"}/>
//                   <option value={"product"}  label={"product"}/>
//                   <option value={"donation"}  label={"donation"}/>
//                 </select>
//             </div>
//                 <p>
//                    {errors.payment_type && touched.payment_type && errors.payment_type}
//                </p>
//           </div>
//           <div className="form__form-group">
//             <span className="form__form-group-label">Payment Interval</span>
//             <div className={`${styles.select_form_custom_container} ${errors.account_type &&  "form__form-validation"}`}>
//             <div className={`form__form-group-icon ${styles.select_form_custom_icon}`}>
//             <Public />
//              </div>
//                 <select
//                 className={className == "theme-light" ? styles.select_form_custom_light : styles.select_form_custom_dark}
//                  name="interval"
//                  component="select"
//                  type="select"
//                  placeholder="Payment Interval"
//                  onChange={handleChange}
//                  onBlur={handleBlur}
//                  value={values.interval}
//                 >
//                   <option  label={"Select an interval for the payment"}/>
//                   <option value={"hourly"}  label={"hourly"}/>
//                   <option value={"daily"}  label={"daily"}/>
//                   <option value={"weekly"}  label={"weekly"}/>
//                   <option value={"monthly"}  label={"monthly"}/>
//                   <option value={"half-yearly"}  label={"half-yearly"}/>
//                   <option value={"yearly"}  label={"yearly"}/>

//                 </select>
//             </div>
//                 <p>
//                    {errors.interval && touched.interval && errors.interval}
//                </p>
//           </div>

//           <div className={`form__form-group`}>
//       <span className="form__form-group-label">Description</span>
//       <div className={`form__form-group-field ${errors.description &&  "form__form-validation"}`}>
//         <div className="form__form-group-icon">
//           <AccountBalanceWallet/>
//         </div>
//         <input
//           name="description"
//           component="input"
//           type="textarea"
//           placeholder="Please enter a description for this payment link"
//           onChange={handleChange}
//           onBlur={handleBlur}
//           value={values.description}
//         />
//       </div>
//       <p>
//       {errors.description && touched.description && errors.description}
//       </p>
//     </div>

//     <button type="submit" disabled={isSubmitting} className="btn btn-primary account__btn account__btn--small" >
//     {isSubmitting ?
//     <div className={styles.button_elem}>
//      <Spinner  radius={20} color={"#333"} stroke={2} visible={true} />
//       </div>
//       : "Create Payment Link"
//     }
//     </button>
//   </form>

//   );
// }}
// </Formik>
// };

// // SignUpForm.propTypes = {
// //   handleSubmit: PropTypes.func.isRequired,
// // };

// export default CreateAccountForm

// */
