import SinglePaymentForm from "./components/SinglePaymentForm";
import SubscriptionPaymentForm from "./components/SubscriptionPaymentForm";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { Modal } from "reactstrap";
import styles from "./components/PaymentLinkForm.module.css";
import { useSelector } from "react-redux";

const FormWrapper = ({ setModalOpen }) => {
  //const [productModalVisible, SetProductModalVisible] = useState(false);
  const [singleModalVisible, SetSingleModalVisible] = useState(false);
  //const [donationModalVisible, SetDonationModalVisible] = useState(false);
  const [subscriptionModalVisible, SetSubscriptionModalVisible] =
    useState(false);

  const { className } = useSelector((state) => state.theme);

  const toggleSingle = () => {
    SetSingleModalVisible((prevState) => !prevState);
  };
  // const toggleProduct = () => {
  //   SetProductModalVisible((prevState) => !prevState);
  // };
  // const toggleDonation = () => {
  //   SetDonationModalVisible((prevState) => !prevState);
  // };
  const toggleSubscription = () => {
    SetSubscriptionModalVisible((prevState) => !prevState);
  };

  // let Icon = <span className="lnr lnr-pushpin modal__title-icon" />;

  return (
    <div
      className="account"
      style={{
        minHeight: "0",
        height: "100%",
      }}
    >
      <div className="account__card">
        {/* <div className="account__head">
          <h4 className="account__subhead subhead">New Payment Link</h4>
          <p>Please select a payment type</p>
        </div> */}
        <div>
          <div
            styles={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <span className={styles.heading}>Single Charge</span>
            <span className={styles.subheading}>
              Single charge allows you create payment links for your customers.
            </span>
            <button
              data-testid="single-payment-btn"
              onClick={toggleSingle}
              type="submit"
              className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
            >
              Single Payment
            </button>
          </div>
          <Modal
            isOpen={singleModalVisible}
            toggle={toggleSingle}
            modalClassName={className}
            wrapClassName={className}
            backdropClassName={className}
            contentClassName={className}
          >
            <div className="modal__body">
              <SinglePaymentForm setModal={setModalOpen} />
            </div>
          </Modal>
        </div>

        {/* <div>
          <div
            styles={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <button
              onClick={toggleProduct}
              type="submit"
              className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
            >
              Product Payment
            </button>
          </div>
          <Modal
            isOpen={productModalVisible}
            toggle={toggleProduct}
            modalClassName={className}
            wrapClassName={className}
            backdropClassName={className}
            contentClassName={className}
          >
            <div className="modal__body">
              { <ProductPaymentForm setModal={setModalOpen} /> }
            </div>
          </Modal>
        </div> */}
        <hr />

        <div>
          <div
            styles={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <span className={styles.heading}>Subscription Link</span>
            <span className={styles.subheading}>
              Create links where your customers can subscribe to your services.
            </span>
            <button
              onClick={toggleSubscription}
              type="submit"
              className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
            >
              Subscription Payment
            </button>
          </div>
          <Modal
            isOpen={subscriptionModalVisible}
            toggle={toggleSubscription}
            modalClassName={className}
            wrapClassName={className}
            backdropClassName={className}
            contentClassName={className}
          >
            <div className="modal__body">
              {<SubscriptionPaymentForm setModal={setModalOpen} />}
            </div>
          </Modal>
        </div>

        {/* <div>
          <div
            styles={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <button
              onClick={toggleDonation}
              type="submit"
              className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
            >
              Donation Payment
            </button>
          </div>
          <Modal
            isOpen={donationModalVisible}
            toggle={toggleDonation}
            modalClassName={className}
            wrapClassName={className}
            backdropClassName={className}
            contentClassName={className}
          >
            <div className="modal__body">
              {<DonationPaymentForm setModal={setModalOpen} /> }
            </div>
          </Modal>
        </div> */}
      </div>
    </div>
  );
};

FormWrapper.propTypes = {
  setModalOpen: PropTypes.func,
};

export default FormWrapper;
