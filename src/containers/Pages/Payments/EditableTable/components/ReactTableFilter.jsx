import React, { useState } from "react";
import PropTypes from "prop-types";
import { useAsyncDebounce } from "react-table";
import { Input } from "reactstrap";
import styles from "./ReactTableFilter.module.css";
import ModalComponent from "../../ModalCreatePaymentLink/Modal";

const ReactTableFilter = ({
  rows,
  setGlobalFilter,
  setFilterValue,
  globalFilter,
  placeholder,
  dataLength,
}) => {
  const [value, setValue] = useState(globalFilter);
  const onChange = useAsyncDebounce((item) => {
    setGlobalFilter(item || undefined);
  }, 200);
  if (value) {
    setFilterValue(value);
  }

  return (
    <div className={styles._bar}>
      {dataLength !== rows.length && (
        <span className="mx-md-3">Found {rows.length} matches</span>
      )}
      <Input
        className={`${styles.search_bar} table__search table__search-input`}
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${placeholder}`}
      />
      <ModalComponent />
    </div>
  );
};

const filterGreaterThan = (rows, id, filterValue) =>
  rows.filter((row) => {
    const rowValue = row.values[id];
    return rowValue >= filterValue;
  });

filterGreaterThan.autoRemove = (val) => typeof val !== "number";

ReactTableFilter.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  setGlobalFilter: PropTypes.func.isRequired,
  setFilterValue: PropTypes.func,
  globalFilter: PropTypes.string,
  placeholder: PropTypes.string,
  dataLength: PropTypes.number.isRequired,
};

ReactTableFilter.defaultProps = {
  setFilterValue: () => {},
  globalFilter: undefined,
  placeholder: "Search...",
};

export default ReactTableFilter;
