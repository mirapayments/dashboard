import React, { useState } from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import ReactTableBase from "./ReactTableBase";
import ReactTableCustomizer from "./ReactTableCustomizer";

// eslint-disable-next-line react/prop-types
const EditableReactTable = ({ reactRowData, reactTableData }) => {
  const [withPagination, setWithPaginationTable] = useState(false);
  const [isSortable, setIsSortable] = useState(true);
  const [withSearchEngine, setWithSearchEngine] = useState(false);

  const handleClickIsSortable = () => {
    setIsSortable(!isSortable);
  };

  const handleClickWithPagination = () => {
    setWithPaginationTable(!withPagination);
  };

  const handleClickWithSearchEngine = () => {
    setWithSearchEngine(!withSearchEngine);
  };

  const tableConfig = {
    isEditable: true,
    isSortable: true,
    isResizable: false,
    withPagination: true,
    withSearchEngine: true,
    manualPageSize: [10, 20, 30, 40],
    placeholder: "Search by Name, Payment Type or Amount",
  };

  return (
    <Col md={12} lg={12}>
      <Card>
        <CardBody>
          <div>
            <ReactTableCustomizer
              className="float-left p-0"
              handleClickIsSortable={handleClickIsSortable}
              handleClickWithPagination={handleClickWithPagination}
              handleClickWithSearchEngine={handleClickWithSearchEngine}
              isSortable={isSortable}
              withPagination={withPagination}
              withSearchEngine={withSearchEngine}
            />

            <ReactTableBase
              key={withSearchEngine ? "searchable" : "common"}
              columns={reactTableData.tableHeaderData}
              data={reactRowData}
              tableConfig={tableConfig}
            />
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

EditableReactTable.propTypes = {
  reactTableData: PropTypes.shape({
    tableHeaderData: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        name: PropTypes.string,
      })
    ),
    tableRowsData: PropTypes.arrayOf(PropTypes.shape()),
  }).isRequired,
};
export default EditableReactTable;
