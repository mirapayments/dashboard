import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { ThemeProps } from "../../../../../shared/prop-types/ReducerProps";
import ReactTableDnDBody from "./ReactTableDnDBody";
import NoCustomers from "./undraw_upload_87y9 1.svg";
import styles from "./ReactTableBody.module.css";
import { useHistory } from "react-router";
import { useState } from "react";
import Skeleton from "react-loading-skeleton";
import { useSelector } from "react-redux";

const ReactTableDefaultBody = ({ page, getTableBodyProps, prepareRow }) => {
  const history = useHistory();
  const { paymentLinkListLoading } = useSelector((state) => state.ui);
  const Button = ({ code }) => {
    const [isCopied, setIsCopied] = useState(false);

    // Copy text to Clipboard Function.
    // function copyTextToClipboard(text) {
    //   setIsCopied(true);
    //   setTimeout(() => {
    //     setIsCopied(false);
    //   }, 2000);
    //   return navigator.clipboard.writeText(text);
    // }

    const copyTextToClipboard = (text) => {
      const el = document.createElement("textarea");
      el.value = text;
      el.setAttribute("readonly", "");
      document.body.appendChild(el);
      const selected =
        document.getSelection().rangeCount > 0
          ? document.getSelection().getRangeAt(0)
          : false;
      el.select();
      document.execCommand("copy");
      document.body.removeChild(el);
      if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
      }
      setIsCopied(true);
      setTimeout(() => {
        setIsCopied(false);
      }, 2000);
    };
    return (
      <button
        className="btn btn-outline-primary btn-sm"
        onClick={(event) => {
          event.stopPropagation();
          const select = `http://www.mirapayments.com/pay/${code}`;
          copyTextToClipboard(select);
        }}
      >
        <span className={isCopied && styles.copy_button}>
          {isCopied ? "Copied!" : "Copy"}
        </span>
      </button>
    );
  };

  return (
    <>
      <tbody
        className="table table--bordered"
        {...getTableBodyProps()}
        id="assertion-table"
      >
        {paymentLinkListLoading ? (
          <tr>
            <td style={{ height: "40vh" }}>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
            <td>
              <Skeleton count={8} width="50%" style={{ marginTop: "20px" }} />
            </td>
          </tr>
        ) : page.length > 0 ? (
          page.map((row, index) => {
            prepareRow(row);
            // page[index]?.original.code = `<a style={{color:"blue",width:"100%"}} target="_blank" href={https://mirapayments.com/pay/${page[index]?.original.code}}>View</a>`
            return (
              <tr
                key={index}
                className={styles.row_style}
                data-testid="table-row"
                onClick={() =>
                  history.push(`/pages/payments/${page[index]?.original.id}`)
                }
                {...row.getRowProps()}
              >
                {row.cells.map((cell) => (
                  <>
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  </>
                ))}
                <td className={styles.tdstyles}>
                  <a
                    onClick={(event) => event.stopPropagation()}
                    style={{ color: "blue", width: "100%" }}
                    target="_blank"
                    href={`http://www.mirapayments.com/pay/${page[index]?.original.code}`}
                    rel="noreferrer"
                  >
                    View
                  </a>
                </td>
                <td className="mb-4">
                  <Button code={page[index]?.original.code} />
                </td>
              </tr>
            );
          })
        ) : (
          <>
            <tr>
              <td
                colSpan={6}
                align="center"
                style={{
                  height: "40vh",
                  verticalAlign: "middle",
                  left: "20px",
                }}
              >
                <img
                  src={NoCustomers}
                  width="20%"
                  height="150px"
                  alt="No customer"
                />
                <p>You have no payment links yet.</p>
                <p>Go ahead and create your payment links and start selling.</p>
              </td>
            </tr>
          </>
        )}
      </tbody>
    </>
  );
};

ReactTableDefaultBody.propTypes = {
  page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getTableBodyProps: PropTypes.func.isRequired,
  prepareRow: PropTypes.func.isRequired,
  code: PropTypes.string,
};

const ReactTableBody = ({
  page,
  getTableBodyProps,
  prepareRow,
  withDragAndDrop,
  updateDraggableData,
  theme,
}) => (
  <Fragment>
    {withDragAndDrop ? (
      <ReactTableDnDBody
        page={page}
        getTableBodyProps={getTableBodyProps}
        prepareRow={prepareRow}
        updateDraggableData={updateDraggableData}
        theme={theme}
      />
    ) : (
      <ReactTableDefaultBody
        page={page}
        getTableBodyProps={getTableBodyProps}
        prepareRow={prepareRow}
      />
    )}
  </Fragment>
);

ReactTableBody.propTypes = {
  page: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getTableBodyProps: PropTypes.func.isRequired,
  prepareRow: PropTypes.func.isRequired,
  updateDraggableData: PropTypes.func.isRequired,
  withDragAndDrop: PropTypes.bool.isRequired,
  theme: ThemeProps.isRequired,
};

export default connect((state) => ({
  theme: state.theme,
}))(ReactTableBody);
