import React from "react";
import { useTranslation } from "react-i18next";
import { Col, Container, Row } from "reactstrap";
import EditableReactTable from "./components/EditableReactTable";
import CreateTableData from "../CreateData";

const EditableTable = ({ reactRowData }) => {
  const { t } = useTranslation("common");
  const reactTableData = CreateTableData();

  return (
    <Container>
      <Row>
        <EditableReactTable
          reactRowData={reactRowData}
          reactTableData={reactTableData}
        />
      </Row>
    </Container>
  );
};

export default EditableTable;
