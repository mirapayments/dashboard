import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal } from "reactstrap";
import classNames from "classnames";
import styles from "../../ListAccount.module.css";
// import Form from "../ModalUpdatePaymentLink/Form/Form";
import SinglePaymentForm from "./Form/components/SinglePaymentForm";
import SubscriptionPaymentForm from "./Form/components/SubscriptionPaymentForm";
import { useSelector } from "react-redux";

const ModalComponent = ({ paymentDetails, color, colored, header }) => {
  const [modal, setModal] = useState(false);
  const [paymentLinkType] = useState(paymentDetails?.payment_type);
  const { className } = useSelector((state) => state.theme);

  const toggle = () => {
    setModal((prevState) => !prevState);
  };

  // let Icon = <span className="lnr lnr-pushpin modal__title-icon" />;

  const modalClass = classNames({
    "modal-dialog--colored": colored,
    "modal-dialog--header": header,
  });

  return (
    <div>
      <div
        styles={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        {/*  Button triggers the modal*/}
        <button
          color={color}
          onClick={toggle}
          type="submit"
          data-testid="edit-payment-link"
          className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
        >
          Edit Payment Link
        </button>
      </div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={`modal-dialog--${color} ${modalClass}`}
        modalClassName={className}
        wrapClassName={className}
        backdropClassName={className}
        contentClassName={className}
      >
        <div className="modal__body">
          {paymentLinkType === "Single" ? (
            <SinglePaymentForm
              paymentDetails={paymentDetails}
              setModal={setModal}
            />
          ) : null}
          {paymentLinkType === "Subscription" ? (
            <SubscriptionPaymentForm
              paymentDetails={paymentDetails}
              setModal={setModal}
            />
          ) : null}
        </div>
      </Modal>
    </div>
  );
};

ModalComponent.propTypes = {
  paymentDetails: PropTypes.string,
  colored: PropTypes.bool,
  header: PropTypes.bool,
  color: PropTypes.string,
};

ModalComponent.defaultProps = {
  title: "",
  message: "",
  colored: false,
  header: false,
};

export default ModalComponent;
