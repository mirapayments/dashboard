import React from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Col } from "reactstrap";
import Modal from "../ModalUpdatePaymentLink/Modal";
import styles from "./CardContainer.module.css";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getPaymentLinkDetailsWithID } from "../../../../../utils/apiHandlers/getPaymentLinkDetailsWithID";
import { togglePaymentLink } from "../../../../../utils/apiHandlers/togglePaymentLink";
import Switch from "react-switch";
import { deletePaymentLink } from "../../../../../utils/apiHandlers/deletePaymentLink";
import { useDispatch } from "react-redux";
import { setPaymentLinkUpdated } from "../../../../../redux/features/user/uiSlice";
import Swal from "sweetalert2";
import { useHistory } from "react-router";

const ExampleCard = ({ id, setTransactionList }) => {
  const { paymentLinkUpdated } = useSelector((state) => state.ui);
  const dispatch = useDispatch();
  const history = useHistory();
  const { signedInAccountNumber } = useSelector((state) => state.user);

  const [paymentDetails, setPaymentDetails] = useState({});

  async function getPaymentLinkDetailsWithIDHandler() {
    try {
      const res = await getPaymentLinkDetailsWithID(
        id,
        signedInAccountNumber
      ).catch((err) => console.log(err));
      setPaymentDetails(res.data.data);
      setTransactionList(res.data.data.transactions);
    } catch (error) {
      console.log(error);
    }
  }

  async function togglePaymentLinkHandler() {
    try {
      const res = await togglePaymentLink(id, signedInAccountNumber).catch(
        (err) => console.log(err)
      );
      console.log(res.data.data);
      // eslint-disable-next-line no-unused-vars
      setPaymentDetails((prev) => {
        return { ...res.data.data };
      });
    } catch (error) {
      console.log(error);
    }
  }
  useEffect(() => {
    getPaymentLinkDetailsWithIDHandler();
  }, [id, signedInAccountNumber, paymentLinkUpdated]);

  async function deletePaymentLinkHandler(id) {
    try {
      const response = await deletePaymentLink(id, signedInAccountNumber).catch(
        (err) => {
          console.log(err);
        }
      );
      if (response.status === 204) {
        Swal.fire({
          title: "Success",
          text: "Payment link was deleted, successfully!",
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        dispatch(setPaymentLinkUpdated(!paymentLinkUpdated));
        history.push("/pages/payments");
      }
    } catch (error) {
      console.log(error);
    }
  }

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: true,
  });

  function confirmKeyChange(e, id) {
    e.preventDefault();
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure you want to delete payment link?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ok",
        cancelButtonText: "Cancel",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          deletePaymentLinkHandler(id);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Action canceled",
            "error"
          );
        }
      });
  }

  function isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  return (
    !isEmpty(paymentDetails) && (
      <Col md={12}>
        <Card>
          <CardBody className={styles.card_body}>
            <div className="mb-4">
              <h4>{paymentDetails?.name}</h4>
            </div>
            <div className={styles.details_wrapper}>
              <div>
                <p>Payment Link:</p>
                <p>
                  <a
                    href={`http://www.mirapayments.com/pay/${paymentDetails?.code}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    http://www.mirapayments.com/pay/{paymentDetails?.code}
                  </a>
                </p>
              </div>
              <div>
                <p>Active:</p>
                <label>
                  <Switch
                    onChange={() => togglePaymentLinkHandler()}
                    checked={paymentDetails?.is_active}
                  />
                </label>
                {/* <p>{paymentDetails?.is_active && <div style={{width: "2px",height:"2px",borderRadius: "100%", backgroundColor: paymentDetails?.is_active ? "green":"red"}}></div>}</p> */}
              </div>
              <div>
                <p>Payment Type</p>
                <p>{paymentDetails?.payment_type}</p>
              </div>
              <div>
                <p>Amount</p>
                <p>{paymentDetails?.amount}</p>
              </div>
              <div>
                <p>Currency</p>
                <p>{paymentDetails?.amount_currency}</p>
              </div>
              {paymentDetails?.payment_type === "Subscription" ? (
                <div>
                  <p>Interval</p>
                  <p>{paymentDetails?.interval}</p>
                </div>
              ) : null}
              {paymentDetails?.payment_type === "Subscription" &&
              paymentDetails?.number_of_charges !== null ? (
                <div>
                  <p>Number of Charges</p>
                  <p>{paymentDetails?.number_of_charges}</p>
                </div>
              ) : null}
              <div>
                <p>Description</p>
                <p>{paymentDetails?.description}</p>
              </div>
              <div>
                <p>Extra Information</p>
                <p>{paymentDetails?.extra_info?.join(", ")}</p>
              </div>
            </div>
            <div className={styles.buttons}>
              <button
                data-testid="delete-btn"
                onClick={(e) => confirmKeyChange(e, id)}
                className={`${styles.button_newaccount} btn btn-danger account__btn account__btn--small`}
              >
                Delete
              </button>

              <div className={styles.modal_button}>
                <Modal paymentDetails={paymentDetails} />
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
    )
  );
};

ExampleCard.propTypes = {
  id: PropTypes.number,
  setTransactionList: PropTypes.func,
};

export default ExampleCard;
