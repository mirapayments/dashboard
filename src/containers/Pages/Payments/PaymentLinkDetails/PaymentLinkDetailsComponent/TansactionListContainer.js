import React from "react";
import { Card, CardBody, Col } from "reactstrap";
import Modal from "../../Payments/ModalCreatePaymentLink/Modal";
import styles from "./CardContainer.module.css";
import PaymentList from "../PaymentList/PaymentList";

const TransactionListContainer = () => (
  <Col md={12}>
    <Card>
      <CardBody>
        <PaymentList />
      </CardBody>
    </Card>
  </Col>
);

export default TransactionListContainer;
