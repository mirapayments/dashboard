import React, { useState } from "react";
import { useParams } from "react-router";
import { Col, Container, Row } from "reactstrap";
import PaymentDetails from "./PaymentLinkDetailsComponent/ExampleCard";
import PaymentTransactions from "./PaymentLinkTransactionsComponent/ExampleCard";

const PaymentLinkDetails = () => {
  const { id } = useParams();
  const [transactionList, setTransactionList] = useState([]);
  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          <h3 className="page-title ml-3">Payment Link Details</h3>
        </Col>
      </Row>
      <Row>
        <Col sm={6} lg={6} md={12}>
          <PaymentDetails id={id} setTransactionList={setTransactionList} />
        </Col>
        <Col sm={6} lg={6} md={12}>
          <PaymentTransactions id={id} transactionList={transactionList} />
        </Col>
      </Row>
    </Container>
  );
};

export default PaymentLinkDetails;
