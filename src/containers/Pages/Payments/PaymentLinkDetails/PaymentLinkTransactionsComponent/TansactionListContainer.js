import React from "react";
import { Card, CardBody, Col } from "reactstrap";
import PaymentList from "../PaymentList/PaymentList";

const TransactionListContainer = () => (
  <Col md={12}>
    <Card>
      <CardBody>
        <PaymentList />
      </CardBody>
    </Card>
  </Col>
);

export default TransactionListContainer;
