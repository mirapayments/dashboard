import { Card, CardBody, Col } from "reactstrap";
import React from "react";
import styles from "./CardContainer.module.css";
import PropTypes from "prop-types";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";
import { Table } from "reactstrap";
import { useHistory } from "react-router-dom";
import { transactionBadge } from "../../../../../utils/transactionBadge/transactionBadge";
import { Badge } from "reactstrap";
import Skeleton from "react-loading-skeleton";

const header = [
  { id: 0, title: "Reference" },
  { id: 1, title: "Status" },
  { id: 2, title: "Amount" },
  { id: 3, title: "Date" },
];

const ExampleCard = ({ transactionList }) => {
  let history = useHistory();
  return (
    <Col md={12}>
      <Card>
        <CardBody className={styles.card_body}>
          <h4 className="ml-2 mb-4">Payment Link Transactions</h4>
          <Table
            responsive
            className="table--bordered dashboard__audience-table"
          >
            <thead>
              <tr>
                {header.map((item) => (
                  <th className={styles.thead_overide} key={item.id}>
                    <p>{item.title || <Skeleton />}</p>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {transactionList?.map((item) => (
                <>
                  <tr
                    onClick={() => {
                      history.push(`/pages/transaction/${item?.id}`);
                    }}
                    className={styles.trstyles}
                  >
                    <td className={styles.tdstyles}>
                      <p>{item.short_reference || <Skeleton />}</p>
                    </td>
                    <td className={styles.tdstyles}>
                      <p>
                        <Badge
                          style={{ fontSize: "smaller" }}
                          color={transactionBadge(item?.status)}
                        >
                          {item?.status || <Skeleton />}
                        </Badge>
                      </p>
                    </td>
                    <td className={styles.tdstyles}>
                      <p>
                        {`${item.amount_currency} ${item.amount}` || (
                          <Skeleton />
                        )}
                      </p>
                    </td>
                    <td className={styles.tdstyles}>
                      <p>{formatDate(item.created_at) || <Skeleton />}</p>
                    </td>
                  </tr>
                </>
              ))}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </Col>
  );
};

ExampleCard.propTypes = {
  transactionList: PropTypes.array,
};

export default ExampleCard;
