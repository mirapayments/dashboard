import React from "react";
import { Card, CardBody, Col } from "reactstrap";
import Modal from "../../Payments/ModalCreatePaymentLink/Modal";
import styles from "./CardContainer.module.css";

const ExampleCard = () => (
  <Col md={12}>
    <Card>
      <CardBody className={styles.card_body}>
        <div className={styles.modal_button}>
          <Modal />
        </div>
      </CardBody>
    </Card>
  </Col>
);

export default ExampleCard;
