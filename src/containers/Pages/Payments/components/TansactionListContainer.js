import React from "react";
import { Col } from "reactstrap";
import PaymentList from "../PaymentList/PaymentList";

const TransactionListContainer = () => (
  <Col md={12}>
    <PaymentList />
  </Col>
);

export default TransactionListContainer;
