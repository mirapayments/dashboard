import { useMemo } from "react";

const CreateTableData = () => {
  const columns = useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Type",
        accessor: "payment_type",
        disableGlobalFilter: true,
      },

      {
        Header: "Amount",
        accessor: "amount",
      },
      {
        Header: "Currency",
        accessor: "amount_currency",
        disableGlobalFilter: true,
      },
      {
        Header: "Created",
        accessor: "created_at",
        disableGlobalFilter: true,
      },
      // {
      //   Header: 'Link',
      //   accessor: 'code',
      //   disableGlobalFilter: true,
      // }
    ],
    []
  );

  const reactTableData = { tableHeaderData: columns };
  return reactTableData;
};

export default CreateTableData;
