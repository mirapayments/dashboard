import React from "react";
import { Col, Container, Row } from "reactstrap";
import TransactionListContainer from "./components/TansactionListContainer";

const Transaction = () => {
  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          <h3 className="page-title ml-3">Payment Links</h3>
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          <TransactionListContainer />
        </Col>
      </Row>
    </Container>
  );
};

export default Transaction;
