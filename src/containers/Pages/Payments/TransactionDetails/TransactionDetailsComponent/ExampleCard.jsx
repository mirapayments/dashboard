import React from "react";
import { Card, CardBody, Col } from "reactstrap";
import styles from "./CardContainer.module.css";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";
import { Badge } from "reactstrap";
import { transactionBadge } from "../../../../../utils/transactionBadge/transactionBadge";

const ExampleCard = ({ transactions }) => {
  return (
    <Col md={12}>
      <Card>
        <CardBody className={styles.card_body}>
          <div className="mb-4">
            <h4>{transactions?.name}</h4>
          </div>
          <div className={styles.details_wrapper}>
            <div>
              <p>Amount</p>
              <p>
                {transactions?.amount_currency} {transactionDetails?.amount}
              </p>
            </div>
            <div>
              <p>Status</p>
              <p>
                <Badge color={transactionBadge(transactions?.status)}>
                  {transactions?.status}
                </Badge>
              </p>
            </div>
            <div>
              <p>Reference</p>
              <p>{transactions?.reference}</p>
              {console.log("TRANSACTION DETAILS in cusTOEMR", transactions)}
            </div>
            <div>
              <p>Transaction Fees</p>
              <p>{transactions?.amount}</p>
            </div>
            <div>
              <p>Amount due</p>
              <p>{transactions?.amount_due}</p>
            </div>
            <div>
              <p>Type</p>
              <p>{transactions?.transaction_type}</p>
            </div>
            <div>
              <p>Date</p>
              <p>{formatDate(transactions?.created_at)}</p>
            </div>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

export default ExampleCard;
