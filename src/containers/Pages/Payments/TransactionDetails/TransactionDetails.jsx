import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { getTransactionDetails } from "../../../../utils/apiHandlers/getTransactionDetails";
import { Col, Container, Row } from "reactstrap";
import TransactionDetails from "./TransactionDetailsComponent/ExampleCard";
import TransactionCustomer from "./TransactionCustomerComponent/ExampleCard";

const TransactionContainer = () => {
  const { id } = useParams();

  const [transactionDetails, setTransactionDetails] = useState({});

  async function getTransactionDetailsHandler() {
    try {
      const res = await getTransactionDetails(id).catch((err) =>
        console.log(err)
      );

      await setTransactionDetails(res.data.data);
    } catch (error) {}
  }

  useEffect(() => {
    getTransactionDetailsHandler();
  }, [id]);

  return (
    <Container className="dashboard">
      <Row>
        <Col md={12}>
          <h3 className="page-title ml-3">Transaction Details</h3>
        </Col>
      </Row>
      <Row>
        <Col sm={6} lg={6} md={12}>
          <TransactionDetails transactions={transactionDetails} />
        </Col>
        <Col sm={6} lg={6} md={12}>
          <TransactionCustomer transactions={transactionDetails} />
        </Col>
      </Row>
    </Container>
  );
};

export default TransactionContainer;
