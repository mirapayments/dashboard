import React from "react";
import { Card, CardBody, Col } from "reactstrap";
import styles from "./CardContainer.module.css";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getTransactionDetails } from "../../../../../utils/apiHandlers/getTransactionDetails";
import { formatDate } from "../../../../../utils/dateFormater/dateFormater";
import Switch from "react-switch";
import { deletePaymentLink } from "../../../../../utils/apiHandlers/deletePaymentLink";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
import { useHistory } from "react-router";

const ExampleCard = ({ transactions }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { signedInAccountNumber } = useSelector((state) => state.user);

  async function deletePaymentLinkHandler(id) {
    try {
      const response = await deletePaymentLink(id, signedInAccountNumber).catch(
        (err) => {}
      );
      if (response.status === 204) {
        Swal.fire({
          title: "Success",
          text: "Payment link was deleted, successfully!",
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        history.push("/pages/payments");
      }
    } catch (error) {}
  }

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: true,
  });

  function confirmKeyChange(e, id) {
    e.preventDefault();
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure you want to delete payment link?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ok",
        cancelButtonText: "Cancel",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          deletePaymentLinkHandler(id);
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            "Cancelled",
            "Action canceled",
            "error"
          );
        }
      });
  }

  return (
    <Col md={12}>
      <Card>
        <CardBody className={styles.card_body}>
          <div className="mb-4">
            <h4 className="mb-4">customer</h4>
          </div>
          <div className={styles.details_wrapper}>
            <div>
              <p>Full Name</p>
              <p>John Does</p>
            </div>
            <div>
              <p>Email</p>
              <p>johndoe@gmail.com</p>
            </div>
            <div>
              <p>Phone</p>
              <p>+2347945499499</p>
              {console.log("TRANSACTION DETAILS", transactionDetails)}
            </div>
            <div>
              <p>Date</p>
              <p>{formatDate(transactions?.created_at)}</p>
            </div>
          </div>
        </CardBody>
      </Card>
    </Col>
  );
};

export default ExampleCard;
