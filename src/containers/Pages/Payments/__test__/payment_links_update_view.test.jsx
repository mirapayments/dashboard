import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import PaymentLinkDetails from "../PaymentLinkDetails/PaymentLinkDetails";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <PaymentLinkDetails /> renders", () => {
  const store = mockStore(data);
  it("check if <PaymentLinkDetails /> contains update modals, update form, buttons", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <PaymentLinkDetails />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      const editPaymentLink = screen.getByTestId("edit-payment-link");
      const deleteButton = screen.getByTestId("delete-btn");

      expect(editPaymentLink).toBeInTheDocument();
      expect(deleteButton).toBeInTheDocument();

      fireEvent.click(editPaymentLink);

      //check if create modal is up
      expect(screen.getByPlaceholderText("Name")).toBeInTheDocument();
      expect(screen.getByPlaceholderText("Currency")).toBeInTheDocument();
      expect(screen.getByPlaceholderText("Payment Type")).toBeInTheDocument();
      expect(screen.getByPlaceholderText("Fixed Amount?")).toBeInTheDocument();
      expect(screen.getByTestId("update-payment-link-btn")).toBeInTheDocument();
    });
  });
});
