import "regenerator-runtime/runtime";
import {
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import Transaction from "../index";
import { data } from "../../../../fixures/store";
import server from "../../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <Transaction /> renders", () => {
  const store = mockStore(data);
  it("check if <Transaction /> contains create modals, update form, list", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Transaction />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      const paymentLinkTable = screen.getByPlaceholderText(
        "Search by Name, Payment Type or Amount"
      );
      const createPaymentlink = screen.getByTestId("create-payment-link");

      const tableRow = screen.getAllByTestId("table-row");

      expect(paymentLinkTable).toBeInTheDocument();
      expect(createPaymentlink).toBeInTheDocument();
      expect(tableRow.length).toBe(3);

      fireEvent.click(createPaymentlink);
      fireEvent.click(screen.getByTestId("single-payment-btn"));

      //check if create modal is up
      expect(screen.getByPlaceholderText("Name")).toBeInTheDocument();
      expect(screen.getByPlaceholderText("Currency")).toBeInTheDocument();
      expect(screen.getByPlaceholderText("Fixed Amount?")).toBeInTheDocument();
      expect(screen.getByTestId("create-payment-link-btn")).toBeInTheDocument();
    });
  });
});
