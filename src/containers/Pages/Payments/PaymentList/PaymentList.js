import React, { useEffect, useState } from "react";
import { getPaymentLinks } from "../../../../utils/apiHandlers/getPaymentLinks";
import { useSelector } from "react-redux";
import { formatDate } from "../../../../utils/dateFormater/dateFormater";
import EditableTable from "../EditableTable/components/EditableReactTable";
import CreateTableData from "../CreateData";
import { setPaymentLinkListLoading } from "../../../../redux/features/user/uiSlice";
import { useDispatch } from "react-redux";

const PaymentLists = () => {
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const { paymentLinkUpdated } = useSelector((state) => state.ui);
  const [paymentLinks, setPaymentLinks] = useState([]);
  const reactTableData = CreateTableData();
  const dispatch = useDispatch();

  useEffect(() => {
    async function getPaymentLinksHandler() {
      try {
        const response = await getPaymentLinks(signedInAccountNumber).catch(
          (err) => {
            console.log(err);
          }
        );
        dispatch(setPaymentLinkListLoading(true));
        if (response.data.status) {
          const modified = response?.data?.data?.map((item) => {
            return {
              ...item,
              created_at: formatDate(item.created_at),
            };
          });

          setPaymentLinks(modified);
          setTimeout(() => dispatch(setPaymentLinkListLoading(false)), 800);
        }
      } catch (error) {
        console.log(error);
      }
    }

    getPaymentLinksHandler();
  }, [paymentLinkUpdated, signedInAccountNumber]);

  return (
    <>
      <EditableTable
        reactRowData={paymentLinks}
        reactTableData={reactTableData}
      />
    </>
  );
};

export default PaymentLists;
