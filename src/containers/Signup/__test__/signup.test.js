import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from 'react-router-dom';
import SignUp from "../index";
import { data } from "../../../fixures/store";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock('react-router-dom', () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));


describe('check if <SignUp/> renders', () => {
  const store = mockStore(data);
    it('check if <SignUp/> contains, signup button, input fields', () => {
      render(
      <Provider store={store}>
        <BrowserRouter>
          <SignUp />
        </BrowserRouter>
      </Provider>);

      const nameInput = screen.getByTestId("account_name");
      const emailInput = screen.getByTestId("email");
      const accountTypeInput = screen.getByTestId("account_type");
      const currencyInput = screen.getByTestId("currency");
      const hearAboutUsInput = screen.getByTestId("hear_about_us");
      const password = screen.getByTestId("password");
      const signUpButton = screen.getByTestId("signup-btn");
    
      expect(nameInput).toBeInTheDocument();
      expect(emailInput).toBeInTheDocument();
      expect(accountTypeInput).toBeInTheDocument();
      expect(currencyInput).toBeInTheDocument();
      expect(hearAboutUsInput).toBeInTheDocument();
      expect(password).toBeInTheDocument();
      expect(signUpButton).toBeInTheDocument();
    })
});