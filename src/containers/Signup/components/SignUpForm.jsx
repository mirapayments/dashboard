import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import styles from "./signUpForm.module.css";
import EyeIcon from "mdi-react/EyeIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import AccountOutlineIcon from "mdi-react/AccountOutlineIcon";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import Public from "mdi-react/PublicIcon";
import AttachMoney from "mdi-react/AttachMoneyIcon";
import { useSelector, useDispatch } from "react-redux";
import { setUserName } from "../../../redux/features/user/userSlice";
import { signUpUser } from "../../../utils/apiHandlers/signUp";
import Spinner from "react-spinner-material";
import { useHistory } from "react-router-dom";
import { setUserEmail } from "../../../redux/features/user/userSlice";
import Swal from "sweetalert2";
import { setUserAccounts } from "../../../redux/features/user/userSlice";
import { logout } from "../../../redux/features/user/userSlice";
import { removeFromLocal } from "../../../utils/storageService/storageService";
import useOnlineStatus from "../../../utils/hooks/useOnlineStatus";
import QuestionAnswerIcon from "mdi-react/QuestionAnswerIcon";
import { removeEmpty } from "../../../utils/removeEmptyObjectKeys/removeEmptyObjectKeys";

const SignUpForm = () => {
  const dispatch = useDispatch();
  const { className } = useSelector((state) => state.theme);
  let history = useHistory();
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const online = useOnlineStatus();

  const showPassword = () => {
    setIsPasswordShown(!isPasswordShown);
  };

  //ensure existing user session is cleared
  useEffect(() => {
    dispatch(logout());
    removeFromLocal("token");
  }, [dispatch]);

  async function signUp(data, setSubmitting) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your network connection",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    try {
      const details = await signUpUser(JSON.stringify(data)).catch((err) => {
        if (err.status === 400 && err.data.data.email[0].includes("already")) {
          Swal.fire({
            title: "",
            text: err.data.data.email[0],
            icon: "error",
            showConfirmButton: false,
            timer: 3000,
          });
          setSubmitting(false);
          return;
        }
        if (err.status >= 500) {
          Swal.fire({
            title: "",
            text: err.data.detail,
            icon: "error",
            showConfirmButton: false,
            timer: 3000,
          });
          setSubmitting(false);
          return;
        }
      });
      const username = data.account_name || "user";
      dispatch(setUserName(username));
      dispatch(setUserEmail(data.email));

      if (details.data.status) {
        // set user current accounts
        dispatch(setUserAccounts(details.data.data.accounts));
        history.push({
          pathname: "/verifyemail",
          state: {
            previousUrl: "signup",
          },
        });
      }
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <>
      <Formik
        initialValues={{
          account_name: "",
          password: "",
          email: "",
          account_type: "",
          currency: "",
          hear_about_us: "",
        }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          const payload = removeEmpty(values);
          signUp(payload, setSubmitting);
        }}
        validationSchema={Yup.object().shape({
          account_name: Yup.string()
            .required("No account name provided.")
            .matches(
              /^[aA-zZ\s]+$/,
              "Only alphabets are allowed as account name "
            )
            .min(4, "Account name should be a minimum of 4 characters"),

          email: Yup.string().email().required("No email provided"),
          password: Yup.string()
            .required("No password provided.")
            .min(8, "Password is too short - should be 8 chars minimum.")
            .matches(/(?=.*[0-9])/, "Password must contain a number.")
            .matches(
              /^.*[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?].*$/,
              "Password must contain at least one special character"
            ),
          account_type: Yup.string().required("Please select an account type"),
          currency: Yup.string().required("Please select a currency"),
          hear_about_us: Yup.string().notRequired(),
        })}
      >
        {({
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
          }) =>
          (
            <form className="form" onSubmit={handleSubmit}>
              <div className="form__form-group">
                <span className="form__form-group-label">Account Name</span>
                <div
                  className={`form__form-group-field ${
                    errors.account_name &&
                    touched.account_name &&
                    "form__form-validation"
                  }`}
                >
                  <div className="form__form-group-icon">
                    <AccountOutlineIcon />
                  </div>
                  <input
                    name="account_name"
                    data-testid="account_name"
                    component="input"
                    type="text"
                    placeholder="Trading name or organisation name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.account_name}
                  />
                </div>
                <p className="error_validation_text">
                  {errors.account_name &&
                    touched.account_name &&
                    errors.account_name}
                </p>
              </div>
              <div className="form__form-group">
                <span className="form__form-group-label">Email</span>
                <div
                  className={`form__form-group-field ${
                    errors.email && "form__form-validation"
                  }`}
                >
                  <div className="form__form-group-icon">
                    <AlternateEmailIcon />
                  </div>
                  <input
                    name="email"
                    data-testid="email"
                    component="input"
                    type="email"
                    placeholder="Email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                  />
                </div>
                <p className="error_validation_text">
                  {errors.email && touched.email && errors.email}
                </p>
              </div>

              <div className="form__form-group">
                <span className="form__form-group-label">Account Type</span>
                <div
                  className={`${styles.select_form_custom_container} ${
                    errors.account_type && "form__form-validation"
                  }`}
                >
                  <div
                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                  >
                    <Public />
                  </div>
                  <select
                    className={
                      className === "theme-light"
                        ? styles.select_form_custom_light
                        : styles.select_form_custom_dark
                    }
                    name="account_type"
                    data-testid="account_type"
                    component="select"
                    type="select"
                    placeholder="Account Type"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.account_type}
                  >
                    <option label={"Select an account type"} />
                    <option value={"Individual"} label={"Individual"} />
                    <option value={"Company"} label={"Company"} />
                    <option value={"Religious"} label={"Religious"} />
                    <option value={"Government"} label={"Government"} />
                    <option value={"NGO"} label={"NGO"} />
                  </select>
                </div>
                <p className="error_validation_text">
                  {errors.account_type &&
                    touched.account_type &&
                    errors.account_type}
                </p>
              </div>

              <div className="form__form-group">
                <span className="form__form-group-label">Currency</span>
                <div
                  className={`${styles.select_form_custom_container} ${
                    errors.balance_currency && "form__form-validation"
                  }`}
                >
                  <div
                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                  >
                    <AttachMoney />
                  </div>
                  <select
                    className={
                      className === "theme-light"
                        ? styles.select_form_custom_light
                        : styles.select_form_custom_dark
                    }
                    name="currency"
                    data-testid="currency"
                    component="select"
                    type="select"
                    placeholder="Currency"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.currency}
                  >
                    <option label={"Select a currency"} />
                    <option value={"NGN"} label={"NGN"} />
                    <option value={"USD"} label={"USD"} />
                    <option value={"EUR"} label={"EUR"} />
                  </select>
                </div>
                <p className="error_validation_text">
                  {errors.currency && touched.currency && errors.currency}
                </p>
              </div>
              <div className="form__form-group">
                <span className="form__form-group-label">
                  How did you hear about us?
                </span>
                <div
                  className={`${styles.select_form_custom_container} ${
                    errors.account_type && "form__form-validation"
                  }`}
                >
                  <div
                    className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                  >
                    <QuestionAnswerIcon />
                  </div>
                  <select
                    className={
                      className === "theme-light"
                        ? styles.select_form_custom_light
                        : styles.select_form_custom_dark
                    }
                    name="hear_about_us"
                    data-testid="hear_about_us"
                    component="select"
                    type="select"
                    placeholder="How did you hear about us?"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.hear_about_us}
                  >
                    <option label={"Please select an option"} />
                    <option
                      value={"From a Google search"}
                      label={"From a Google search"}
                    />
                    <option value={"From a friend"} label={"From a friend"} />
                    <option value={"From the news"} label={"From the news"} />
                    <option
                      value={"Via a blog post"}
                      label={"Via a blog post"}
                    />
                    <option
                      value={"Through Social Media"}
                      label={"Through Social Media"}
                    />
                    <option
                      value={" From a webinar"}
                      label={"From a webinar"}
                    />
                  </select>
                </div>
                <p className="error_validation_text">
                  {errors.hear_about_us &&
                    touched.hear_about_us &&
                    errors.hear_about_us}
                </p>
              </div>

              <div className={`form__form-group`}>
                <span className="form__form-group-label">Password</span>
                <div
                  className={`form__form-group-field ${
                    errors.password && "form__form-validation"
                  }`}
                >
                  <div className="form__form-group-icon">
                    <KeyVariantIcon />
                  </div>
                  <input
                    name="password"
                    data-testid="password"
                    component="input"
                    type={isPasswordShown ? "text" : "password"}
                    placeholder="Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                  />
                  <button
                    className={`form__form-group-button${
                      isPasswordShown ? " active" : ""
                    }`}
                    onClick={() => showPassword()}
                    type="button"
                  >
                    <EyeIcon />
                  </button>
                </div>
                <p className="error_validation_text">
                  {errors.password && touched.password && errors.password}
                </p>
              </div>
              <button
                type="submit"
                data-testid="signup-btn"
                disabled={isSubmitting}
                className="btn btn-primary account__btn account__btn--small"
              >
                {isSubmitting ? (
                  <div className={styles.button_elem}>
                    <Spinner
                      radius={20}
                      color={"#333"}
                      stroke={2}
                      visible={true}
                    />
                  </div>
                ) : (
                  "Sign Up"
                )}
              </button>
              <div className={styles.bottom_text}>
                <p>Already have an account?</p>
                <span>
                  <Link to="/signin"> sign in</Link>
                </span>
              </div>
            </form>
          )
        }
      </Formik>
    </>
  );
};

export default SignUpForm;
