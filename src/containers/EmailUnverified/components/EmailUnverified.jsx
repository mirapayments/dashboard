import React, { useState } from "react";
import styles from "./EmailUnverified.module.css";
import emailgif from "../../../shared/img/emailverification.gif";
import PropTypes from 'prop-types';
import emailsvg from "../../../shared/img/emailverification.svg";
import { sendEmailVerification } from "../../../utils/apiHandlers/sendEmailVerification";
import { useSelector } from "react-redux";
import Swal from "sweetalert2";
import { useLocation } from "react-router-dom";
import MirapayLogo from "../../../shared/img/mirapaylogo.png";
import useOnlineStatus from "../../../utils/hooks/useOnlineStatus";

const EmailUnverifiedPage = ({ previousUrl }) => {
  const [emailverificationRequestSent, setEmailVerificationRequestSent] =
    useState(false);
  const { email } = useSelector((state) => state.user);
  const location = useLocation();
  const online = useOnlineStatus();

  const pageReferer = location.state?.previousUrl || previousUrl;

  function stopTimerHandler() {
    setEmailVerificationRequestSent((prev) => !prev);
  }

  // Timer component
  const Timer = ({ stopTimer }) => {
    const [seconds, setSeconds] = useState(60);
    React.useEffect(() => {
      seconds > 0 && setTimeout(() => setSeconds(seconds - 1), 1000);
      if (seconds === 0) {
        stopTimer();
      }
    }, [seconds, stopTimer]);
    return seconds;
  };

  async function sendEmailVerificationHandler(userEmail) {
    const email = {
      email: userEmail,
    };
    const result = await sendEmailVerification(JSON.stringify(email));
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "success",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    try {
      if (result.data.status) {
        setEmailVerificationRequestSent(true);

        Swal.fire({
          title: "",
          text: result.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
      }
    } catch (error) {
      Swal.fire({
        title: "",
        text: "An error occurred",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }

  return (
    <div className={styles.container}>
      <img
        alt="email Icon"
        style={{
          height: "100%",
          width: "50%",
        }}
        src={MirapayLogo}
      />
      <h3>Verify Your Email Address</h3>
      {emailverificationRequestSent ? (
        <img
          className={styles.img_style}
          src={emailgif}
          alt="email verification logo"
        />
      ) : (
        <img alt="Email svg" className={styles.img_style} src={emailsvg} />
      )}
      <p>
        {pageReferer === "signup" && "Welcome to the Mirapay Platform"} We sent
        an email to <b>{email}.</b> Please verify it’s you.
      </p>

      <p>Check your spam folder if you don’t see it.</p>

      <p>
        {pageReferer === "signup"
          ? "Still can’t find the email?"
          : "Thanks for signing in, but you still need to verify your email address"}
      </p>
      <button
        disabled={emailverificationRequestSent}
        onClick={() => {
          // startCountdown()
          sendEmailVerificationHandler(email);
        }}
        className={`${styles.btn_style} btn btn-primary account__btn account__btn--small`}
      >
        {emailverificationRequestSent ? (
          <Timer stopTimer={stopTimerHandler} />
        ) : (
          "Resend Email"
        )}
      </button>
    </div>
  );
};

EmailUnverifiedPage.propTypes = {
  previousUrl: PropTypes.string
};

export default EmailUnverifiedPage;
