import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from 'react-router-dom';
import SignIn from "../index";
import { data } from "../../../fixures/store";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock('react-router-dom', () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/example/path"
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));


describe('check if <SignIn/> renders', () => {
  const store = mockStore(data);
    it('check if <SignIn/> contains, signin and create account button', () => {
      render(
      <Provider store={store}>
        <BrowserRouter>
          <SignIn />
        </BrowserRouter>
      </Provider>);

      const signInButton = screen.getByTestId("signin-btn");
      const signUpButton = screen.getByTestId("signup-btn");
      expect(signInButton).toBeInTheDocument();
      expect(signUpButton).toBeInTheDocument();
    })
});