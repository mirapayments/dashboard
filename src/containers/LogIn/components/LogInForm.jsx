import React, { useState, useMemo } from "react";
import { Link, useLocation, useHistory } from "react-router-dom";
import EyeIcon from "mdi-react/EyeIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import { Formik } from "formik";
import * as Yup from "yup";
import styles from "./loginForm.module.css";
import { useDispatch, useSelector } from "react-redux";
import { signInUser } from "../../../utils/apiHandlers/signIn";
import { getUserAccountDetails } from "../../../utils/apiHandlers/getUserAccountDetails";
import Spinner from "react-spinner-material";
import {
  setSignedInAccountNumber,
  setUserEmailVerification,
  setUserToken,
  setUserRole,
} from "../../../redux/features/user/userSlice";
import { setUserAccounts } from "../../../redux/features/user/userSlice";
import { setUserEmail } from "../../../redux/features/user/userSlice";
import { setSignedInState } from "../../../redux/features/user/userSlice";
import { saveToLocal } from "../../../utils/storageService/storageService";
import { removeFromLocal } from "../../../utils/storageService/storageService";
import Swal from "sweetalert2";
import { logout } from "../../../redux/features/user/userSlice";
import useOnlineStatus from "../../../utils/hooks/useOnlineStatus";
// import { setUserName } from "../../../redux/features/user/userSlice";

const LogInForm = () => {
  const online = useOnlineStatus();
  const dispatch = useDispatch();
  let history = useHistory();
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const { signedInAccountNumber } = useSelector((state) => state.user);
  const { complianceSetupComplete } = useSelector(state => state.ui);
  // const { userEmailVerified } = useSelector((state) => state.user);

  // Clear existing user session on visiting login page
  /*
  useEffect(() => {
    dispatch(logout());
    removeFromLocal("token");
  }, []);
  */


  function useSearchQuery() {
    const { search } = useLocation();
    return useMemo(() => new URLSearchParams(search), [search]);
  }

  let searchQuery = useSearchQuery();

  const showPassword = () => {
    setIsPasswordShown(!isPasswordShown);
  };

  async function getUserRole(account_number) {
    try {
      const response = await getUserAccountDetails(account_number).catch(
        (err) => {
          console.log(err);
        }
      );
      const role = response?.data?.data?.signed_in_user_role;
      await dispatch(setUserRole(role));
    } catch (error) {
      console.log(error);
    }
  }
  async function signUser(data, setSubmitting) {
    //clear existing user session on sign in
    dispatch(logout());
    removeFromLocal("token");
    console.log(searchQuery?.get("previous_route"));
    if (!online) {
      Swal.fire({
        title: "",
        text: "you are offline please check your internet connection",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      setSubmitting(false);
      return;
    }

    try {
      const res = await signInUser(JSON.stringify(data)).catch((err) => {
        if (err.status === 403 && err.data.detail.includes("verify")) {
          dispatch(setUserEmail(data.email));
          dispatch(setUserEmailVerification(false));
          history.push({
            pathname: "/verifyemail",
            state: {
              previousUrl: "signin",
            },
          });
          return;
        }
        if (
          err.status == 401 &&
          err.data.detail.includes("Invalid credentials")
        ) {
          Swal.fire({
            title: "",
            text: err.data.detail,
            icon: "error",
            showConfirmButton: false,
            timer: 3000,
          });
          return setSubmitting(false);
        }
        if (err.status >= 500) {
          Swal.fire({
            title: "",
            text: "A server error occurred",
            icon: "error",
            showConfirmButton: false,
            timer: 3000,
          });
        }
      });

      //if user is logging in due to expired token
      if(signedInAccountNumber !== "" 
        && searchQuery?.get("previous_route") !== null 
        && searchQuery?.get("previous_route") !== '/'){
          console.log(signedInAccountNumber);
          const accounts = res.data?.data?.accounts;
          const token = res.data.data.token;
          dispatch(setUserEmailVerification(true));
          dispatch(setUserAccounts(accounts));
          dispatch(setSignedInAccountNumber(signedInAccountNumber));
          await saveToLocal("token", token);
          await dispatch(setUserToken(token));
          await dispatch(setSignedInState(true));
          await getUserRole(signedInAccountNumber);
          history.push(`${searchQuery?.get("previous_route")}`);
          return;
      }

      // if user has multiple accounts, take user to select account screen
      if (res.data.data.accounts.length > 1) {
        dispatch(setUserEmailVerification(true));
        dispatch(setUserAccounts(res.data.data.accounts));
        const token = res.data.data.token;
        await saveToLocal("token", token);
        await dispatch(setUserToken(token));
        await dispatch(setSignedInState(true));
        history.push("/selectaccount");
        return;
      }

      //if user logs in successfully take user to dashboard
      if (res.data.status && res.data.detail.includes("Login successful")) {
        const accounts = res.data?.data?.accounts;
        const accountNumber = res.data?.data?.accounts[0].account_number;
        dispatch(setUserEmailVerification(true));
        dispatch(setUserAccounts(accounts));

        // current user account number
        const userAccount = res.data?.data?.accounts[0]?.account_number;
        dispatch(setSignedInAccountNumber(userAccount));
        const token = res.data.data.token;
        await saveToLocal("token", token);
        await dispatch(setUserToken(token));
        await dispatch(setSignedInState(true));
        await getUserRole(accountNumber);
        if(!complianceSetupComplete){
          return history.push("/pages/compliance");
        }
        history.push("/pages/dashboard");
      }
    } catch (error) {
      setSubmitting(false);
    }
  }

  return (
    <>
      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          signUser(values, setSubmitting);
          dispatch(setUserEmail(values.email));
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string().required("Please provide your email").email(),
          password: Yup.string().required("Please input your password."),
        })}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
        }) => (
          <form className="form" onSubmit={handleSubmit}>
            <div className="form__form-group">
              <span className="form__form-group-label">Email</span>
              <div
                className={`form__form-group-field ${
                  errors.email && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <AlternateEmailIcon />
                </div>
                <input
                  name="email"
                  type="text"
                  placeholder="Email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.username}
                />
              </div>
              <p className="error_validation_text">
                {touched.email && errors.email && errors.email}
              </p>
            </div>
            <div className="form__form-group">
              <span className="form__form-group-label">Password</span>
              <div
                className={`form__form-group-field ${
                  errors.password && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <KeyVariantIcon />
                </div>
                <input
                  name="password"
                  type={isPasswordShown ? "text" : "password"}
                  placeholder="Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                <button
                  className={`form__form-group-button${
                    isPasswordShown ? " active" : ""
                  }`}
                  onClick={() => showPassword()}
                  type="button"
                >
                  <EyeIcon />
                </button>
              </div>
              <p className="error_validation_text">
                {errors.password && touched.password && errors.password}
              </p>
              <div className="account__forgot-password mt-4">
                <Link to="/resetpassword">Forgot a password?</Link>
              </div>
            </div>
            <button
              type="submit"
              data-testid="signin-btn"
              placeholder="signin"
              disabled={isSubmitting}
              className="btn btn-primary account__btn account__btn--small mt-4"
            >
              {isSubmitting ? (
                <div className={styles.button_elem}>
                  <Spinner
                    radius={20}
                    color={"#333"}
                    stroke={2}
                    visible={true}
                  />
                </div>
              ) : (
                "Sign In"
              )}
            </button>
            <Link
              data-testid="signup-btn"
              className="btn btn-outline-primary account__btn account__btn--small"
              to="/signup"
            >
              Create Account
            </Link>
          </form>
        )}
      </Formik>
    </>
  );
};

{
  /* LogInForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
}; */
}

export default LogInForm;
