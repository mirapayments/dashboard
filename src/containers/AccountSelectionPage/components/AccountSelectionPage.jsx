import React from "react";
import { useSelector } from "react-redux";
import AccountSelectionProfile from "./AccountSelectionProfile";

const AccountSelectionPage = () => {
  const { userAccounts } = useSelector((state) => state.user);
  return userAccounts.map((item) => (
    <AccountSelectionProfile
      key={item.account_number}
      name={item.name}
      accountnumber={item.account_number}
    />
  ));
};

export default AccountSelectionPage;
