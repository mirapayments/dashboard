import React from "react";
import Avatar from "./Avatar";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import styles from "./avatar.module.css";
import { setSignedInAccountNumber } from "../../../redux/features/user/userSlice";
import { useHistory } from "react-router-dom";
import { setSignedInState } from "../../../redux/features/user/userSlice";
import { setUserEmailVerification } from "../../../redux/features/user/userSlice";
import { setUserRole } from "../../../redux/features/user/userSlice";
import { getUserAccountDetails } from "../../../utils/apiHandlers/getUserAccountDetails";

const AccountSelectionProfile = ({ name, accountnumber }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { complianceSetupComplete } = useSelector(state => state.ui);

  async function signInUserWithSelectedAccount() {
    dispatch(setUserEmailVerification(true));
    await signInAccountNumberHandler(accountnumber);
    await dispatch(setSignedInAccountNumber(accountnumber));
    dispatch(setSignedInState(true));
    if(!complianceSetupComplete){
      return history.push("/pages/compliance");
    }
    history.push("/pages/dashboard");
  }

  async function signInAccountNumberHandler() {
    try {
      const response = await getUserAccountDetails(accountnumber).catch(
        (err) => {
          console.log(err);
        }
      );
      dispatch(setUserRole(response.data.data.signed_in_user_role));
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div onClick={signInUserWithSelectedAccount} className={styles.account}>
      <div className={styles.avatar}>
        <div className={styles.icon}>
          <Avatar email={name} />
        </div>
        <div>
          <p>{name}</p>
          <p>{accountnumber}</p>
        </div>
      </div>
      <div>
        <hr className={styles.hr_color} />
      </div>
    </div>
  );
};

AccountSelectionProfile.propTypes = {
  name: PropTypes.string,
  accountnumber: PropTypes.number,
};

export default AccountSelectionProfile;
