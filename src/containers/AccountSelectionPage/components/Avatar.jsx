import React from "react";
import PropTypes from 'prop-types';
const Avatar = ({ email }) => (
  <div>
    <img
      alt="Avatar Img"
      style={{
        height: 50,
        width: 50,
        borderRadius: "100%",
      }}
      src={`https://avatars.dicebear.com/api/initials/${email}.svg`}
    />
  </div>
);

Avatar.propTypes = {
  email: PropTypes.string
};

export default Avatar;
