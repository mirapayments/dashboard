import { render, screen } from "@testing-library/react";
import { BrowserRouter } from 'react-router-dom';
import ResetPassword from "../index";



describe('check if <ResetPassword/> renders', () => {
    it('check if <ResetPassword /> contains email input, back to sign in and send reset password email button', () => {
      render(
      
        <BrowserRouter>
          <ResetPassword />
        </BrowserRouter>
      );

      const emailInput = screen.getByTestId("email");
      const submitButton = screen.getByTestId("submit-btn");
      const backToSignInButton = screen.getByTestId("go-back-btn");

      expect(emailInput).toBeInTheDocument();
      expect(submitButton).toBeInTheDocument();
      expect(backToSignInButton).toBeInTheDocument();
    })
});