import React from "react";
import ResetPass from "./components/ResetPassword";
import styles from "./ResetPassword.module.css";

const ResetPassword = () => {
  return (
    <div className="account">
      <div className="account__wrapper">
        <div className={`${styles.container} account__card`}>
          <>
            <div className="account__head">
              <h4 className="account__subhead subhead">
                Would you like to reset your password?
              </h4>
            </div>
            <ResetPass />
          </>
        </div>
      </div>
    </div>
  );
};
export default ResetPassword;
