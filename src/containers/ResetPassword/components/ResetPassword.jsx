import React, { useState } from "react";
import styles from "./ResetPassword.module.css";
import AlternateEmailIcon from "mdi-react/AlternateEmailIcon";
import { resetPasswordRequest } from "../../../utils/apiHandlers/sendResetPasswordRequest";
import { Link } from "react-router-dom";
import Swal from "sweetalert2/dist/sweetalert2.js";
import useOnlineStatus from "../../../utils/hooks/useOnlineStatus";

const ResetPasswordForm = () => {
  const [email, setUserEmail] = useState("");
  const [resetRequestSent, setResetRequestSent] = useState(false);
  const online = useOnlineStatus();

  async function resetUserPassword(email) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your network connection",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    const userEmail = {
      email: email,
    };
    try {
      const result = await resetPasswordRequest(userEmail);
      if (result.data.status) {
        Swal.fire({
          title: "",
          text: result.data.detail,
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });
        setResetRequestSent(false);
      }
    } catch (error) {
      Swal.fire({
        title: "",
        text: "An Error Occured",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      setResetRequestSent(false);
    }
  }

  return (
    <div className={styles.container}>
      <form className="form" 
        onSubmit={(e) => {
          setResetRequestSent(true);
          e.preventDefault();
          return resetUserPassword(email);
        }}
      >
        <div className="form__form-group">
          <span className="form__form-group-label">Email</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <AlternateEmailIcon />
            </div>
            <input
              data-testid="email"
              onChange={(e) => setUserEmail(e.target.value)}
              name="email"
              type="email"
              required
              placeholder="please enter your account's email"
            />
          </div>
        </div>

        <button
          data-testid="submit-btn"
          type="submit"
          disabled={resetRequestSent}
          className={`${styles.btn_reset_color} btn btn-primary account__btn account__btn--small`}
        >
          {resetRequestSent ? "Sending" : "Send reset password email"}
        </button>
        <div className={styles.bottom_text}>
          <Link to="/signin" data-testid="go-back-btn" >Go back Back to signin</Link>
        </div>
      </form>
    </div>
  );
};

export default ResetPasswordForm;
