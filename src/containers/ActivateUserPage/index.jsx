import React from "react";
import PasswordResetPage from "./components/ActivateUserPage";
import styles from "./ActivateUserPage.module.css";

const ActivateUserPageContainer = () => {
  return (
    <div className="account">
      <div className="account__wrapper">
        <div className={`${styles.container} account__card`}>
          <div className="account__head">
            <h4 className="account__subhead subhead">
              Update your password to join Mirapay
            </h4>
          </div>
          <PasswordResetPage />
        </div>
      </div>
    </div>
  );
};
export default ActivateUserPageContainer;
