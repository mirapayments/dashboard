import React, { useState } from "react";
import styles from "./ActivateUserPage.module.css";
import { activateUser } from "../../../utils/apiHandlers/activateUser";
import { useDispatch } from "react-redux";
import Spinner from "react-spinner-material";
import { useParams, useHistory } from "react-router-dom";
import { setUserPasswordReset } from "../../../redux/features/user/userSlice";
import EyeIcon from "mdi-react/EyeIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import { Formik } from "formik";
import * as Yup from "yup";
import Swal from "sweetalert2";
import useOnlineStatus from "../../../utils/hooks/useOnlineStatus";

const ActivateUserPage = () => {
  const online = useOnlineStatus();
  const dispatch = useDispatch();
  const [isPasswordShown1, setIsPasswordShown1] = useState(false);
  const [isPasswordShown2, setIsPasswordShown2] = useState(false);
  let history = useHistory();
  // the slug from the route
  const { uid } = useParams();

  function showPassword1() {
    setIsPasswordShown1((prev) => !prev);
  }
  function showPassword2() {
    setIsPasswordShown2((prev) => !prev);
  }

  async function activateUserHandler(values, setSubmitting) {
    if (!online) {
      Swal.fire({
        title: "",
        text: "You are offline, please check your internet connection.",
        icon: "error",
        showConfirmButton: false,
        timer: 3000,
      });
      return;
    }
    try {
      const payload = {
        uid: uid,
        password: values.password,
      };

      const response = await activateUser(JSON.stringify(payload)).catch(
        (err) => {
          if (err.status === 400) {
            Swal.fire({
              title: "Error",
              text: "User has already been activated",
              icon: "error",
              showConfirmButton: false,
              timer: 3000,
            });
            setSubmitting(false);
          }
        }
      );
      if (response.data.status) {
        Swal.fire({
          title: "Password successfully set",
          text: "Please sign in with your email and password",
          icon: "success",
          showConfirmButton: false,
          timer: 3000,
        });

        dispatch(setUserPasswordReset(true));
        setSubmitting(false);
        history.push("/signin");
      }
    } catch (error) {
      console.log(error?.response?.data?.status);
      setSubmitting(false);
    }
  }

  return (
    <Formik
      initialValues={{ password: "", confirmPassword: "" }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true);
        activateUserHandler(values, setSubmitting);
      }}
      validationSchema={Yup.object().shape({
        password: Yup.string()
          .required("No password provided.")
          .min(8, "Password is too short - should be 8 chars minimum.")
          .matches(/(?=.*[0-9])/, "Password must contain a number.")
          .matches(
            /^.*[!@#$%^&*()_+\-=\\[\]{};':"\\|,.<>\\/?].*$/,
            "Password must contain at least one special character"
          ),
        confirmPassword: Yup.string().oneOf(
          [Yup.ref("password"), null],  
          "Passwords must match"
        ),
      })}
    >
      {({ values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit, }) =>
        (
          <form className="form" onSubmit={handleSubmit}>
            <div className="form__form-group">
              <span className="form__form-group-label">Enter Password</span>
              <div
                className={`form__form-group-field ${
                  errors.password && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <KeyVariantIcon />
                </div>
                <input
                  name="password"
                  type={isPasswordShown1 ? "text" : "password"}
                  placeholder="Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                <button
                  className={`form__form-group-button${
                    isPasswordShown1 ? " active" : ""
                  }`}
                  onClick={() => showPassword1()}
                  type="button"
                >
                  <EyeIcon />
                </button>
              </div>
              <p className="error_validation_text">
                {errors.password && touched.password && errors.password}
              </p>
            </div>
            <div className="form__form-group">
              <span className="form__form-group-label">Confirm Password</span>
              <div
                className={`form__form-group-field ${
                  errors.confirmPassword && "form__form-validation"
                }`}
              >
                <div className="form__form-group-icon">
                  <KeyVariantIcon />
                </div>
                <input
                  name="confirmPassword"
                  type={isPasswordShown2 ? "text" : "password"}
                  placeholder="Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.confirmPassword}
                />
                <button
                  className={`form__form-group-button${
                    isPasswordShown2 ? " active" : ""
                  }`}
                  onClick={() => showPassword2()}
                  type="button"
                >
                  <EyeIcon />
                </button>
              </div>
              <p className="error_validation_text">
                {errors.confirmPassword &&
                  touched.confirmPassword &&
                  errors.confirmPassword}
              </p>
            </div>

            <button
              type="submit"
              disabled={isSubmitting}
              className="btn btn-primary account__btn account__btn--small"
            >
              {isSubmitting ? (
                <div className={styles.button_elem}>
                  <Spinner
                    radius={20}
                    color={"#333"}
                    stroke={2}
                    visible={true}
                  />
                </div>
              ) : (
                "Submit"
              )}
            </button>
          </form>
        )}
    </Formik>
  );
};

export default ActivateUserPage;
