import React, { useEffect } from "react";
import { Route, Switch, useHistory } from "react-router-dom";
import Layout from "../Layout/index";
import MainWrapper from "./MainWrapper";
import LogIn from "../LogIn/index";
import Signup from "../Signup/index";
import Dashboard from "../Pages/Dashboard/index";
import Transaction from "../Pages/Transaction/index";
import Customers from "../Pages/Customers/index";
import Balance from "../Pages/Balance";
import Disputes from "../Pages/Disputes";
import SubAccounts from "../Pages/Subaccounts";
import SwitchToLiveMode from "../Pages/SwitchtoLiveMode";
import Settings from "../Pages/Settings";
import ResetPassword from "../ResetPassword";
import NotFound from "../DefaultPage/404/index";
import EmailUnverified from "../EmailUnverified";
import EmailVerifiedPage from "../EmailVerifiedPage";
import PasswordResetPage from "../PasswordResetPage";
import ActivateUserPage from "../ActivateUserPage";
import AccountSelectionPage from "../AccountSelectionPage";
import Payments from "../Pages/Payments";
import { useSelector } from "react-redux";
import { getFromLocal } from "../../utils/storageService/storageService";
import { useDispatch } from "react-redux";
import { setSignedInState } from "../../redux/features/user/userSlice";
import ProtectedRoute from "../../utils/authService/RouteGuard";
import { verifyToken } from "../../utils/apiHandlers/verifyToken";
import PaymentLinkDetails from "../Pages/Payments/PaymentLinkDetails/PaymentLinkDetails";
import CustomerDetails from "../Pages/Customers/CustomerDetails/CustomerDetails";
import TransactionDetails from "../Pages/Transaction/TransactionDetails/TransactionDetails";
import Compliance from "../Pages/Compliance";

const Pages = () => (
  <Switch>
    <Route path="/pages/dashboard" component={Dashboard}/>
    <Route path="/pages/transaction/:id" component={TransactionDetails} />
    <Route path="/pages/transaction" component={Transaction} />
    <Route path="/pages/customers/:id" component={CustomerDetails} />
    <Route path="/pages/customers" component={Customers} />
    <Route path="/pages/balance" component={Balance} />
    <Route path="/pages/disputes" component={Disputes} />
    <Route path="/pages/subaccounts" component={SubAccounts} />
    <Route path="/pages/switchtolivemode" component={SwitchToLiveMode} />
    <Route path="/pages/settings" component={Settings} />
    <Route path="/pages/settings/user/rolepermissions" component={Settings} />
    <Route path="/pages/payments/:id" component={PaymentLinkDetails} />
    <Route path="/pages/payments" component={Payments} />
    <Route path="/pages/compliance" component={Compliance} />
  </Switch>
);

// setting tab route

const wrappedRoutes = () => (
  <div>
    <Layout />
    <div className="container__wrap">
      <Route path="/pages" component={Pages} />
    </div>
  </div>
);

const Router = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isSignedIn } = useSelector((state) => state.user);

  //check if there is a user token in local storage and then change signed in status.
  //if user token is expired, set signedin state to false
  useEffect(() => {
    async function verifyUserToken() {
      if (getFromLocal("token")) {
        const userToken = getFromLocal("token");
        const payLoad = {
          token: userToken,
        };

        let currentRoute = window.location.pathname;
        const res = await verifyToken(JSON.stringify(payLoad)).catch((err) => {
          dispatch(setSignedInState(false));
          if(err.status === 400){
            let params = new URLSearchParams(document.location.search);
            let previousRoute = params.get("previous_route");
            if(previousRoute !== "/" && previousRoute !== null){
              return;
            }
            history.push(`/?previous_route=${currentRoute}`);
          }
        });
        if (res?.data?.status) {
          dispatch(setSignedInState(true));
        }
      } else {
        dispatch(setSignedInState(false));
      }
    }
    verifyUserToken();
  }, [history, dispatch]);

  // check if a user exists and a token exist in local storage to determine if the user is signed in

  return (
    <MainWrapper>
      <main>
        <Switch>
          <Route
            path="/verifyemail/:subroute/:token"
            component={EmailVerifiedPage}
          />
          <Route
            path="/resetpassword/:subroute/:token"
            component={PasswordResetPage}
          />
          <Route path="/activate/:uid" component={ActivateUserPage} />
          <Route exact path="/verifyemail" component={EmailUnverified} />
          <Route exact path="/404" component={NotFound} />
          <Route auth={isSignedIn} exact path="/" component={LogIn} />
          <Route auth={isSignedIn} exact path="/signin" component={LogIn} />
          <Route auth={isSignedIn} exact path="/signup" component={Signup} />
          <Route exact path="/resetpassword" component={ResetPassword} />
          <Route exact path="/selectaccount" component={AccountSelectionPage} />
          <ProtectedRoute
            auth={isSignedIn}
            path="/pages"
            component={wrappedRoutes}
          />
          <Route component={NotFound} />
        </Switch>
      </main>
    </MainWrapper>
  );
};

export default Router;
