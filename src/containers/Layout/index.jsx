import React, { useEffect } from "react";
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import { connect, useSelector } from "react-redux";
import classNames from "classnames";
import Topbar from "./topbar/Topbar";
import Sidebar from "./sidebar/Sidebar";
import { SidebarProps } from '../../shared/prop-types/ReducerProps';
import {
  changeThemeToDark,
  changeThemeToLight,
} from "../../redux/actions/themeActions";
import {
  changeMobileSidebarVisibility,
  changeSidebarVisibility,
} from "../../redux/actions/sidebarActions";
import { getAccountDetails } from "../../utils/apiHandlers/getAccountDetails";
import { setComplianceSetupComplete } from "../../redux/features/user/uiSlice";

const Layout = ({ dispatch, sidebar }) => {
  const { isSignedIn, signedInAccountNumber } = useSelector((state) => state.user);
  
  async function handleAccountDetails(){
    try {
      if(!isSignedIn) return;
      const response = await getAccountDetails(signedInAccountNumber).catch((err) => {
        console.log(err);
      });
      if (response.data.status) {
        dispatch(setComplianceSetupComplete(response.data.data.setup_complete));
      }
    } catch (error) {
      console.log(error);
    }  
  }

  useEffect(() => {
    handleAccountDetails();
  }, [signedInAccountNumber]);

  const layoutClass = classNames({
    layout: true,
    "layout--collapse": sidebar.collapse,
  });

  const sidebarVisibility = () => {
    dispatch(changeSidebarVisibility());
  };

  const mobileSidebarVisibility = () => {
    dispatch(changeMobileSidebarVisibility());
  };

  const changeToDark = () => {
    dispatch(changeThemeToDark());
  };

  const changeToLight = () => {
    dispatch(changeThemeToLight());
  };

  return (
    <div className={layoutClass}>
      <Topbar
        changeMobileSidebarVisibility={mobileSidebarVisibility}
        changeSidebarVisibility={sidebarVisibility}
        changeToDark={changeToDark}
        changeToLight={changeToLight}
      />
      <Sidebar
        sidebar={sidebar}
        changeMobileSidebarVisibility={mobileSidebarVisibility}
      />
    </div>
  );
};

Layout.propTypes = {
  dispatch: PropTypes.func.isRequired,
  sidebar: SidebarProps.isRequired,
};

export default withRouter(
  connect((state) => ({
    sidebar: state.sidebar,
  }))(Layout)
);
