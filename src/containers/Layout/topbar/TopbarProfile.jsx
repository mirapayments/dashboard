import React, { useState } from "react";
import PropTypes from "prop-types";
import DownIcon from "mdi-react/ChevronDownIcon";
import { Collapse } from "reactstrap";
import TopbarMenuLink from "./TopbarMenuLink";
import { useSelector } from "react-redux";
import styles from "./topbar.module.css";

// eslint-disable-next-line no-undef
const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;

const TopbarProfile = ({ changeToLight, changeToDark }) => {
  const [isCollapsed, setIsCollapsed] = useState(false);

  const { userAccounts, signedInAccountNumber } = useSelector((state) => state.user);
  let username;
  userAccounts.forEach((item) => {
    if(item.account_number === signedInAccountNumber){
      username = item.name;
    }
  });

  const handleToggleCollapse = () => {
    setIsCollapsed(!isCollapsed);
  };

  return (
    <div className="topbar__profile">
      <button type="button" className="topbar__avatar" onClick={setIsCollapsed}>
        <img className="topbar__avatar-img" src={Ava} alt="avatar" />
        <p className="topbar__avatar-name">{username}</p>
        <DownIcon className="topbar__icon" />
      </button>
      {isCollapsed && (
        <button
          type="button"
          aria-label="button collapse"
          className="topbar__back"
          onClick={handleToggleCollapse}
        />
      )}
      <Collapse isOpen={isCollapsed} className="topbar__menu-wrap">
        <div className="topbar__menu">
          <div>
            <button
              type="button"
              className={`sidebar__links ${styles.cursor_unset}`}
            >
              <span className="sidebar__links-icon lnr" />
              <p className="sidebar__link-title">{username}</p>
            </button>
            <button
              type="button"
              className={`sidebar__links ${styles.cursor_unset}`}
            >
              <span className="sidebar__links-icon lnr" />
              <p className="sidebar__link-title">{`ID: ${signedInAccountNumber}`}</p>
            </button>
          </div>

          <div className="topbar__menu-divider"></div>
         
          <div>
            <button
              type="button"
              className="sidebar__links"
              onClick={changeToLight}
            >
              <span className="sidebar__links-icon lnr lnr-sun" />
              <p className="sidebar__link-title">Light Theme</p>
            </button>
            <button
              type="button"
              className="sidebar__links"
              onClick={changeToDark}
            >
              <span className="sidebar__links-icon lnr lnr-moon" />
              <p className="sidebar__link-title">Dark Theme</p>
            </button>
          </div>

          <div className="topbar__menu-divider" />
          <TopbarMenuLink
            title="Log Out"
            icon="exit"
            path="/"
            islogOutButton={true}
          />
        </div>
      </Collapse>
    </div>
  );
};

TopbarProfile.propTypes = {
  changeToDark: PropTypes.func,
  changeToLight: PropTypes.func
};

export default TopbarProfile;