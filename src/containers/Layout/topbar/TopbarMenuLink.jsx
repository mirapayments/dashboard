import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
// import { logOut } from "../../../utils/apiHandlers/logout";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
// import { getFromLocal } from "../../../utils/storageService/storageService";
import { removeFromLocal } from "../../../utils/storageService/storageService";
import { logout } from "../../../redux/features/user/userSlice";
// import axios from "axios";

const TopbarMenuLinks = ({ title, icon, path, islogOutButton }) => {
  // const token = getFromLocal("testToken");

  const dispatch = useDispatch();
  const history = useHistory();

  // async function logUserOutTemp(){
  //    ("token inide requ", token)
  //   try {
  //    const res = await axios.post("http://localhost:8000/users/logout/", {
  //       headers: {
  //           'Content-Type': 'application/json',
  //           'Authorization': `Bearer ${token}`
  //       }
  //   })
  //    (res)

  //     if(res.data.status){
  //       await dispatch(setlogUserOut())
  //        history.push("/")
  //     }

  //    } catch (error) {
  //       (error)
  //    }

  // }

  async function logUserOut() {
    await removeFromLocal("token");
    await dispatch(logout());
    history.push("/");

    // try {
    //   const res = await logOut()
    //    (res)
    //  if(res.data.status){
    //  }

    // } catch (error) {

    // }
  }

  return islogOutButton ? (
    <Link
      to={path}
      className="topbar__link"
      onClick={(event) => {
        event.preventDefault();
        logUserOut();
      }}
    >
      <span className={`topbar__link-icon lnr lnr-${icon}`} />
      <p className="topbar__link-title">{title}</p>
    </Link>
  ) : (
    <Link to={path} className="topbar__link">
      <span className={`topbar__link-icon lnr lnr-${icon}`} />
      <p className="topbar__link-title">{title}</p>
    </Link>
  );
};

TopbarMenuLinks.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  islogOutButton: PropTypes.bool
};

export default TopbarMenuLinks;
