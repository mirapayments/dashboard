import React, { useEffect } from "react";

import PropTypes from 'prop-types';
import TopbarSidebarButton from "./TopbarSidebarButton";
import TopbarProfile from "./TopbarProfile";
import MirapayLogo from "../../../shared/img/mirapaylogo.png";
import { Link, useHistory } from "react-router-dom";
import Switch from "react-switch";
import { useSelector, useDispatch } from "react-redux";
import { setLiveDashBoardMode } from "../../../redux/features/user/userSlice";
import styles from "./topbar.module.css";
import { toast } from "react-toastify";

const Topbar = ({
  changeMobileSidebarVisibility,
  changeSidebarVisibility,
  changeToLight,
  changeToDark,
}) => {
  let { liveDashboardMode, userAccounts, signedInAccountNumber } = useSelector((state) => state.user);
  let { complianceSetupComplete } = useSelector((state) => state.ui);
  let history = useHistory();
  const dispatch = useDispatch();
  const Live = () => <p>Live</p>;
  const Test = () => <p>Test</p>;

  async function liveDashboardModeHandler() {
    if(!liveDashboardMode && !complianceSetupComplete){
      toast.info("Complete compliance");
      return history.push("/pages/compliance");
    }
    dispatch(setLiveDashBoardMode(!liveDashboardMode));
    let mode = await localStorage.getItem("mode");
    let modeObject = JSON.parse(mode);
    if (!liveDashboardMode) {  
      modeObject[signedInAccountNumber] = "live";
      await localStorage.setItem("mode", JSON.stringify(modeObject));
    } else {
      modeObject[signedInAccountNumber] = "test";
      await localStorage.setItem("mode", JSON.stringify(modeObject));
    }
    window.location.reload();
  }

  useEffect(() => {
    console.log(userAccounts, signedInAccountNumber);
    initializeAccountModes();
  }
  ,[]);

  const initializeAccountModes = async () => {
    let mode = await localStorage.getItem("mode");
    let modeObject = {};

    //add mode to local storage if not already set
    if(!mode){
      userAccounts.forEach( (item) => {
        modeObject = { ...modeObject, [item.account_number]: 'test'};
      });
      await localStorage.setItem("mode", JSON.stringify(modeObject));
      console.log(modeObject, modeObject[signedInAccountNumber]);
    }

    //check if mode has all accounts, update if not
    if(Object.keys(JSON.parse(await localStorage.getItem("mode"))).length !== userAccounts.length) {
      userAccounts.forEach( (item) => {
        modeObject = { ...modeObject, [item.account_number]: 'test'};
      });
      await localStorage.setItem("mode", JSON.stringify(modeObject));
    }

    //set active mode
    await localStorage.setItem("activeMode", JSON.parse(await localStorage.getItem("mode"))[signedInAccountNumber]);
    dispatch(setLiveDashBoardMode(JSON.parse(await localStorage.getItem("mode"))[signedInAccountNumber] == "live"? true : false ));
    return;
  };


  return (
    <div className="topbar">
      <div className="topbar__wrapper">
        <div className="topbar__left">
          <TopbarSidebarButton
            changeMobileSidebarVisibility={changeMobileSidebarVisibility}
            changeSidebarVisibility={changeSidebarVisibility}
          />
          <Link to="/pages/dashboard">
            <img
              style={{
                height: "100%",
                width: "5em",
              }}
              src={MirapayLogo}
              alt="Mirapay logo"
            />
          </Link>
        </div>
        <div className="topbar__right">
          <div
            className={`${
              liveDashboardMode ? styles.live_mode : styles.test_mode
            } mr-4 mt-3`}
          >
            <p>{liveDashboardMode ? "Live" : "Test"}</p>
          </div>
          <div className="mt-3 ">
            <Switch
              uncheckedIcon={Test}
              onColor="#3BB75E"
              checkedIcon={Live}
              onChange={() => liveDashboardModeHandler()}
              checked={liveDashboardMode}
            />
          </div>
          <TopbarProfile
            changeToDark={changeToDark}
            changeToLight={changeToLight}
          />
        </div>
      </div>
    </div>
  );
};


Topbar.propTypes = {
  changeMobileSidebarVisibility: PropTypes.func.isRequired,
  changeSidebarVisibility: PropTypes.func.isRequired,
  changeToDark: PropTypes.func,
  changeToLight: PropTypes.func
};
export default Topbar;
