import React from "react";
import PropTypes from "prop-types";
import SidebarLink from "./SidebarLink";
import { setPaymentLinkListLoading } from "../../../redux/features/user/uiSlice";
import { setTransactionListLoading } from "../../../redux/features/user/uiSlice";
import { setCustomerListLoading } from "../../../redux/features/user/uiSlice";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router";

const SidebarContent = ({ onClick }) => {
  const { complianceSetupComplete } = useSelector(state => state.ui);
  const location = useLocation();
  const dispatch = useDispatch();

  const handleHideSidebar = () => {
    onClick();
  };


  //here we also intialize loading spinners or shimmer for the sections
  return (
    <div className="sidebar__content">
      <ul className="sidebar__block">
        <div className="sidebar__menu-divider"></div>
        <SidebarLink
          title="Dashboard"
          icon="home"
          route="/pages/dashboard"
          onClick={handleHideSidebar}
        />
        {!complianceSetupComplete?<SidebarLink
          title="Compliance"
          icon="enter"
          route="/pages/compliance"
          onClick={handleHideSidebar}
        />: null}
        <div className="sidebar__menu-divider"></div>
        <SidebarLink
          title="Transactions"
          icon="enter"
          route="/pages/transaction"
          onClick={() => {
            if (location.pathname !== "/pages/transaction") {
              dispatch(setTransactionListLoading(true));
            }
            handleHideSidebar();
          }}
        />
        <SidebarLink
          title="Customers"
          icon="enter"
          route="/pages/customers"
          onClick={() => {
            if (location.pathname !== "/pages/customers") {
              dispatch(setCustomerListLoading(true));
            }
            handleHideSidebar();
          }}
        />
        <SidebarLink
          title="Balance"
          icon="enter"
          route="/pages/balance"
          onClick={handleHideSidebar}
        />
        <SidebarLink
          title="Disputes"
          icon="enter"
          route="/pages/disputes"
          onClick={handleHideSidebar}
        />
        <SidebarLink
          title="Subaccounts"
          icon="enter"
          route="/pages/subaccounts"
          onClick={handleHideSidebar}
        />
        <SidebarLink
          title="Payment Links"
          icon="enter"
          route="/pages/payments"
          onClick={() => {
            if (location.pathname !== "/pages/payments") {
              dispatch(setPaymentLinkListLoading(true));
            }
            handleHideSidebar();
          }}
        />
        <SidebarLink
          title="Settings"
          icon="enter"
          route="/pages/settings"
          onClick={handleHideSidebar}
        />
      </ul>
    </div>
  );
};

SidebarContent.propTypes = {
  changeToDark: PropTypes.func,
  changeToLight: PropTypes.func,
  onClick: PropTypes.func.isRequired,
};

export default SidebarContent;
