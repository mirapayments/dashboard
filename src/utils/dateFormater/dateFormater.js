function formatDate(str) {
  if (str == null) {
    return "";
  }

  const months = {
    1: "Jan.",
    2: "Feb.",
    3: "Mar.",
    4: "Apr.",
    5: "May",
    6: "June",
    7: "July",
    8: "Aug.",
    9: "Sep.",
    10: "Oct.",
    11: "Nov.",
    12: "Dec.",
  };

  //Days of the week
  // const days ={
  //   1:"Monday",
  //   2:"Tuesday",
  //   3:"Wednesday",
  //   4:"Thursday",
  //   5:"Friday",
  //   6:"Saturday",
  //   7:"Sunday"
  // }

  function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "PM" : "AM";
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + "" + ampm;
    return strTime;
  }

  const date = new Date(str);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return `${months[month]}  ${day},  ${year} at ${formatAMPM(date)}`;
}

export { formatDate };
