import axios from "axios";

// base url depending on whether live or dev
const baseUrl =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

// remove  console logs from prod
if (process.env.NODE_ENV === "production") {
  console.log = function () {};
}

//create an axios instance for subsequent calls
const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_TEST_WITH_LIVE_SERVER == "yes" ? "https://api.mirapayments.com" : baseUrl,
  headers: {
    "Content-Type": "application/json",
  },
});

axiosClient.interceptors.response.use(
  (response) => response,
  (err) => Promise.reject(err.response)
);

/*
// EXPERIMENTAL FEATURES}
// function manageErrorConnection(err) {
//   if (err.response && err.response.status >= 400 && err.response.status <= 500) {
//     // this will trigger the `handleError` function in the promise chain
//     return Promise.reject(err.response)
//   } else if (err.code === 'ECONNREFUSED') {
//     // this will trigger the `handlerResponse` function in the promise chain
//     // bacause we are not returning a rejection! Just an example
//     return 'nevermind'
//   }
//   else if(err.response.status === 400){
//     return Promise.reject(new Error('User Already Exists'))
//   }
//   else {
//     // this will trigger the `handleError` function in the promise chain
//     return Promise.reject(err)
//   }
// }

// AXIOS TEST API CALL
// axios.get('http://localhost:3000/user?ID=12345')
//   .then(handleResponse)
//   .catch(handleError)



// TEST FUNCTIONS
// function handleResponse(response) {
//    (`handleResponse: ${response}`);
// }

// function handleError(error) {
//    (`handleError: ${error}`);
// }

// axiosClient.interceptors.response.use(
//   function (response) {
//     return response;
//   },
//   function (error) {
//     return error;
//     // return error.response
//     let res = error.response;
//     // redirect rules for all 401 responses to another page
//     if (res.status >= 401 || res.status < 500) {
//       return res;
//     }
//     // return error;
//     return Promise.reject(error)
//   }
// );
*/

export { axiosClient };
