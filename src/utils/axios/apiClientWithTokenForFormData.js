import axios from "axios";

const baseUrl =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_TEST_WITH_LIVE_SERVER == "yes" ? "https://api.mirapayments.com" : baseUrl,
  headers: {
    "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundarySaAvyr3jxrCIwlIX",
  },
});

axiosClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    let res = error.response;
    //takes user back to the home page when the token expires. Needs some review though
    if ((res.status === 401 && res.data.detail.includes("Token has expired")) || res.status === 401 ) {
      const currentRoute = window.location.pathname;
      console.log(currentRoute);
      window.location.replace(`/?previous_route=${currentRoute}`);
      return;
    }
    return Promise.reject(res);
  }
);

axiosClient.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("token");
    const mode = localStorage.getItem("liveMode") ?? "test";
    if (token) {
      config.headers.authorization = `Token ${token}`;
      config.headers.mode = mode;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export { axiosClient };
