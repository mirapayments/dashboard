function getFromLocal(key) {
  return localStorage.getItem(key);
  // const res = localStorage.getItem(key);
  // return res ? JSON.parse(localStorage.getItem(key)) : null;
}
function saveToLocal(key, value) {
  localStorage.setItem(key, value);
}

function removeFromLocal(key) {
  localStorage.removeItem(key);
}

export { getFromLocal, saveToLocal, removeFromLocal };
