function removeEmpty(obj) {
  return Object.fromEntries(
    Object.entries(obj).filter(([v]) => v.length !== 0)
  );
}

export { removeEmpty };
