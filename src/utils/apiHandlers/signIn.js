import { axiosClient } from "../axios/apiClient";

export function signInUser(data) {
  return axiosClient.post("users/login/", data);
}
