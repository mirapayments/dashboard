import { axiosClient } from "../axios/apiClientWithToken";

export async function getTransactionDetails(id) {
  return axiosClient.get(`/transactions/transaction/${id}/`);
}
