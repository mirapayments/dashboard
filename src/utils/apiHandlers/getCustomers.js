// Import the necessary libraries.
import { axiosClient } from "../axios/apiClientWithToken";

// Export function to fetch customers.
export async function getCustomers(account_number) {
  return axiosClient.get(`/customers/${account_number}/`);
}
