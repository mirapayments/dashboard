import { axiosClient } from "../axios/apiClientWithToken";

export async function getPaymentLinkDetailsWithID(id, account_number) {
  return axiosClient.get(`/payments/payment-link/${id}/${account_number}/`);
}
