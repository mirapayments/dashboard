import { axiosClient } from "../axios/apiClientWithToken";

export function inviteUser(account_number, data) {
  return axiosClient.post(`/users/invite/${account_number}/`, data);
}
