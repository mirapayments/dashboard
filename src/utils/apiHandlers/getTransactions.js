import { axiosClient } from "../axios/apiClientWithToken";

export async function getTransactions(account_number) {
  return axiosClient.get(`/transactions/${account_number}/`);
}
