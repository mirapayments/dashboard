// Import the necessary library.
import axios from 'axios';
import { axiosClient } from '../axios/apiClientWithToken';

const CancelToken = axios.CancelToken;
export const source = CancelToken.source();

// Export the function to fetch Dashboard Stats.
export async function getUserDashboardStats(account_number, filter) {
  return axiosClient.get(`/accounts/dashboard/${account_number}/?q=${filter}`, {
    CancelToken: source.token,
  });
}
