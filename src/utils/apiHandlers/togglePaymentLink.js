import { axiosClient } from "../axios/apiClientWithToken";
export function togglePaymentLink(id, account_number) {
  return axiosClient.post(
    `/payments/payment-link/toggle/${id}/${account_number}/`
  );
}
