import { axiosClient } from "../axios/apiClientWithToken";

export function refreshApiKeys(account_number, data) {
  return axiosClient.post(`/accounts/refresh-keys/${account_number}/`, data);
}
