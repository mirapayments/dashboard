import { axiosClient } from "../axios/apiClientWithToken";

export async function logOut() {
  return axiosClient.post("/users/logout/");
}
