import { axiosClient } from "../axios/apiClientWithToken";

export async function getAccountDetails(account_number) {
  return axiosClient.get(`/accounts/detail/${account_number}/`);
}
