import { axiosClient } from "../axios/apiClientWithToken";

export async function getPaymentLinks(account_number) {
  return axiosClient.get(`/payments/payment-link/${account_number}/`);
}
