import { axiosClient } from "../axios/apiClientWithToken";

export async function exportTransactions(account_number) {
  return axiosClient.post(`/transactions/export/${account_number}/`);
}
