import { axiosClient } from "../axios/apiClientWithToken";

export async function getPaymentLinkTransactions(account_number, code) {
  return axiosClient.get(`/transactions/${account_number}/?source_reference=${code}`);
}
