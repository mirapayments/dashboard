import { axiosClient } from "../axios/apiClientWithToken";

export function verifyToken(data) {
  return axiosClient.post(`/users/token/verify/`, data);
}
