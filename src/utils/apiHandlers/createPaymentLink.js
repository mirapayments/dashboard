import { axiosClient } from "../axios/apiClientWithToken";

export function createPaymentLink(account_number, data) {
  return axiosClient.post(`/payments/payment-link/${account_number}/`, data);
}
