import { axiosClient } from "../axios/apiClient";

export function activateUser(data) {
  return axiosClient.post(`/users/activate/`, data);
}
