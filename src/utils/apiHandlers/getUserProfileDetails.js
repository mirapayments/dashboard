import { axiosClient } from "../axios/apiClientWithToken";
export function getUserProfileDetail() {
  return axiosClient.get("/users/me/");
}
