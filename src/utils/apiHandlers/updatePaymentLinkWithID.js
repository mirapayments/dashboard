import { axiosClient } from "../axios/apiClientWithToken";
export function updatePaymentLinkWithID(id, account_number, data) {
  return axiosClient.put(
    `/payments/payment-link/${id}/${account_number}/`,
    data
  );
}
