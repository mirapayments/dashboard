import { axiosClient } from "../axios/apiClientWithToken";

export async function getPaymentLinkDetails() {
  return axiosClient.get(`/payments/payment-link/${code}/`);
}
