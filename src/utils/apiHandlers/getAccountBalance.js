// Import Axios Client
import { axiosClient } from "../axios/apiClientWithToken";

export async function getAccountBalance(accountNumber) {
    return axiosClient.get(`/accounts/balance/${accountNumber}/`);
} 

