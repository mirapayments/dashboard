// Import axiosClient
import { axiosClient } from "../axios/apiClientWithToken";

export async function getCustomersDetailWithID(id) {
  return axiosClient.get(`/customers/customer/${id}/`);
}
