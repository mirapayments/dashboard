import { axiosClient } from "../axios/apiClientWithToken";

export function changePassword(data) {
  return axiosClient.put(`/users/me/change-password/`, data);
}
