import { axiosClient } from "../axios/apiClientWithToken";

export function getCompliance(account_number) {
  return axiosClient.get(
    `/accounts/compliance/${account_number}/`
  );
}
