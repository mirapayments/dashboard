import { axiosClient } from "../axios/apiClientWithToken";

export function createNewBankAccount(data) {
  return axiosClient.post("/accounts/create/", data);
}
