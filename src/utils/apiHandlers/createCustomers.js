// Import axios client.
import { axiosClient } from "../axios/apiClientWithToken";

export function createNewCustomers(account_number, data) {
  return axiosClient.post(`/customers/${account_number}/`, data);
}
