import { axiosClient } from "../axios/apiClientWithToken";

export async function getUserAccountDetails(account_number) {
  return axiosClient.get(`/accounts/detail/${account_number}/`);
}
