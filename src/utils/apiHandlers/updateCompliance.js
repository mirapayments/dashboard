import { axiosClient } from "../axios/apiClientWithTokenForFormData";

export function updateCompliance(account_number, data) {
  return axiosClient.put(
    `/accounts/compliance/${account_number}/`,
    data
  );
}
