import { axiosClient } from "../axios/apiClientWithToken";

export function deletePaymentLink(id, account_number) {
  return axiosClient.delete(`/payments/payment-link/${id}/${account_number}/`);
}
