import { axiosClient } from "../axios/apiClientWithToken";

export async function getCurrencyTypes() {
  return axiosClient.get("/accounts/currencies/");
}
