import { axiosClient } from "../axios/apiClientWithToken";
export function updateUserProfile(data) {
  return axiosClient.put("/users/me/", data);
}
