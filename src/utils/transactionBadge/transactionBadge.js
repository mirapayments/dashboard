function transactionBadge(status) {
  let color = "success";
  switch (status) {
    case "Initiated":
      color = "warning";
      break;
    case "Success":
      color = "success";
      break;
    case "Failed":
      color = "danger";
      break;
    case "Canceled":
      color = "danger";
      break;
    case "Reversed":
      color = "primary";
      break;
    default:
      color = "success";
  }
  return color;
}

export { transactionBadge };
