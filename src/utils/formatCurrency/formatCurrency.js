export const numberFormat = (value, currency = 'NGN') =>
    new Intl.NumberFormat('en-IN', {
      style: 'currency',
      currency: currency,
      currencyDisplay: "code"
    }).format(value);