export function setLimitBasedOnCurrency(currency){
    if(currency === "EUR" || currency === "USD" || currency === "GBP"){
      return 1;
    }
    return 100;
}