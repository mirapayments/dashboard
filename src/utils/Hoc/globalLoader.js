import React, { useState } from "react";
import LoadingBar from "./Loader";

const isLoadingHOC = (WrappedComponent) => {
  function HOC(props) {
    const [isLoading, setLoading] = useState(true);

    const setLoadingState = (isComponentLoading) => {
      setLoading(isComponentLoading);
    };

    return (
      <>
        <LoadingBar color="#f11946" startLoad={isLoading} />
        <WrappedComponent {...props} setLoading={setLoadingState} />
      </>
    );
  }
  return HOC;
};

export default isLoadingHOC;
