import {
  useRef,
  useEffect,
  forwardRef,
  useState,
  useImperativeHandle,
  createElement,
} from "react";

const noop = () => {};

function useInterval(callback, delay, immediate) {
  const savedCallback = useRef(noop);
  useEffect(() => {
    savedCallback.current = callback;
  });
  useEffect(() => {
    if (!immediate) return;
    if (delay === null || delay === false) return;
    savedCallback.current();
  }, [immediate]);
  useEffect(() => {
    if (delay === null || delay === false) return undefined;

    const tick = () => savedCallback.current();

    const id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
}

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const LoadingBar = forwardRef(
  (
    {
      setLoading,
      progress,
      height: _height = 2,
      className: _className = "",
      color: _color = "red",
      background: _background = "transparent",
      onLoaderFinished,
      transitionTime: _transitionTime = 300,
      loaderSpeed: _loaderSpeed = 500,
      waitingTime: _waitingTime = 1000,
      shadow: _shadow = true,
      containerStyle: _containerStyle = {},
      style: _style = {},
      shadowStyle: shadowStyleProp = {},
      containerClassName: _containerClassName = "",
    },
    ref
  ) => {
    const isMounted = useRef(false);
    const [localProgress, localProgressSet] = useState(0);
    const [pressedContinuous, setPressedContinuous] = useState({
      active: false,
      startingValue: 20,
      refreshRate: 1000,
    });
    const [usingProps, setUsingProps] = useState(false);
    const [pressedStaticStart, setStaticStartPressed] = useState({
      active: false,
      value: 20,
    });
    const initialLoaderStyle = {
      height: "100%",
      background: _color,
      transition: `all ${_loaderSpeed}ms ease`,
      width: "0%",
    };
    const loaderContainerStyle = {
      position: "fixed",
      top: 0,
      left: 0,
      height: _height,
      background: _background,
      zIndex: 99999999999,
      width: 100 + "%",
    };
    const initialShadowStyles = {
      boxShadow: `0 0 10px ${_color}, 0 0 10px ${_color}`,
      width: "5%",
      opacity: 1,
      position: "absolute",
      height: "100%",
      transition: `all ${_loaderSpeed}ms ease`,
      transform: "rotate(3deg) translate(0px, -4px)",
      left: "-10rem",
    };
    const [loaderStyle, loaderStyleSet] = useState(initialLoaderStyle);
    const [shadowStyle, shadowStyleSet] = useState(initialShadowStyles);
    useEffect(() => {
      isMounted.current = true;
      return () => {
        isMounted.current = false;
      };
    }, []);
    useImperativeHandle(ref, () => ({
      continuousStart(startingValue, refreshRate = 1000) {
        if (pressedStaticStart.active) return;

        if (usingProps) {
          console.warn(
            "react-top-loading-bar: You can't use both controlling by props and ref methods to control the bar!"
          );
          return;
        }

        const val = startingValue || randomInt(10, 20);
        setPressedContinuous({
          active: true,
          refreshRate,
          startingValue,
        });
        localProgressSet(val);
        checkIfFull(val);
      },

      staticStart(startingValue) {
        if (pressedContinuous.active) return;

        if (usingProps) {
          console.warn(
            "react-top-loading-bar: You can't use both controlling by props and ref methods to control the bar!"
          );
          return;
        }

        const val = startingValue || randomInt(30, 50);
        setStaticStartPressed({
          active: true,
          value: val,
        });
        localProgressSet(val);
        checkIfFull(val);
      },

      complete() {
        if (usingProps) {
          console.warn(
            "react-top-loading-bar: You can't use both controlling by props and ref methods to control the bar!"
          );
          return;
        }

        localProgressSet(100);
        checkIfFull(100);
      },
    }));

    //customer
    if (setLoading) {
      setPressedContinuous({
        active: true,
      });
    } else {
      localProgressSet(100);
      checkIfFull(100);
    }

    useEffect(() => {
      loaderStyleSet({ ...loaderStyle, background: _color });
      shadowStyleSet({
        ...shadowStyle,
        boxShadow: `0 0 10px ${_color}, 0 0 5px ${_color}`,
      });
    }, [_color]);
    useEffect(() => {
      if (ref) {
        if (ref && progress !== undefined) {
          console.warn(
            'react-top-loading-bar: You can\'t use both controlling by props and ref methods to control the bar! Please use only props or only ref methods! Ref methods will override props if "ref" property is available.'
          );
          return;
        }

        checkIfFull(localProgress);
        setUsingProps(false);
      } else {
        if (progress) checkIfFull(progress);
        setUsingProps(true);
      }
    }, [progress]);

    const checkIfFull = (_progress) => {
      if (_progress >= 100) {
        loaderStyleSet({ ...loaderStyle, width: "100%" });

        if (_shadow) {
          shadowStyleSet({ ...shadowStyle, left: _progress - 10 + "%" });
        }

        setTimeout(() => {
          if (!isMounted.current) {
            return;
          }

          loaderStyleSet({
            ...loaderStyle,
            opacity: 0,
            width: "100%",
            transition: `all ${_transitionTime}ms ease-out`,
            color: _color,
          });
          setTimeout(() => {
            if (!isMounted.current) {
              return;
            }

            if (pressedContinuous.active) {
              setPressedContinuous({ ...pressedContinuous, active: false });
              localProgressSet(0);
              checkIfFull(0);
            }

            if (pressedStaticStart.active) {
              setStaticStartPressed({ ...pressedStaticStart, active: false });
              localProgressSet(0);
              checkIfFull(0);
            }

            if (onLoaderFinished) onLoaderFinished();
            localProgressSet(0);
            checkIfFull(0);
          }, _transitionTime);
        }, _waitingTime);
      } else {
        loaderStyleSet((_loaderStyle) => {
          return {
            ..._loaderStyle,
            width: _progress + "%",
            opacity: 1,
            transition: _progress > 0 ? `all ${_loaderSpeed}ms ease` : "",
          };
        });

        if (_shadow) {
          shadowStyleSet({
            ...shadowStyle,
            left: _progress - 5.5 + "%",
            transition: _progress > 0 ? `all ${_loaderSpeed}ms ease` : "",
          });
        }
      }
    };

    useInterval(
      () => {
        const random = randomInt(10, 20);

        if (localProgress + random < 90) {
          localProgressSet(localProgress + random);
          checkIfFull(localProgress + random);
        }
      },
      pressedContinuous.active ? pressedContinuous.refreshRate : null
    );
    return createElement(
      "div",
      {
        className: _containerClassName,
        style: { ...loaderContainerStyle, ..._containerStyle },
      },
      createElement(
        "div",
        {
          className: _className,
          style: { ...loaderStyle, ..._style },
        },
        _shadow
          ? createElement("div", {
              style: { ...shadowStyle, ...shadowStyleProp },
            })
          : null
      )
    );
  }
);

export default LoadingBar;
//# sourceMappingURL=index.modern.js.map
