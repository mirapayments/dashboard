describe("Customer List", () => {
    it("Customer List Component should render", () => {
        // Visit the site.
        cy.visit("/");
        // Login
        cy.findByPlaceholderText(/Email/i).type("CI/CDtest@gmail.com");
        cy.findByPlaceholderText(/password/i).type("ci/cd@1.618");
        cy.findByRole('button', {  name: /sign in/i}).click();
        // Select the account.
        cy.get('.AccountSelectionPage_container__3As3T  > :nth-child(3)').click();
        // Click on the customer tab
        cy.get('.sidebar__wrapper--desktop > .sidebar__content > .sidebar__block > [href="/pages/customers"] > .sidebar__link').click();
        // Click on the Add Customer Button to add customer.
        cy.findByRole('button', {  name: /add customer/i}).click();
        // Fill the customer's details.
        cy.findByPlaceholderText(/full name/i).type("Adebayo Abdulmalik");
        //cy.findByPlaceholderText(/Email/i ).type("milikiadbay@gmail.com");
        cy.get('[name="email"]').type("milikiadbay@gmail.com");
        cy.findByPlaceholderText(/Enter phone number/i).type("+2349039561875");
        // Click the submit button.
        cy.findByRole('button', {  name: /create customer/i}).click();
        
        // Verify if Customer was Created.
        cy.get('table').find('tr').should('be.visible');
        // cy.get('table').find('td').should('have.text', "Adebayo Abdulmalik");
        cy.get('table').find('tbody tr:last td:first div span').contains('span', /adebayo abdulmalik/i).should('be.visible');
        cy.get('table').find('tbody tr:last td').eq(1).contains('div span span', /milikiadbay@gmail.com/i).should('exist');
    })
})
