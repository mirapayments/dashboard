describe('User Accounts', () => {
    it('Assert an account can be created', () => {
        // Visit the page.
        cy.visit("/");
        // Login
        cy.findByPlaceholderText(/Email/i).type("CI/CDtest@gmail.com");
        cy.findByPlaceholderText(/password/i).type("ci/cd@1.618");
        cy.findByRole('button', {  name: /sign in/i}).click();
        // Select the account.
        cy.get('.AccountSelectionPage_container__3As3T  > :nth-child(3)').click();
        // Click on the Settings on the sidebar.
        cy.get('.sidebar__wrapper--desktop > .sidebar__content > .sidebar__block > [href="/pages/settings"] > .sidebar__link').click();
        // Click on Accounts tabs.
        cy.get(':nth-child(4) > .nav-link').click();
        // Click New Account button.
        cy.get(':nth-child(1) > .ListAccount_Account_buttons__O16Qm > div.ListAccount_button_newaccount__3usAY > :nth-child(1) > div > .ListAccount_button_newaccount__3usAY').click();
        // Fill the form.
        // Name.
        cy.get('.account__card > .form > :nth-child(1) > .form__form-group-field > input').type('Rafiu Moshood');
        // Currency.
        cy.get(':nth-child(2) > .CreateAccountForm_select_form_custom_container__DBeJT > .CreateAccountForm_select_form_custom_light__3v4BX').select('EUR');
        // Account Type.
        cy.get(':nth-child(3) > .CreateAccountForm_select_form_custom_container__DBeJT > .CreateAccountForm_select_form_custom_light__3v4BX').select('Individual');
        // Click the create account button.
        cy.get('.form > .btn').click();
        // Wait for the table to display.
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(5000);
        
    });

    it("Assert all accounts are listed", () => {
        // Assert the table is rendered and visible.
        cy.get('#assertion-table').should('be.visible');
        // Assert a new tr is added.
        cy.get('#assertion-table').find('tr').should('be.visible');
        // Assert the account created is listed.
        cy.get('#assertion-table').find('tbody').find('tr').last().find('td').first().find('p').should('contain', 'Rafiu Moshood');
    });

    it("Assert the account can be switched", () => {
        cy.get(':nth-child(2) > :nth-child(5) > .ListAccount_button__FPd-k').click();
        cy.wait(5000);
        cy.get(':nth-child(2) > :nth-child(5) > .ListAccount_button__FPd-k').should('be.disabled');
    })
 });
    