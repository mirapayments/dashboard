describe("Transaction List", () => {
    it("transaction page should render", () => {
        // Visit the page
        cy.visit("/");
        // Login.
        cy.findByPlaceholderText(/Email/i).type("CI/CDtest@gmail.com");
        cy.findByPlaceholderText(/password/i).type("ci/cd@1.618");
        cy.findByRole('button', {  name: /sign in/i}).click();
        // Select the account.
        cy.get('.AccountSelectionPage_container__3As3T  > :nth-child(3)').click();
        // Click on the Transaction Navlink.
        cy.get('.sidebar__wrapper--desktop > .sidebar__content > .sidebar__block > [href="/pages/transaction"] > .sidebar__link').click();
        // Assert that the transaction are rendered.
        cy.get('table').find('tr').should('be.visible');
        cy.get('table').find('tr').should('be.visible');
    });
});