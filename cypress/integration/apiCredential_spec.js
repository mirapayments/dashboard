describe('APICredentials', () => {
    before(() => {
        cy.clearLocalStorageSnapshot();
    });
    
    beforeEach(() => {
        cy.restoreLocalStorage();
        // cy.visit("/");
    });
    
    afterEach(() => {
        cy.saveLocalStorage();
    });
    it('Assert the api credential for an account are displayed', () => {
          // Visit the page.
        cy.visit("/");
        // Login
        cy.findByPlaceholderText(/Email/i).type("CI/CDtest@gmail.com");
        cy.findByPlaceholderText(/password/i).type("ci/cd@1.618");
        cy.findByRole('button', {  name: /sign in/i}).click();
        // Select the account.
        //AccountSelectionPage_container__3As3T
        cy.get('.AccountSelectionPage_container__3As3T  > :nth-child(3)').click();
        // Click on the Settings on the sidebar.
        cy.get('.sidebar__wrapper--desktop > .sidebar__content > .sidebar__block > [href="/pages/settings"] > .sidebar__link').click();
        // Click on Api tabs.
        cy.findByTestId(/api/i).click();
        // Assert the secret key are displayed.
        cy.get(':nth-child(3) > :nth-child(1) > .form__form-group-field > input').invoke('val').should('not.be.empty');
        // Assert the public key are displayed.
        cy.get(':nth-child(3) > :nth-child(2) > .form__form-group-field > input').invoke('val').should('not.be.empty');
        // Add a new webhook url
        cy.get(':nth-child(3) > :nth-child(3) > .form__form-group-field > input').type('{selectall}{backspace}https://webhook.site/0cef69cd-111c-48c4-8862-0aa07b667eb96');
        // Save the new webhook url.
        cy.findByTestId(/save/i).click();
    });
    
    it('Assert the webhook url was updated successfully', () => {
        // Assert the new webhook url was updated.
        cy.get(':nth-child(3) > :nth-child(3) > .form__form-group-field > input').should('have.value', 'https://webhook.site/0cef69cd-111c-48c4-8862-0aa07b667eb96');
    });
});
