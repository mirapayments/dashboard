describe("Payment List", () => {
  before(() => {
    cy.clearLocalStorageSnapshot();
  });

  beforeEach(() => {
    cy.restoreLocalStorage();
    // cy.visit("/");
  });

  afterEach(() => {
    cy.saveLocalStorage();
  });
  it("Assert that payment can be created", () => {
    cy.visit("/");
    // Login
    cy.findByPlaceholderText(/Email/i).type("CI/CDtest@gmail.com");
    cy.findByPlaceholderText(/password/i).type("ci/cd@1.618");
    cy.findByRole("button", { name: /sign in/i }).click();
    // Select the account.
    cy.get(".AccountSelectionPage_container__3As3T  > :nth-child(3)").click();
    // Click on the Payment links tab.
    cy.get(
      '.sidebar__wrapper--desktop > .sidebar__content > .sidebar__block > [href="/pages/payments"] > .sidebar__link'
    ).click();
    // Click on the new payment link button.
    cy.get(".icon").click();
    // Click on single payment button.
    cy.get(
      ":nth-child(1) > div > .PaymentLinkForm_button_newaccount__1NekY"
    ).click();
    // Fill the form.
    // Name.
    cy.get(":nth-child(1) > .form__form-group-field > input").type(
      "School fees Payment"
    );
    // Currency.
    cy.get(
      ":nth-child(2) > .PaymentLinkForm_select_form_custom_container__Lx2-T > .PaymentLinkForm_select_form_custom_light__1CoMe"
    ).select("NGN");
    // Fixed Amount (Yes) and type the amount.
    cy.get(
      ":nth-child(3) > .PaymentLinkForm_select_form_custom_container__Lx2-T > .PaymentLinkForm_select_form_custom_light__1CoMe"
    ).select("Yes");
    cy.get(":nth-child(4) > .form__form-group-field > input")
      .clear()
      .type("25000.00");
    // Description.
    cy.get(":nth-child(5) > .form__form-group-field > input").type(
      "This is suppose to test this feature of the application."
    );
    // Click the Advanced options.
    cy.get(".PaymentLinkForm_button_advanced_toggle__nZ4fz").click();
    // Fill in the short form.
    // Notification Email.
    cy.get(":nth-child(7) > .form__form-group-field > input").type(
      "milikiadbay@gmail.com"
    );
    // Redirect Url.
    cy.get(":nth-child(8) > .form__form-group-field > input").type(
      "https://previews.aspirity.com/easydev/tables/resizable_table"
    );
    // Extra Info
    cy.get(
      ".form__form-group-field > div > .PaymentLinkForm_button_advanced_toggle__nZ4fz"
    ).click();
    // Enter the field name.
    cy.get('[style="display: flex; margin-bottom: 20px;"] > input').type(
      "Phone Number"
    );
    // Click the + button.
    cy.get(
      '[style="display: flex; margin-bottom: 20px;"] > :nth-child(4)'
    ).click();
    // Enter another field name.
    cy.get(":nth-child(1) > :nth-child(1) > input").type("Gender");
    // Click the + button.
    cy.get(
      ".form__form-group-field > :nth-child(1) > :nth-child(2) > :nth-child(4)"
    ).click();
    // Click the - button.
    cy.get(
      ".form__form-group-field > :nth-child(1) > :nth-child(2) > :nth-child(3)"
    ).click();
    // Click the create payment link button.
    cy.get(".container > .btn").click();
    // Wait for the table to be displayed.
    cy.wait(5000);
  });

  it("Assert payment is listed", () => {
    // Assert that the payment link was created successfully.
    cy.get("#assertion-table").find("tr").should("be.visible");
    cy.get("#assertion-table")
      .find("tr")
      .first()
      .find("td")
      .first()
      .should("have.text", "School fees Payment");
  });

  it("Assert payment can be updated", () => {
    // Click on payment detail entry
    cy.get(".editable-table > .table > :nth-child(1) > :nth-child(1)").click();
    // Click on the Edit Payment Link button.
    cy.get(".ListAccount_button_newaccount__2Fh4k").click();
    // Fill the form.
    // Name
    cy.get(":nth-child(1) > .form__form-group-field > input").type(
      "{selectall}{backspace}Department fees Payment"
    );
    // Currency.
    cy.get(
      ":nth-child(2) > .PaymentLinkForm_select_form_custom_container__2vqOO > .PaymentLinkForm_select_form_custom_light__sdVhk"
    ).select("EUR");
    // Change the amount.
    cy.get(":nth-child(5) > .form__form-group-field > input").type(
      "{selectall}{backspace}30000.00"
    );
    // Description.
    cy.get(":nth-child(6) > .form__form-group-field > input").type(
      "{selectall}{backspace}This is suppose to test this feature."
    );
    // Click the Advanced options.
    cy.get(".PaymentLinkForm_button_advanced_toggle__33sHQ").click();
    // Fill in the short form.
    // Notification Email.
    cy.get(":nth-child(8) > .form__form-group-field > input").type(
      "{selectall}{backspace}abdulmalikadebayo07@gmail.com"
    );
    // Redirect Url.
    cy.get(":nth-child(9) > .form__form-group-field > input").type(
      "{selectall}{backspace}http://localhost:3000/pages/payments/21"
    );
    // Extra Info
    // Enter the field name.
    cy.get(":nth-child(1) > :nth-child(1) > input").type(
      "{selectall}{backspace}Matric No"
    );
    cy.get(":nth-child(1) > :nth-child(2) > input").type(
      "{selectall}{backspace}Department"
    );
    // Click on the update payment link button.
    cy.get(".container > .btn").click();
  });
});
