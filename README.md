# Mirapayments Dashboard
# Local Setup
1.  Clone the repository
2.  `cd` into the repository
3.  Install [n](https://www.npmjs.com/package/n) to help manage your node versions, as the node version supported for this app is `14.17.4`. Use `npm install -g n` to install `n`
4.  Use `n auto` to confirm you have the correct node version installed, or to install the desired node version
5.  Run `npm install` to install all packages and dependencies
6.  Make sure the API server has been setup
7. Run `npm run dev` to start the local server
The application should now be running on port 3000 (localhost:3000)


# Development:
1. Ensure only needed packages are added and remove a package no longer in use
    from the application dependencies.

2. Ensure no sensitive data is exposed to the user or browser. 
Note: Some values are stored in the local storage.
However, never store sensitive data to the local storage

3. Provide comments that adequately describes your operation/aim/usage in the code base

4. Write tests for all your implementations

5. Before you update a line of code, make sure you fully understand what it does, else seek help from other developers

6. Always make a pull request. Do **NOT** push to the `main` branch directly.